# Dtransfert

Dtransfert, pour Distributed transfert, est un outil qui permets aux administrateurs de stockage de copier des fichiers d'une plateforme à une autre. 

Chaque référence de fichier d'une arborescence source est ajouté dans un message broker puis traité (calcul de hash à la source, cpie calcule de hash à la destination). L'arborescence manquante à la destination est recrée. 

Par défaut, Dtransfert pilote le nombre de copie simultanée en s'assurant de la santé de runner de copie. 

Optionnellement :
 - Dtransfert s'assure de la santé (cpu et mémoire) de la plateforme source et destination.

## Pré-requis d'architecture

Les pré-requis d'architecture sont décrits dans [install](docs/install.md). 

Les différents composants et serive nécéssaires à l'application sont décrits dans [conception](docs/conception.md##composant,-service,-schéma-applicatif-&-middleware)



## Installlation de Dtransfert

L'installation est piloté par un script [install.sh](deploy/install.sh) dont l'utilisation est décrites dans [install](docs/install.md#-procédure).


## Utilisation de Dtransfert

L'utilisation basique de Dtransfert est décrites dans [manage-copy-job](docs/how-to/manage-copy-job.md).
Depuis un runner, cette commande
```
dtransfert@runner# /OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -s /VAR/source/test/ -t /VAR/target/test/ -j test -e http://controller -c SHA-256 -u admin -p admin123 
```
crée un job nommé `test`.
Chaque fichier enfant sera évalué par des [tâches en arriere plan](docs/conception.md####-Focus-sur-les-taches-de-transfert-:-tasks_transfer.*) qui vont :
 - calculer le checksum du fichier source grâce à l'algorithme SHA-256
 - recrée l'arborescence manquante à la destination
 - copier le fichier
 - calculer le checksum du fichier destination grâce à l'algorithme SHA-256
 - comparer les checksum. Si les checksum sont différents, le fichier destination est supprimé. 

# Comportement connus
 * Une arborescence sans fichier ne sera pas recrée.


# Roadmap
Ce projet a un statut inactif. Je l'ai concu pour un besoin ponctuel. Je reste disponible pour le faire évoluer au besoin.

# Utilisation en production
Ce programme m'a permis de copier 250 TO de petits fichiers en 2 mois en prenant en compte différents points de santé des plateformes en présence.


# Vérification 
rsync est un formidable outils pour transférer des données d'une plteforme à une autre. Mais sur des arborescences larges, il est inutilisable.
Il est possible de le rendre utilisable en morcelant le travail. Imaginons une arborescence /VAR/source/YYYY/MM/DD dans laquelle aucun fichier n'existe dans les sous-répertoire YYYY, YYYY/MM 


```

LOG_DIR='/VAR/log/'
mkdir -p ${LOG_DIR}
ROOT_SOURCE='/VAR/source/'
ROOT_TARGET='/VAR/target/'

for source_dir in $(find ${ROOT_SOURCE} -mindepth 3 -maxdepth 3 -type d);do
  target_dir=${source_dir//${ROOT_SOURCE}/${ROOT_TARGET}}
  mkdir -p ${target_dir}
  child=${source_dir/${ROOT_SOURCE}} 
  job="${child//'/'/'-'}"
  echo "${child} ${job} ${source_dir} -> ${target_dir} "
  echo "${child} ${job} ${source_dir} -> ${target_dir} " >>${LOG_DIR}/${job}.log 
  rsync -rc --progress --no-perms --no-owner --no-group ${source_dir}/ ${target_dir}/ >> ${LOG_DIR}/${job}.log 2>&1

done


```




## Contribuer au Dtransfert
<!--- If your README is long or you have some specific process or steps you want contributors to follow, consider creating a separate CONTRIBUTING.md file--->
To contribute to Dtransfert, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.



## Contributeurs

* [@overider](https://gitlab.com/overider) 📖




## Contact

If you want to contact me you can reach me at emerik.nicole@gmail.com.

## License

Ce projet utilise la licence [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/#)
