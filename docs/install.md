
# standard d'installation
## /USR
Le code de l'application se trouveront dans le répertoire /USR/dtransfert/app.
Le fichier de configuration se trouvera /USR/dtransfert/circusd/circus.ini. 

## /OPT
Les binaire python de l'application (virtualenv) se trouveront dans /OPT/dtransfert. 

## /VAR
Les log de l'application se trouveront dans /VAR/dtransfert
La base de donnée se trouvera par défaut dans PDGATA=/VAR/pg_dtransfert/. Sauf s'il est modifié voir plus bas. 

# architecture
## <10TO
En deça de 10 TO de données à copier, voici les spécifications recommandé.

Le [Service : Base de donnée/Postgresql](conception.md#service-:-base-de-donnée/postgresql), [Service : Agent de message/Rabbitmq](conception.md#service-:-agent-de-message/rabbitmq) et le [Composant : Controller plane](conception.md#composant-:-controller-plane) peuvent être installé sur une meme machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 8G
 * DISK DATA: 50G

Le [Composant : Runner plane](conception.md#composant-:-runner-plane) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 4G
 * DISK DATA: 50G


## >10TO <250TO ou si [Composant : Runner plane](conception.md#composant-:-runner-plane) est installé sur plus de 5 machines.
Entre 10 TO et 250 To de données à copier, voici les spécifications recommandé.

Le [Service : Base de donnée/Postgresql](conception.md#service-:-base-de-donnée/postgresql) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 8G
 * DISK DATA: 200G

Le [Service : Agent de message/Rabbitmq](conception.md#service-:-agent-de-message/rabbitmq) et le [Composant : Controller plane](conception.md#composant-:-controller-plane) peuvent être installé sur une meme machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 4G
 * DISK DATA: 50G

Le [Composant : Runner plane](conception.md#composant-:-runner-plane) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 4G
 * DISK DATA: 50G

## >250TO 
Le [Service : Base de donnée/Postgresql](conception.md#service-:-base-de-donnée/postgresql) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 16G
 * DISK DATA: 300G

Le [Service : Agent de message/Rabbitmq](conception.md#service-:-agent-de-message/rabbitmq) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 4G
 * DISK DATA: 50G

Le [Composant : Controller plane](conception.md#composant-:-controller-plane) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 4G
 * DISK DATA: 50G

Le [Composant : Runner plane](conception.md#composant-:-runner-plane) doit être installé sur une machine disposant de ces ressources :
 * VCPU : 4  
 * RAM : 4G
 * DISK DATA: 50G


## Autres conseils et constatations:
### Ajout d'une machine [Composant : Runner plane](conception.md#composant-:-runner-plane)
L'ajout d'une machine runner ajoute ~ 20 connexions concurrentes vers la base de données avec les réglages de base (WORKER_numprocessmax=4,WORKER_numconcurrency=4)



# Procédure
1. cloner le dépot git sur la machine controller dans /VAR 
```
root@controller# cd /VAR
root@controller# git clone https://gitlab.com/e.nicole/Dtransfert.git
root@controller# cd /VAR/Dtransfert/
root@controller# git checkout tags/1.0.0 -b release-1.0.0
```

2. Pour installer les différentes briques de cette application, un script est disponible dans [deploy/install.sh](deploy/install.sh).
Un fichier exemple de paramétrage est disponible [deploy/export_env.sh-sample](deploy/export_env.sh-sample).
Copier ce fichier en tant que deploy/export_env.sh et remplissez le avec les informations relatives à votre environement.

3. Executer le script sur les différentes machines en répétant le paramètre -r pour chacun des rôles que vous souhaitez voir porter par la machine. 
Les différents rôles disponibles sont : 
  - controller
  - postgresql
  - rabbitmq
  - runner

Dans l'idéal, il faut installer les rôles dans l'ordre décrits ci-dessus.

```
root@controller# cd /VAR/Dtransfert/
root@controller# deploy/install.sh -r controller
```

4. Initialiser la base
```
dtransfert@controller# cd /USR/dtransfert/app/
dtransfert@controller# /OPT/dtransfert/bin/python manage.py db upgrade
```

5. Adapter /USR/dtransfert/app/src/api.py à vos besoins en décommentant les blocs qui vous sont ncéssaires. 
Si vous souhaitez que la sante de la plateforme source et destination soit évalué, il faut décommenter les blocs ayant pour clés snmpsource_beat et snmptarget_beat.
Il faut avoir initialiser snmp en conséquence sur ces plateformes.

6. Poster sur le endpoint http://<controller>/healthcheckparams l'ensemble des [paramères](params-snmp.json) paramètre par paramètre en les adaptant à votre environement. notament SNMP_serversource, SNMP_servertarget si vous souhaitez que la sante de la plateforme source et destination soit évalué

7. Assurez vous du bon fonctionnement de l'application sur le controller une fois que le role postgresql et rabbitmq sont installés (voir log /VAR/dtransfert/app/log/) 

8. Copier le repertoire /VAR/Dtransfert sur les autre machines et éxécuter le script d'installation avec le rôle que vous souhaitez voir porter par la machine. 


# Pilotage
Pour piloter une accélération du processus de copie, vous pouvez implémenter des taches planifiés en jouant sur le paramètre WORKER_numprocessmax depuis le controller
```
cat shrink_WORKER_numprocessmax.json
{
    "value": "4",
    "description": "Nombre de process maximal par machine runner (entier) utilisé dans la tache manageworkersconcurencies "
}

curl -X POST --data @shrink_WORKER_numprocessmax.json --user "login:password" -H "Content-Type: application/json; charset=UTF-8" http://<controller>/healthcheckparams/WORKER_numprocessmax

cat grow_WORKER_numprocessmax.json
{
    "value": "8",
    "description": "Nombre de process maximal par machine runner (entier) utilisé dans la tache manageworkersconcurencies "
}

curl -X POST --data @grow_WORKER_numprocessmax.json --user "login:password" -H "Content-Type: application/json; charset=UTF-8" http://<controller>/healthcheckparams/WORKER_numprocessmax
```

# Troubleshoot
## [Service : Base de donnée/Postgresql](conception.md#service-:-base-de-donnée/postgresql) 

## [Service : Agent de message/Rabbitmq](conception.md#service-:-agent-de-message/rabbitmq)

## [Composant : Controller plane](conception.md#composant-:-controller-plane) 

## [Composant : Runner plane](conception.md#composant-:-runner-plane) 


