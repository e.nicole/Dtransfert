
# L’architecture
## Synthèse de la solution
### Schéma de principe

![Schéma de principe](schema-principe.png "Schéma de principe")

## Composant, Service, schéma applicatif & middleware
### Schéma applicatif
![Schéma applicatif](brique-applicative.png "Schéma applicatif")


#### Composant : Controller plane
Le composant controller plane porte la fonctionnalité API,vérification de la bonne santé. base de donné et message broker.
#### Composant : Runner plane
Le composant runner plane réalise la copie et la vérification de l'intégrité des fichiers copiés.
#### Service : Base de donnée/Postgresql
Le service base de donnée est la partie restfull de l'application. Elle stocke l'état des transferts et des indicateurs de bonne santé.
#### Service : Agent de message/Rabbitmq
Le service agent de message (ou message broker) permets la transmission des ordres de copie de la partie agent de copie et des ordres
d'interrogations.
#### Service : Applicative/dtransfert
La partie applicative est développé en python 3.
##### Fonctionnalité API.
L'API REST s'appuie sur la librairie python flask et flask-restfull. Cette API permets la consultation de l'état et le suivi des transferts et des
indicateurs de bonne santé.
##### Fonctionnalité Vérification de bonne santé (healthchecking).
La fonctionnalité de healthchecking s'appuie sur la librairie python celery. Elle interroge l'api geos de Scality ainsi que des indicateurs SNMP de la
plateforme source, destination et runner plane pour orchestrer la copie. Cette partie est opéré sous forme de taches planifiées intégrés à
l'application.
##### Fonctionnalité Agent de copie.
La fonctionnalité d'agent de copie s'appuie sur la librairie python celery. Elle consomme les message de taches générés par la partie API.##### Fonctionnalité Communication instantanée . (xmpp/jabber/spark)
Lorsque l'agent de copie est démarré, arrêté ou reconfiguré une message instantanée est envoyé sur une conférence xmpp et à l'utilisateur si
celui ci le souhaite. Lorsque qu'un job n'a pas évolué depuis plus de 30 minutes, un message instantanée est également envoyé.

### Architecture physique
#### Matériel
NA.
L'ensemble des machines créer pour ces migrations de données sont des machines virtuelles. Elles ont vocation à disparaitre une fois la
migration réalisée
#### Liaisons

#### Schéma physique
NA
#### Plans de mise en baie et plan de salle
NA
### Architecture logique

### Schéma logique
NA. 
### Logiciels et applicatifs
Les composant controller et worker plane sont réputés supportés sur centos 7
#### Postgresql12
 - Emplacement du binaire : /usr/pgsql-12/bin/postmaster
 - Source des binaires : yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm ; yum install postgresql12-server
 - Affichage du numéro de version : /usr/pgsql-12/bin/postmaster --version
 - Version : postgres (PostgreSQL) 12.1
 - Méthode de démarrage auto : SystemD /
 - Configuration de démarrage : /etc/systemd/system/postgresql-12.service
 - Démarrage manuel : systemctl start postgresql-12.service
 - Arrêt manuel :systemctl stop postgresql-12.service
 - Reload : NA
 - Configuration : ${PGDATA}/pg_hba.conf
 - Log :  ${PGDATA}/log

#### Rabbitmq/message broker
 - Emplacement du binaire : /usr/sbin/rabbitmq-server
 -  Source des binaires : rpm --import https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey ; curl -s https://packagecloud.io/install/repos
itories/rabbitmq/rabbitmq-server/script.rpm.sh | sudo bash ; yum install -y rabbitmq-server ; curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | sudo bash
 - Affichage du numéro de version : yum list rabbitmq-server
 - Version : rabbitmq-server.noarch 3.8.2-1.el7
 - Méthode de démarrage auto : SystemD
 - Configuration de démarrage : /etc/systemd/system/multi-user.target.wants/rabbitmq-server.service
 - Démarrage manuel : systemctl start rabbitmq-server
 - Arrêt manuel : systemctl stop rabbitmq-server
 - Reload : NA
 - Configuration : /etc/systemd/system/rabbitmq-server.service.d/limits.conf & rabbitmqctl
 - Log : /var/log/rabbitmq

#### superviseur circusd pour dtransfert
 - Emplacement du binaire :/OPT/dtransfert/bin/
 - Source des binaires : pip
 - Affichage du numéro de version : /OPT/dtransfert/bin/ pip list | grep circus
 - Version : -circus 0.16.1
 - Méthode de démarrage auto : SystemD
 - Configuration de démarrage : /etc/systemd/system/multi-user.target.wants/rabbitmq-server.service
 - Démarrage manuel : systemctl start circusd
 - Arrêt manuel : systemctl stop circusd
 - Reload : NA
 - Configuration : /USR/dtransfert/circusd/circus.ini
 - Log : /VAR/dtransfert/log/circus.log

#### Frontal Web/reverse proxy sur le controller
 - Emplacement du binaire : /usr/sbin/nginx
 - Source des binaires : yum install nginx-1.16.1-1.el7.ngx.x86_64.rpm
 - Affichage du numéro de version : /usr/sbin/nginx -v
 - Version :nginx version: nginx/1.16.1
 - Méthode de démarrage auto : SystemD
 - Configuration de démarrage : /usr/lib/systemd/system/nginx.service
 - Démarrage manuel : systemctl start nginx
 - Arrêt manuel : systemctl stop nginx
 - Reload : NA
 - Configuration : /etc/sysconfig/nginx & /etc/nginx/nginx.conf & /etc/nginx/ conf.d/controller.conf
 - Log : /var/log/nginx/

#### Tcp reverse proxy sur les runner
 - Emplacement du binaire : /usr/sbin/nginx
 - Source des binaires : yum install nginx-1.16.1-1.el7.ngx.x86_64.rpm
 - Affichage du numéro de version : /usr/sbin/nginx -v
 - Version :
 - nginx version: nginx/1.16.1
 - Méthode de démarrage auto : SystemD
 - Configuration de démarrage : /usr/lib/systemd/system/nginx.service
 - Démarrage manuel : systemctl start nginx
 - Arrêt manuel : systemctl stop nginx
 - Reload : NA
 - Configuration : /etc/sysconfig/nginx & /etc/nginx/nginx.conf
 - Log : /var/log/nginx/

#### Postman
 - Emplacement du binaire : NA
 - Source des binaires: https://www.postman.com/downloads/
 - Affichage du numéro de version : NA
 - Version : La dernière disponible
 - Méthode de démarrage auto : NA
 - Configuration de démarrage : NA
 - Démarrage manuel : NA
 - Arrêt manuel : NA
 - Reload : NA
 - Configuration : NA
 - Log : NA

Il s'agit d'un client pour API rest à installer sur les machines d'administration sous windows.


### Paramétrage
NA
#### Focus sur l'applicatif, l'API et les tâches en background
La documentation de l'api est auto hebergé par l'applicatif à l'adresse http://<controlleur>/apidocs/
Voici un résumé de l'api et de ses fonctionalités
| Fonctionnalité                      | endpoint associé | Description              | Verbe possible | base associé | table associé | tache associé |
| ----------------------------------- | ---------------- | ------------------------ | -------------- | ------------ | ------------- | -------------- |
| auth API : gestion des utilisateurs | /users           | Gestion des utilisateurs habilités à se connecter a l'api | GET,POST,PUT   | dtransfert   |users         | NA |
| auth API : gestion des utilisateurs |  /auth/token     | Création d'un token pour utilisation dans l'api| GET |  dtransfert | NA | NA | NA |
| API : Agent de copie. |  '/transferts','/transferts/','/transferts/<int:id>','/transferts/<int:id>/' '/transferts/<string:uuid>','/transferts/<string:uuid>/' | Permets de gérer les transferts par leur id ou leur uuid |  GET,POST,PUT | dtransfert | transferts | tasks_transfer |
| API : Agent de copie. | '/jobs/<int:id>/transferts', '/jobs/<string:jobname>/transferts' |  Affiche les transferts associés à un job specifie par son id ou son nom | GET | dtransfert | jobs, transferts | 
| API : Agent de copie. | '/jobs/<int:id>/status', '/jobs/<int:id>/status/', '/jobs/<int:id>/status/<string:labelstatus>', '/jobs/<int:id>/status/<string:labelstatus>/', '/jobs/<string:jobname>/status', '/jobs/<string:jobname>/status/','/jobs/<string:jobname>/status/<string:labelstatus>' |  Affiche le resumé d'un job selon le statut des transferts par id ou nom de job. La liste des transferts dans ce statut est affiché si le statut est précisé |  GET | dtransfert | jobs, transferts | 
| API : Agent de copie. | '/jobs/<int:id>/summarize', '/jobs/<string:jobname>/summarize |  Affiche le resumé d'un job. Force le recalcul du résumé | GET |dtransfert | jobs, transferts | |
| API : Vérification de bonne santé (healthchecking) | '/healthcheckhistory','/healthcheckhistory/','/healthcheckhistory/<string:uuid>','/healthcheckhistory/<string:uuid>/','/healthcheckhistory/<int:id>','/healthcheckhistory/<int:id>/')  | Affiche l'historique des indicateurs | GET | dtransfert | hist_healthcheck | |
| API : Vérification de bonne santé (healthchecking).  | '/healthcheck','/healthcheck/', '/healthcheck/<string:uuid>','/healthcheck/<string:uuid>/','/healthcheck/<int:id>','/healthcheck/<int:id>/'|  Affiche la valeur actuelle des indicateurs | GET,POST,DELETE | dtransfert | healthcheck |
| API : Vérification de bonne santé (healthchecking).  | '/healthcheckparams', '/healthcheckparams/', '/healthcheckparams/<string:key>', '/healthcheckparams/<string:key>/' | Permets de définir le paramétrage de la partie healthcheck. | GET, POST, PUT, DELETE | dtransfert | params | 
| Celery : Worker Vérification de bonne santé | | Interrogation des indicateurs de bonnes santés | NA | NA | NA |
| Celery : Worker | NA | NA | NA | dtransfert_result | NA | NA| 
|  Documentation de l'api | /docs | NA| NA| NA| NA| NA|


#### Focus sur les taches de healthchecking : tasks_healthcheck .*
Les taches de healthchecking sont planifiés au sein de l'application elle même grâce à celery-beat. Ces taches enrichissent indépendamment
l'une de l'autre la table healthcheck, et hist_healthcheck.Chacune de ces tâches a un (ou plusieurs) point d'entrée dans l'entité
healthcheckparams qui lui permets de pointer vers divers points de supervision de même nature.

| Nom de la tache de healthchecking | Fréquence | description du fonctionnement | params point d'entrée | params source d'information|
| --------------------------------- | --------- | ----------------------------- | --------------------- | -------------------------- |
| tasks_healthcheck.geosmetrics     |Toutes les 5 minutes | c'est une tâche qui est lié à la vérification du fonctionement de la santé de geos (exemple de retour de l'API geos sfullsync plus bas). Elle compare une metrique avec une valeur max/min. Si la métrique dépasse la valeur max, le healthcheck passe à un statut 'stop' . Elle ne repassera a un statut 'start' que lorsque la métrique sera en deca de la valeur min. La plateforme est la plateforme de stockage Scality sur CLY. |  GEOS_metrics |  GEOS_URL | 
| tasks_healthcheck.geosindicators | Toutes les 5 minutes |  c'est une tâche qui est lié à la vérification du fonctionnement de la santé de geos (exemple de retour de l'API geos sfullsync plus bas). Elle compare un indicateur geos avec un statut attendu. Si un indicateur n'est pas dans l'etat attendu, le healthcheck passe à un statut 'stop'. Lorsque l'indicateur reviens à l'état attendu le helthcheck respasse à un
statut 'start'. La plateforme est la plateforme de stockage Scality sur CLY. | GEOS_indicators | GEOS_URL |
|tasks_healthcheck.snmpsourcehealthcheck | Toutes les 5 minutes | c'est une tâche qui est lié à la vérification de la plateforme source de
copie (dit ancienne plateforme). | SNMP_labelsoids | SNMP_serversource |
|tasks_healthcheck.snmptargethealthcheck | Toutes les 5 minutes | c'est une tâche qui est lié à la vérification de la plateforme destination de copie (dit plateforme de stockage Scality sur MRS).| SNMP_labelsoids | SNMP_servertarget |
| tasks_healthcheck.snmpworkershealthcheck | Toutes les 5 minutes | c'est une tâche qui est lié à la vérification des runner.| SNMP_labelsoids  |dynamique selon les runners connecté au message broker |
|tasks_healthcheck.manageworkersconcurencies | Toutes les 5 minutes | C'est la tache qui permets l'ordonnancement des runner de copie. Cette tache interroge les points de surveillance précedement remonté et evalue si ce runner spécifique doit continuer à fonctionner et à quel
rythme. | WORKER_pacesnumprocess WORKER_numprocessonstart WORKER_numprocessmax WORKER_loadratiomargin WORKER_freemem |dynamique selon les  runners connectés au message broker |


#### Paramétrage de la vérification de bonne santé.
verbe : GET. uri : healthcheckparams
La liste exhaustive des paramètres ainsi que leur description est disponible aux coordonnées dans le fichier 
J’attire l'attention sur certains paramètres.
| representation json | URL |  type |  description |
| ------------------- | --- | ----- | ------------ | 
| ```{ "key": "WORKER_pacesnumprocess", "value": "2", "description": "Pas pour l'ajout ou le retrait au nombre de process. (entier) utilisé dans la tache manageworkersconcurencies "} ``` |  healthcheckparams/WORKER_pacesnumprocess | entier |  Il s'agit du pas appliqué par le programme quand il estime que les indicateurs de santé révèle un besoin de réguler le nombre de taches concurrentes traiter par un runner. Chaque test d'indicateur de santé peut impliquer un ajout ou un retrait de la valeur de ce paramètre au nombre de taches concurrentes actuelle.|
| ``` { "key": "WORKER_numprocess", "value": "auto","description": "Comportement de l'application en rapport avec la régulation du nombre de tachesconcurentes traités. peut valoir 'off','auto','entier' " } ```| healthcheckparams/WORKER_numprocess |  polymorphe |  Ce paramètre peut prendre différente valeur. 'auto' : l'application prends en compte les différents indicateurs de santé (SNMP) pour calculer le nombre de taches concurentes adéquates. 'off' : l'application ne prends pas en compte les différents indicateurs de santé (SNMP). le champ value du point de santé ayant pour source 'WORKER' et pour name 'numprocess' est considéré comme nombre de taches concurentes.Dans ce mode, le nombre de taches concurrentes est laissé à l'appréciation de l'exploitant de l'application pour chacun des worker.  un entier : cet entier sera définit comme étant le nombre de taches concurrentes pour chacun des worker. |

et sur certains indicateurs de santé

| representation json exemple | URL exemple | Explication de l'exemple |
| --------------------------- | ----------- | ------------------------ | 
| ``` {"id": 197, "uuid": "d1cc3deb-4323-4e5b-9856-20402463b226", "source": "WORKER", "name": "numprocess", "status": "start","value": "16" , "reason": " status:start:workerbehaviour:auto:currentmaxconcurency:4:nextmaxconcurency:16:action:grow:paces:2 because of ['fe906a58-790e-4be8-b616-aa5ace0390c4', 'c33cb276-ce92-42fa-bed7-4c9634110679', 'e1d1f6bc-82c4-4689-9b85-c89f441d0f97', 'c2c12291-4a18-4b95-933a-8e8e6eb7d9d4','0d613a14-a43a-4570-aef3-f85bc528584a', '4e94dcbf-b9af-441f-91f5-e9ab007cb253']" ,"element": "worker1", "created_on": "Tue, 04 Feb 2020 12:27:23-0000", "updated_on": "Tue, 04 Feb 2020 12:27:23 -0000" } ```  | healthcheck/d1cc3deb-4323-4e5b-9856-20402463b226  | pour le runner worker1 , le comportement de l'application est définit sur auto. L'application a définit que ce worker devait être en fonctionement. Le nombre de taches concurentes etait de 4 et a du augm enté jusqu'a 16. L'application a calculé le nombre de taches concurrentes grace aux indicateurs de santé ayant les uuid suivants ['fe906a58-790e-4be8-b616-aa5ace0390c4', 'c33cb276-ce92-42fa-bed7-4c9634110679', 'e1d1f6bc-82c4-4689-9b85-c89f441d0f97','c2c12291-4a18-4b95-933a-8e8e6eb7d9d4', '0d613a14-a43a-4570-aef3-f85bc528584a', '4e94dcbf-b9af-441f-91f5-e9ab007cb253']

#### Focus sur les taches de transfert : tasks_transfer.*
La fonctionnalité de transfert est découpé en plusieurs tâches. C'est l'enchainement de ces tâches qui réalise le transfert. L'échec d'un des taches
provoque la non-exécution des tâches suivantes. Le résultat de chacune des taches est utilisé dans la taches suivante. Le champ step de la
représentation json d'un transfert est mis à jour de taches en taches.
| Nom de la tache | position dans l'enchainement | description  |
| --------------------------- | ----------- | ------------------------ | 
|tasks_transfer.broadening |1| enrichit les paramètres de la tache avec le nom absolu du fichier source et le nom absolu du fichier destination.|
|tasks_transfer.checksumsource |2| enrichit les paramètres de la tache avec le checksum du fichier source. l'algorithme utilisé est un paramètre du job.
|tasks_transfer.mkdir |3| crée le répertoire destination parent au besoin.|
|tasks_transfer.copy |4| il s'agit de la copie du fichier.|
|tasks_transfer.checksumtarget |5| enrichit les paramètres de la tache avec le checksum du fichier destination.l'algorithme utilisé est un paramètre du job.|
|tasks_transfer.comparechecksum| 6| compare les checksum du fihcier source et du fichier destination. Si le checkum est identique les transfert est noté comme OK, sinon le fichier destination est supprimé et le transfert devra être relancé manuellement. |
|||
|tasks_transfer.job_summarize || il s'agit d'une tache qui ne contribue pas à la copie de fichier. elle permets de compter le nombre de transfert par statut pour un job. Elle est lancé toutes les heures sur les job dont la propriété enable_summary est à true. ou la demande via une requete de type get sur l'enpoint d'un job. |
|tasks_transfer.alert_for_dead_job| |il s'agit d'une tache qui ne contribue pas à la copie de fichier. elle permets de signaler lorsqu'un job n'a pas été mis a jour depuis un temps |

## Les Flux
| Source | Destination | ProtocoleNom/UDP-TCP/No | Commentaire  Indiquer le fonctionnel |
| ------ | ----------- | ----------------------- | -------------------------------------|
|controller| rabbitmq| AMQP (TCP 5672)| Flux permettant le transport des message de taches|
|runner |rabbitmq| AMQP (TCP 5672)| Flux permettant le transport des message de taches|
|controller |Postgresql |SQL (TCP 5432)| Flux permettant l’enregistrement de l’état des |
|runner |rabbitmq SQL |(TCP 5432) |Flux permettant le transport des message de taches|
|Apache frontal| controller plane| TCP 5000| Flux local permtteant l'exposition de l'api au travers d'un frontal apache|
| Poste admin | Apache frontal | TCP 80 (443)|  Flux d'administration, soumission de job, suivi et monitoring |
| Poste admin | rabbitmq | HTTP (TCP 15672) | Flux d'administration de la partie rabbitmq|
| controller | source de copie (Ancienne plateforme de stockage )| SNMP (TCP 161 ) | Flux permettant l'orchestration de la copie|
| controller | target de copie connecteur source Scality sur MRS | SNMP (TCP 161 ) | Flux permettant l'orchestration de la copie|
| controller | runner | SNMP (TCP 161 ) | Flux permettant l'orchestration de la copie|
| controller |target de copie connecteur target Scality sur CLI HTTP (TCP 8381)|  Flux permettant l'orchestration de la copie|
| controlleur| jabber | XMPP (TCP 5222) | Communication et suivi par messagerie instantanée |

## Routage

## Sécurité
### Disponibilité, Confidentialité et Intégrité
NA
### Risques de sécurité

## Filtrage et Matrice de flux
NA

## Conformité à la PSSI/PSO
NA
## Autres principes mis en œuvre
NA
## Incompatibilités
NA
## Impact sur l’architecture existante
NA

# Administration
## Télémaintenance
L’administration de la solution dtransfert peut se faire depuis un poste de travail grâce à une session SSH ou l’interface graphique. Le mode
actuel d’accès aux outils ou interfaces d’administration des composants du service de partage est « Direct ».
Deux types sont possibles
SSH pour les opérations de configuration via ligne de commande
HTTP pour les opérations configuration via l(es) interfaces WEB
## Synchronisation horaire
La synchronisation horaire se fait en utilisant le protocole NTP au travers du service ntpd installés sur les machines. 
## AAA
L’authentification sur les composants (Linux) de la solution « dtransfert » est réalisé au moyen d’un compte AD nominatif disposant du droit
d’élévation en mode « root ».
L’authentification sur les composants Web de la solution « dtransfert » est réalisé au moyen de compte générique locaux à la solution.
## Supervision
La supervision OS est assurée par l’outil ITM.
## Logs
NA


# Exploitation
## Conception
 NA
## Réalisation
 NA
## Gestion des accès
 NA 
##  Gestion des configurations
NA 

## Gestion des versions
NA

## Compétences techniques
On recommande de disposer d’une compétence 
 - en administration de système Linux CENTOS 
 - Etre familiarisé avec l'utilisation de Postman
 - Connaitre git et son utilisation est un plus
 - Connaitre le principe de l'API REST est un plus.

## Gestion de la performance
NA
## Gestion de la capacité
Un minimum de 4VCPU et 4 go de RAM pour les machines controller, runner, postgresql et rabbitmq sont nécessaires. Selon les cas d'usage, les
capacités des machines pourront être revu

## Gestion de la supervision
La gestion de la supervision est sous la responsabilité des équipes d'exploitation cependant au vu de l'utilisation de ces machines les supervision de
base pourront/devront être désactivé sur ces machines.

## Gestion d’incident et problème
NA 

## Gestion des changements
NA 

## Gestion des sauvegardes
Une sauvegarde quotidienne des VM est acceptable sur cette brique.

## Veille sécuritaire
NA 
## Maintenance
Aucune particularité sur ce point.
## Mode Opératoire
Liste des modes opératoires d’exploitation. Préciser le cadre de leurs exécutions.
 * [MOP - Utilisation du wrapper](how-to/use-wrapper.md)
 * [MOP - Arréter/redémarer un worker/runner](how-to/stop-worker.md)
 * [MOP - Créer/Relancer/Suivre un job de copie par la ligne de commande](how-to/manage-copy-job.md)
 * [MOP - Créer un utilisateur capable de se connecter à l'api de l'outils de migration de données](how-to/create-api-user.md)
 * [MOP - désactiver la sommarization pour un job](how-to/disable-summarize.md)
 * [MOP - Gestion des erreurs de copie](how-to/known-error.md)
 

## Test de perf

## Conseils pratiques
Les points de montage doivent monter au redémarrage de la machine.

fichier automount source :
```# /etc/systemd/system/VAR-dtransfert-source.automount
[Unit]
Description=AutoMount de la source
Requires=network.target
[Automount]
Where=/VAR/dtransfert/source/
[Install]
WantedBy=multi-user.target
```

fichier mount source :
```# /etc/systemd/system/VAR-dtransfert-destination.mount
[Unit]
Description=Mount de la source
Requires=network.target
[Mount]
Where=/VAR/dtransfert/source/
What=#{source}:/#{share source}
Options=_netdev,sec=sys,hard,nointr,noexec
Type=nfs
TimeoutSec=5s
[Install]
WantedBy=multi-user.target
```

fichier automount destination:
```# /etc/systemd/system/VAR-dtransfert-destination.automount
[Unit]
Description=AutoMount de la destination
Requires=network.target
[Automount]
Where=/VAR/dtransfert/destination/
[Install]
WantedBy=multi-user.target
```
fichier mount destination:
```# /etc/systemd/system/VAR-dtransfert-destination.mount
[Unit]
Description=Mount de la destination
Requires=network.target
[Mount]
Where=/VAR/dtransfert/destination/
What=#{destination}:/#{share destination}
Options=_netdev,sec=sys,hard,nointr,noexec
Type=nfs
TimeoutSec=5s
[Install]
WantedBy=multi-user.target
```

Tous les runner doivent monter la racine source et la racine destination au même endroit.Pour ce faire l'utilisation d'un IaC est conseillé.