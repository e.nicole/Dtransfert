MOP - Arréter/redémarer un worker/runner
# Introduction
Le mode opératoire suivant permets d’arrêter un worker.
Ces éléments sont à adapter en fonction du contexte et du DAT.
# Mode opératoire
## Stopper l’exécution du service
Pour appliquer ce mode opératoire, un compte ssh permettant de ses connecter à la machine runner est nécéssaire.
```
systemctl stop circusd
```
## Bridage applicatif du worker
Il est également possible d'arreter l'execution de tache par un worker de maniere applicatif. Pour ce faire il fait envoyer à l'api le payload suivant :
```
#verb : post, uri : /healthcheck
{
"source": "WORKER",
"name": "manual",
"status": "stop",
"value": "xx",
"reason": "stop by <utilisateur>",
"element": "<runner> ",
}
```
remplace
 * <runner> par le nom court de la machine runner que vous souhaitez stopper.
 * <utilisateur> par votre nom/prènom/identifiant.
L'api va vous répondre quelque chose ressemeblant à ceci :
```{
"id": 197,
"uuid": "d1cc3deb-4323-4e5b-9856-20402463b226",
"source": "WORKER",
"name": "numprocess",
"status": "start",
"value": "16",
"reason":
"status:start:workerbehaviour:auto:currentmaxconcurency:4:nextmaxconcurenc
y:16:action:grow:paces:2 because of
['fe906a58-790e-4be8-b616-aa5ace0390c4',
'c33cb276-ce92-42fa-bed7-4c9634110679',
'e1d1f6bc-82c4-4689-9b85-c89f441d0f97',
'c2c12291-4a18-4b95-933a-8e8e6eb7d9d4',
'0d613a14-a43a-4570-aef3-f85bc528584a',
'4e94dcbf-b9af-441f-91f5-e9ab007cb253']",
"element": "worker1",
"created_on": "Tue, 04 Feb 2020 12:27:23 -0000",
"updated_on": "Tue, 04 Feb 2020 12:27:23 -0000"
}
```

Lorsque vous souhaitez redémarer le worker, il suffit d'envoyer #verb : delete, uri : /healthcheck/197