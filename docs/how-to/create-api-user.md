MOP - Créer un utilisateur capable de se connecter à l'api de
l'outils de migration de données
# Introduction
Le mode opératoire suivant permets de créer un utilisateur capable de se connecter à l'API de migration de données.
# Mode opératoire
En tant qu'utilisateur applicatif dtransfert, il faut éxécuter le script /USR/dtransfert/app/src/createuser.py sur la machine controller avec les bonnes
options.
ex :
```
dtransfert@controller# source /OPT/dtransfert/bin/activate
dtransfert@controller# cd /USR/dtransfert/app/src 
dtransfert@controller# /OPT/dtransfert/bin/python createuser.py -u user -p admin123 -e user@domain.com -pg user@domain.com
```
créer un utilisateur ayant pour :
nom d'utilisateur : user
mot de passe : admin123
email : user@domain.com
pager: user@domain.com
# Remarques
 * Pour recevoir les notifications spark directement, il faut que le champs pageme soit à true. Ce n'est pas le cas par défaut. Pour le passer
à true, il faut identifier l'id de votre utilisateur.
   * verb : GET, uri : /users. Conserver le champ <id>, <username>, <email>, <pager>
   * verb : PUT, uri : /users/<id>. payload :
```
{
"username": "<username>",
"email": "<email>",
"pager": "<pager>",
"pageme": true
}
```
sinon vous pouvez vous connecter au salon de conférence.
 * Le champ <pager> (parametre pg de createuser.py ) est toujours de la forme<compte_utilisateur_prod>@s401lxjabber01.production-real.fr