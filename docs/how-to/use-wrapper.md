MOP - Utilisation du wrapper

# Introduction
Le mode opératoire suivant permets de créer une collection de job par rapport à un point d'entrée et une profondeur de répertoire.
Une mécanisme permets de s'assurer que le job est terminé avant de passer au prochain.
# Contexte d'utilisation
Pour executer ce mode opératoire, il faut se connecter en ssh à une machine qui dispose : 
- de l'application dtransfert déployé selon le [mode opératoire prévu](../install.md)
- d'un montage sur la source de copie dit ancienne plateforme de stockage
- d'un montage sur la destination de copie dit nouvelle platefome

Un runner de copie remplit l'ensemble de ces contraintes et donc tout indiqué pour réaliser ce mode opératoire

# Mode opératoire et mise en situation
## Utilisation
 * -s|--source : repertoire source de copie
 * -t|--target : repertoire destination de copie
 * -f|--prefix : prefix pour la collection de job crée
 * -e|--endpoint : l'url du controller
 * -d|--depth : profondeur de création d'un job.
 * -c|--checksum : l'algorithme de hash à utiliser pour vérifier la cohérencedes données. valuer par défaut 'SHA-256'. valeur possible 'MD5','SHA-256'
 * -u|--username : utilisateur crée dans l'api
 * -p|--password : le mot de passe de l'utilisateur
 * -i|--maxiteration : nombre d'iteration avant de relancer le job

## Mise en situation
Imaginons une arborescence /VAR/source/YYYY/MM/DD que l'on souhaite copier vers /VAR/target/.
Cette arborescence est large avec plusieurs millions de fichier en feuille (sous /VAR/source/YYYY/MM/DD ).
Nous souhaitons créer un job par /VAR/source/YYYY/MM/DD.
ex :
``` 
/USR/dtransfert/app/src/client-rest/wrapper-local-copy.sh -e http://<controller> -f 2012 -c SHA-256 -u <user> -p <mot_de_passe> -s /VAR/source/2012/ -t /VAR/target/2012/ -d 2 -i 100
```
créer un job par jour pour l'année 2012.
chaque job est préfixé par 2012
toutes les 100 iterations de vérification de job terminé, le job est relancer .