MOP - Gestion des erreurs de copie
# Introduction
Le mode opératoire suivant permets de gérer le cas d'erreur des copies
# Cas d'erreur
## comparechecksum-failed
### Explication
Cela indique une copie qui n'a pas aboutit complétement (cause possible, reboot de runner, du service decopie ... ). Le hash source et le
hash destination ne sont donc pas le même. Il est normalement prévu une procédure de remédiation dans l'application. Cependant, il arrive
que la procédure de remédiation ne peut être joué dans un temps acceptable avant pose du verrou WORM.
Pour résoudre ce soucis, il faut aller supprimer le ficheir en erreur
### Pré-requis
 - Disposer d'un accès a la plateforme de destination
 - Pouvoir passer root sur ces connecteurs.
### Mode opératoire
Se connecter a chacun des connecteurs qui sont en réplication continu.
Supprimer les fichiers concernés par ces messages d'erreurs 
[Relancer la copie](how-to/manage-copy-job.md#relancer-un-job)

## mkdir-failed
### Explication
Cela indique souvent que l'un des runners ne monte pas la target
### Pré-requis
Disposer d'un accès au runner
### Mode opératoire
Se connecter a chacun des runners.
Vérifier les montages
[Relancer la copie](how-to/manage-copy-job.md#relancer-un-job)