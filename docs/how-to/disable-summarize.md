MOP - désactiver la sommarization pour un job
# Introduction
Le mode opératoire suivant permets de désactiver la summarisation sur un job.
# Pré-requis
Disposer d'un accès à l'api.
# Mode opératoire
Se connecter sur l'api.
Et envoyer ceci : verb : PUT , URL: /jobs/<nom_du_job>
```
{
"enable_summary" : false
}
```