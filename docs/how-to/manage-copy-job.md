MOP - Créer/Relancer/Suivre un job de copie par la ligne de commande

# Introduction
Le mode opératoire suivant permets de créer un job de copie ou de le relancer depuis la ligne de commande. Ces éléments sont à adapter en fonction du contexte et du DAT.

# Contexte d’exécution
Pour executer ce mode opératoire, il faut se connecter en ssh à une machine qui dispose :
 - de l'application dtransfert déployé selon le mode opératoire prévu (cf voir ddc)
 - d'un montage sur la source de copie dit ancienne plateforme de stockage
 - d'un montage sur la destination de copie dit connecteur scality source
Un runner de copie remplit l'ensemble de ces contraintes et donc tout indiqué pour réaliser ce mode opératoire

# Mode opératoire
## Création d'un job de copie
```
dtransfert@runner# /OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -s <répertoire_racine_source> -t <répertoire_racine_target> -j <nom_du_job> -e <url_du_controller> -c <algorithme_de_hash> -u <utilisateur_api> -p <mot_de_passe_utilisateur_api> 
```
où :
 * <répertoire_racine_source> est le repertoire source de copi
 * <répertoire_racine_target> est le repertoire destination de copie
 * <nom_du_job> est le petit nom du job de copie sans espace et sans caractere special . Il sera possible d'utiliser ce nom dans l'API pour
suivre l'évolutio
 * <url_du_controller> est l'url du controller pour la déclinaison de l'architecture cf voir DAT.
 * <algorithme_de_hash> est l'algorithme de hash à utiliser pour vérifier la cohérence des données. valuer par défaut 'SHA-256'. valeur
possible 'MD5','SHA-256'
 * <utilisateur_api> est un utilisateur crée dans l'api.
 * <mot_de_passe_utilisateur_api> est le mot de passe de l'utilisateur crée précédemment.

## Relancer un job
```
dtransfert@runner# /OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -j <nom_du_job> -e <url_du_controller> -r -u <utilisateur_api> -p <mot_de_passe_utilisateur_api>
```
où:
 * <nom_du_job> est le petit nom du job de copie sans espace et sans caractere special . Il sera possible d'utiliser ce nom dans l'API pour
suivre l'évolution
 * <url_du_controller> est l'url du controller pour la déclinaison de l'architecture
 * <utilisateur_api> est un utilisateur crée dans l'api.
 * <mot_de_passe_utilisateur_api> est le mot de passe de l'utilisateur crée précédemment.

## Lister les job ou un job spécifique et lister son statut
### Pour lister l'ensemble de job et leur statut
```
dtransfert@runner# /OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -l -e <url_du_controller> -u <utilisateur_api> -p <mot_de_passe_utilisateur_api>
```
### Pour lister le statut d'un job spécifique
```
dtransfert@runner# /OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -l -e <url_du_controller> -j <nom_du_job> -u <utilisateur_api> -p <mot_de_passe_utilisateur_api>
```
où:
 * <nom_du_job> est le petit nom du job de copie sans espace et sans caractere special . Il sera possible d'utiliser ce nom dans l'API pour
suivre l'évolution
 * <url_du_controller> est l'url du controller pour la déclinaison de l'architecture 

## Aide de la ligne de commande
```
dtransfert@runner# /OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -h
```