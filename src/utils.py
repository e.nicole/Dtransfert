# -*- coding: utf-8 -*-

from __future__ import absolute_import
import slixmpp




class SendMsgBot(slixmpp.ClientXMPP):
  def __init__(self, jid, password,msg,nick,room=None,invitations=None,recipients=None):
    super().__init__(jid, password)
    self.room = room
    self.invitations = invitations
    self.recipients = recipients
    self.msg = msg
    self.nick = nick
    self.add_event_handler('session_start', self.start)
    


  async def start(self, event):
    self.send_presence()
    self.get_roster()
    
    if self.room is not None:
      items = await self['xep_0030'].get_items(jid=self.room.split('@')[1],node='',block=True,timeout=30)
      createdroomjid = [ room['jid'] for room in items['disco_items']]
      if self.room not in createdroomjid:
        print('room {} need to be created by and administrator'.format(self.room))
      else:
        self.plugin['xep_0045'].join_muc(self.room,self.nick,wait=True)
        if self.invitations is not None:
          for invitation in self.invitations:
            self.plugin['xep_0045'].invite(self.room,invitation,reason=' {invitation} dans votre profil pageme est à true'.format(invitation=invitation),mfrom=self.boundjid.bare)
 
        self.send_message(mto=self.room,mbody="Bonjour à tous.",mtype='groupchat',mfrom=self.boundjid.bare)
        self.send_message(mto=self.room,mbody=self.msg,mtype='groupchat',mfrom=self.boundjid.bare)
        self.send_message(mto=self.room,mbody="Votre dévoué Bot : {me}. ".format(me=self.boundjid.bare),mtype='groupchat',mfrom=self.boundjid.bare)

    if self.recipients is not None:
      for recipient in self.recipients:
        self.send_message(mto=recipient,mbody="Bonjour {recipient}. ".format(recipient=recipient),mtype='chat',mfrom=self.boundjid.bare)
        self.send_message(mto=recipient,mbody=self.msg,mtype='chat',mfrom=self.boundjid.bare)
        self.send_message(mto=recipient,mbody="Votre dévoué Bot : {me}. ".format(me=self.boundjid.bare),mtype='chat',mfrom=self.boundjid.bare)

    self.disconnect()

