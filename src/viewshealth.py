from __future__ import absolute_import
import os
from pathlib2 import Path
from datetime import datetime
from flask import Flask, request, abort,redirect, url_for,jsonify,make_response,current_app, g
from flask_restful import Resource, reqparse, fields, marshal_with,inputs
from flasgger import swag_from
from time import sleep
import uuid
import json


from models import (db,
                    ParamModel,HealthcheckModel,HealthCheckHistoryModel,
                    ParamSchema,HealthcheckSchema,HealthcheckHistorySchema)

from viewsauth import multi_auth


class ParamsListView(Resource):
  param_fields = {
    'key': fields.String,
    'value' : fields.String,
    'description' : fields.String,
  }
  def __init__(self):
    self.post_parser = reqparse.RequestParser()
    self.post_parser.add_argument('key',
                                type=str,
                                required=True,
                                help='Cle du parametre',
                                location='json'                
    )
    self.post_parser.add_argument('value',
                                type=str,
                                required=True,
                                help='Valeur du parametre',
                                location='json'                
    )
    self.post_parser.add_argument('description',
                                type=str,
                                required=True,
                                help='Description du parametre',
                                location='json'                
    )


  @multi_auth.login_required
  @marshal_with(param_fields)
  @swag_from({
      'tags': ['healthcheck','params'],
      'summary': 'get all params',
      'definitions': {
        'Params': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/Param',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. A list of params",
           'schema' : {
             '$ref' : '#/definitions/Params',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self):
    params = ParamModel.get_all(session=db.session)
    if len(params) == 0:
      params = []
    return params

  @multi_auth.login_required
  @marshal_with(param_fields)
  @swag_from({
      'tags': ['healthcheck','params'],
      'summary': 'create a new  param ',
      'parameters' : [
        { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/Param'}},
      ],
      'responses': {
        200: {
          'description': "Creation is OK. Give newly param data",
           'schema' : {
             '$ref' : '#/definitions/Param',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def post(self):
    args = self.post_parser.parse_args()
    param = ParamModel(key=args['key'],value=args['value'],description=args['description'])
    db.session.add(param)
    db.session.commit()
    return param



class ParamView(Resource):
  param_fields = {
    'key': fields.String,
    'value' : fields.String,
    'description' : fields.String,
  }
  def __init__(self):
    self.put_parser = reqparse.RequestParser()
    self.put_parser.add_argument('value',
                                type=str,
                                required=True,
                                help='Valeur du parametre',
                                location='json'                
    )
    self.put_parser.add_argument('description',
                                type=str,
                                required=True,
                                help='Description du parametre',
                                location='json'                
    )


  @multi_auth.login_required
  @marshal_with(param_fields)
  @swag_from({
      'tags': ['healthcheck','param'],
      'summary': 'get a param by key',
      'parameters' : [
        { 'in': 'path','name': 'key' ,'type': 'string','description':'key of a param' },
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. give the param data ",
           'schema' : {
             '$ref' : '#/definitions/Param',
           }
        },
        404: {
          'description': "A param with this key does not exist ",
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,key=None):
    param = ParamModel.get_one(session=db.session,key=key)
    if not param:
      abort(404,"That param key does not exists")
    return param



  @multi_auth.login_required
  @marshal_with(param_fields)
  @swag_from({
      'tags': ['healthcheck','param'],
      'summary': 'modify a  param',
      'parameters' : [
        { 'in': 'path','name': 'key' ,'type': 'string','description':'key of a param' },
        { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/Param'}},
      ],
      'responses': {
        200: {
          'description': "Modify is OK. give modified param dans",
           'schema' : {
             '$ref' : '#/definitions/Param',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def put(self,key=None):
    args = self.put_parser.parse_args()
    param = ParamModel.get_one(db.session,key=key)
    param.value = args['value'] if args['value'] is not None else param.value
    param.description = args['description'] if args['description'] is not None else param.description
    
    db.session.commit()
    db.session.refresh(param)
    return param

  @multi_auth.login_required
  @marshal_with(param_fields)
  @swag_from({
      'tags': ['healthcheck','param'],
      'summary': 'delete a  param',
      'parameters' : [
        { 'in': 'path','name': 'key' ,'type': 'string','description':'key of a param' },
      ],
      'responses': {
        200: {
          'description': "Delete is OK. Give deleted param data",
           'schema' : {
             '$ref' : '#/definitions/Param',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def delete(self,key=None):
    param = ParamModel.get_one(db.session,key=key)
    db.session.delete(param)
    db.session.commit()
    return param


class HealthCheckListView(Resource):
  healthcheck_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'source':fields.String,
    'name':fields.String,
    'status':fields.String,
    'value':fields.String,
    'reason':fields.String,
    'element':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
  }

  def __init__(self):
    self.post_parser = reqparse.RequestParser()
    self.post_parser.add_argument('source',
                                  type=str,
                                  required=True,
                                  help='Source',
                                  location='json'
    )
    self.post_parser.add_argument('name',
                                  type=str,
                                  required=True,
                                  help='name',
                                  location='json'
    )
    self.post_parser.add_argument('reason',
                                  type=str,
                                  required=True,
                                  help='reason',
                                  location='json'
    )
    self.post_parser.add_argument('element',
                                  type=str,
                                  required=True,
                                  help='element',
                                  location='json'
    )
    self.post_parser.add_argument('status',
                                  type=str,
                                  required=True,
                                  help='status',
                                  location='json'
    )

  @multi_auth.login_required 
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthchecks'],
      'summary': 'create a  healthcheck  Only healthcheck with source=\'WORKER\' and name=\'manual\' are creatable ',
      'parameters' : [
        { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/Healthcheck'}},
      ],
      'responses': {
        200: {
          'description': "Create is OK. Give created healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/Healthcheck',
           }
        },
        409: {
          'description': 'Creation of this healthcheck is unauthorized. Only healthcheck with source=\'WORKER\' and name=\'manual\' are deletable ',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def post(self):
    args = self.post_parser.parse_args()
    if args['source'] != 'WORKER':
      abort(409,"That health check source specified {} is not usable.Only WORKER is usable".format(args['source']))
    if args['name'] != 'manual':
      abort(409,"That health check name specified {} is not usable.Only manual is usable".format(args['name']))
    
    
    healthcheck = HealthcheckModel(
      source=args['source'],
      name=args['name'],
      status=args['status'],
      reason=args['reason'],
      element=args['element'],
    )
    db.session.add(healthcheck)
    db.session.commit()
    return healthcheck

  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthchecks'],
      'summary': 'get all healthchecks',
      'definitions': {
        'Healthchecks': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/Healthcheck',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. A list of HealthChecks",
           'schema' : {
             '$ref' : '#/definitions/Healthchecks',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self):
    """ Retrieve all health check item"""
    healthchecks = HealthcheckModel.get_all(session=db.session)
    if len(healthchecks) == 0:
      healthchecks = []
    
    return healthchecks

class HealthCheckView(Resource):
  healthcheck_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'source':fields.String,
    'name':fields.String,
    'status':fields.String,
    'value':fields.String,
    'reason':fields.String,
    'element':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
  }

  def __init__(self):
    self.put_parser = reqparse.RequestParser()
    self.put_parser.add_argument('status',
                                  type=str,
                                  required=True,
                                  help='reason',
                                  location='json'
    )
    self.put_parser.add_argument('value',
                                  type=str,
                                  required=True,
                                  help='reason',
                                  location='json'
    )
    self.put_parser.add_argument('reason',
                                  type=str,
                                  required=True,
                                  help='reason',
                                  location='json'
    )


  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthcheck'],
      'summary': 'modify a  healthcheck by id or uuid. Only healthcheck with source=\'WORKER\' and name=\'manual\' are modifiable ',
      #'parameters' : [
      #  { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a healthcheck' },
      #  { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a healthcheck' },
      #  { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/Healthcheck'}},
      #],
      'responses': {
        200: {
          'description': "Modify is OK. Give modified healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/Healthcheck',
           }
        },
        404: {
          'description': 'A healthcheck with this id or uuid does not exist',
        },
        409: {
          'description': 'Modification of this healthcheck is unauthorized. Only healthcheck with source=\'WORKER\' and name=\'manual\' are modfifiable.  healthcheck with source=\'WORKER\' and name=\'numprocess\' are modifiable only when healthcheckparams/WORKER_numprocess value is off.',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def put(self,id=None,uuid=None):
    args = self.put_parser.parse_args()
    if id is not None:
      healthcheck = HealthcheckModel.get_one(session=db.session,id=id)
      if not healthcheck:
        abort(404,"That health check item id does not exists")
    elif uuid is not None:
      healthcheck = HealthcheckModel.get_one(session=db.session,uuid=uuid)
      if not healthcheck:
        abort(404,"That health check item uuid does not exists")
    
    if healthcheck.source != 'WORKER':
      abort(409,"That healthcheck specified uuid:{uuid} is not modifiable. Only healthcheck with source = 'WORKER' is accepted. actually : {actual}".format(uuid=healthcheck.uuid, actual=healthcheck.source))

    if healthcheck.name not in ['manual','numprocess']:
      abort(409,"That healthcheck specified uuid:{uuid} is not modifiable. Only healthcheck with name = manual or name = numprocess is accepted. actually : {actual}".format(uuid=healthcheck.uuid,actual=healthcheck.name))

    if healthcheck.name == 'numprocess':
      workerbehaviour = ParamModel.get_one(session=db.session,key='WORKER_numprocess').value
      if workerbehaviour != 'off':
        abort(409,"That healthcheck specified uuid:{uuid} is not modifiable because params WORKER_numprocess != 'off'".format(uuid=healthcheck.uuid,actual=workerbehaviour))
          
    healthcheck.value = args['value']
    healthcheck.status = args['status']
    healthcheck.reason = args['reason']
    db.session.commit()
    db.session.refresh(healthcheck)
    return healthcheck

  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthcheck'],
      'summary': 'delete a  healthcheck by id or uuid. Only healthcheck with source=\'WORKER\' and name=\'manual\' are deletable ',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a healthcheck' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a healthcheck' }
      ],
      'responses': {
        200: {
          'description': "Delete is OK. Give deleted healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/Healthcheck',
           }
        },
        404: {
          'description': 'A healthcheck with this id or uuid does not exist',
        },
        409: {
          'description': 'Deletion of this healthcheck is unauthorized. Only healthcheck with source=\'WORKER\' and name=\'manual\' are deletable ',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def delete(self,id=None,uuid=None):
    if id is not None:
      healthcheck = HealthcheckModel.get_one(db.session,id=id)
    elif uuid is not None:
      healthcheck = HealthcheckModel.get_one(db.session,uuid=uuid)
    else:
      abort(404,'id or uuid is not known : id {id}. uuid {uuid}.'.format(id=id, uuid=uuid))


    if healthcheck.source != 'WORKER':
      abort(409,"That healthcheck specified uuid:{uuid} is not deletable. Only healthcheck with source = 'WORKER' is accepted. actually : {actual}".format(uuid=healthcheck.uuid, actual=healthcheck.source))

    if healthcheck.name not in ['manual']:
      abort(409,"That healthcheck specified uuid:{uuid} is not deletable. Only healthcheck with name = manual  is accepted. actually : {actual}".format(uuid=healthcheck.uuid,actual=healthcheck.name))

    db.session.delete(healthcheck)
    db.session.commit()
    return healthcheck


  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthcheck'],
      'summary': 'get a healthcheck by id or uuid',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a healthcheck' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a healthcheck' }
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. Give healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/Healthcheck',
           }
        },
        404: {
          'description': 'A user healthcheck this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,uuid=None):
    if id is not None:
      healthcheck = HealthcheckModel.get_one(session=db.session,id=id)
      if not healthcheck:
        abort(404,"That health check item id does not exists")
      return healthcheck
    elif uuid is not None:
      healthcheck = HealthcheckModel.get_one(session=db.session,uuid=uuid)
      if not healthcheck:
        abort(404,"That health check item uuid does not exists")
      return healthcheck
    else:
      abort(404,'id or uuid is not known : id {id}. uuid {uuid}.'.format(id=id, uuid=uuid))


class HealthCheckSearchByElementView(Resource):
  healthcheck_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'source':fields.String,
    'name':fields.String,
    'status':fields.String,
    'value':fields.String,
    'reason':fields.String,
    'element':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
  }


  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthchecks'],
      'summary': 'get a healthcheck by element',
      'parameters' : [
        { 'in': 'path','name': 'element' ,'type': 'string','description':'element associated to a heathcheck' },
      ],
      'definitions': {
        'Healthchecks': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/Healthcheck',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. Give healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/Healthchecks',
           }
        },
        404: {
          'description': 'A user healthcheck this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,element):
    healthchecks = HealthcheckModel.get(session=db.session,element=element)

    if len(healthchecks) == 0:
      abort(404,"Could not found any healthcheck  with element = {} ".format(element))

    return healthchecks

class HealthCheckSearchByFullsourcenameView(Resource):
  healthcheck_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'source':fields.String,
    'name':fields.String,
    'status':fields.String,
    'value':fields.String,
    'reason':fields.String,
    'element':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
  }


  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthchecks'],
      'summary': 'get a healthcheck by fullsourcename',
      'parameters' : [
        { 'in': 'path','name': 'fullsourcename' ,'type': 'string','description':'fullsourcename associated to a heathcheck' },
      ],
      'definitions': {
        'Healthchecks': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/Healthcheck',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. Give healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/Healthchecks',
           }
        },
        404: {
          'description': 'A user healthcheck this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,fullsourcename):

    healthchecks = HealthcheckModel.get(session=db.session,source=fullsourcename.split('_')[0],name=fullsourcename.split('_')[1])
    
    if len(healthchecks) == 0:
      abort(404,"Could not found any healthcheck  with  fullsourcename = {}".format( fullsourcename))

    return healthchecks


class HealthCheckHistoryListView(Resource):
  healthcheck_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'source':fields.String,
    'name':fields.String,
    'status':fields.String,
    'value':fields.String,
    'reason':fields.String,
    'element':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
  }


  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthcheckshistory'],
      'summary': 'get all healthchecks history',
      'definitions': {
        'HealthchecksHistory': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/HealthcheckHistory',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. A list of HealthChecksHistory",
           'schema' : {
             '$ref' : '#/definitions/HealthchecksHistory',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,uuid=None):
    healthchecks = HealthCheckHistoryModel.get_all(session=db.session)
    if len(healthchecks) == 0:
      healthchecks = []
    
    return healthchecks



class HealthCheckHistoryView(Resource):
  healthcheck_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'source':fields.String,
    'name':fields.String,
    'status':fields.String,
    'value':fields.String,
    'reason':fields.String,
    'element':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
  }


  @multi_auth.login_required
  @marshal_with(healthcheck_fields)
  @swag_from({
      'tags': ['healthcheckhistory'],
      'summary': 'get a healthcheckhistory by id or uuid',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a healthcheckhistory' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a healthcheckhistory' }
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. Give healthcheck data",
           'schema' : {
             '$ref' : '#/definitions/HealthcheckHistory',
           }
        },
        404: {
          'description': 'A user healthcheck this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,uuid=None):
    if id is not None:
      healthcheck = HealthCheckHistoryModel.get_one(session=db.session,id=id)
      if not healthcheck:
        abort(404,"That health check item id {} does not exists".format(id))
      return healthcheck
    elif uuid is not None:
      healthcheck = HealthCheckHistoryModel.get_one(session=db.session,uuid=uuid)
      if not healthcheck:
        abort(404,"That health check item uuid {} does not exists".format(uuid=uuid))
      return healthcheck
    else:
      abort(404,'id or uuid is not known : id {id}. uuid {uuid}.'.format(id=id, uuid=uuid))

