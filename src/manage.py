from __future__ import absolute_import
import os
from pathlib2 import Path
from environs import Env
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from factory import create_app
from models import db

env = Env()
env_file = str('.env')
if Path(env_file).exists():
  env.read_env(env_file)
environment = env('ENV',default='production')
app = create_app(environment)
migrate = Migrate(app=app, db=db)
manager = Manager(app=app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
  manager.run()
