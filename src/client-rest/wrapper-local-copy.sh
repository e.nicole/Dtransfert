



SCRIPTNAME=$(basename $0)


oops(){
  /bin/cat << EOF
Oooops, an error occured. Read above message if any.
Process Aborted !

EOF
  exit 1
}

usage(){
  /bin/cat << EOF
Usage : ${SCRIPTNAME} 
  - <-r|--roles>: role
Description : 
EOF
}


ARGS=`getopt -o "s:t:f:d:e:c:u:p:i:h:" -l "source:" -l "target:" -l "prefix:" -l "depth:" -l "endpoint:"  -l "checksum:" -l "username:"  -l "password:" -l "maxiteration:"  -l "help" -- "$@"`


if [ $? -ne 0 ]; then # script have to follow corect syntax
  usage
  exit 1
fi

while true; do
  case "$1" in
    -s|--source) sourcedir=("${2}"); shift 2;;
    -t|--target) targetdir=("${2}"); shift 2;; 
    -f|--prefix) prefix=("${2}"); shift 2;; 
    -d|--depth) depth=("${2}"); shift 2;;
    -e|--endpoint) endpoint=("${2}"); shift 2;;
    -c|--checksum) checksum=("${2}"); shift 2;;
    -u|--username) username=("${2}"); shift 2;;
    -p|--password) password=("${2}"); shift 2;;
    -i|--maxiteration) maxiteration=("${2}"); shift 2;;
    -h|--help) usage; exit 0;;
    *) break;;
  esac
done




for directory in $(find ${sourcedir} -mindepth ${depth} -maxdepth ${depth}  -type d | sort );do
  echo " ---- ${directory} ---- "
  arrsrc=($(echo $sourcedir | tr "/" "\n"))
  arrdir=($(echo $directory | tr "/" "\n"))
  arrjobname=${arrdir[@]:${#arrsrc[@]}:${depth}}
  path="${arrjobname[@]// //}"
  fulljobname="${prefix}_${arrjobname[@]// /_}"
  cmdenqueue="/OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -w 5  -e ${endpoint} -c ${checksum} -u ${username} -p ${password} -j ${fulljobname} -s ${sourcedir}${path} -t ${targetdir}${path}/"
  echo $cmdenqueue
  $cmdenqueue >/dev/null
  total=1000 
  ok=0
  i=0
  cmdresume="/OPT/dtransfert/bin/python /USR/dtransfert/app/src/client-rest/local-copy.py -w 5  -e ${endpoint} -u ${username} -p ${password} -j ${fulljobname} -r"

  while  [ ${total} -ne ${ok} ];do
    curl --noproxy '*' --user ${username}:${password} ${endpoint}/jobs/${fulljobname}/summarize 
    sleep 3
    JOBJSONFILE="/VAR/dtransfert/work/${fulljobname}.json"
    SUMMARYJSONFILE="/VAR/dtransfert/work/summary.${fulljobname}.json"
    curl --noproxy '*' --user ${username}:${password} ${endpoint}/jobs/${fulljobname} -o ${JOBJSONFILE}
  
    cat ${JOBJSONFILE} | jq '.summary' | jq -r . | tr "'"  \" | sed 's/None/0/'  > ${SUMMARYJSONFILE}
    total=$(cat ${SUMMARYJSONFILE} | jq '.total')
    ok=$(cat ${SUMMARYJSONFILE} | jq '.OK')
    if [ $ok == 'null' ];then 
      echo " ---- ${directory} ---- ${fulljobname}. ok : ${ok}. api not ready"
      ok=0
    fi
    if [ $total == 'null' ];then
      echo " ---- ${directory} ---- ${fulljobname}. total : ${total}.  api not ready"
      total=1000
    fi
    if [ $i -eq $maxiteration ];then
      echo " ---- ${directory} ---- ${fulljobname} ---- ${i} ---- ${maxiteration}. Resumed"
      echo $cmdresume
      $cmdresume >/dev/null
      i=0
    fi
    echo " ---- ${directory} ---- ${fulljobname} ---- ${i} ---- ${maxiteration}. ok : ${ok}. total : ${total}"
    i=$((i+1)) 
  done
done


