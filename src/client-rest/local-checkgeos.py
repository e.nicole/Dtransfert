from __future__ import absolute_import
import argparse
import concurrent.futures
import os
import json
from pathlib2 import Path
import requests
from requests.auth import HTTPBasicAuth
from datetime import datetime, timedelta, date
import logging


headersapi = {'Accept': 'application/json', 'Content-Type':'application/json'}

def dir_path(string):
  """ 
    Permets de verifier qu'il s'agit d'un repertoire.

    :param string: chaine de caractere représentant un répertoire

  """
  path = Path(string)
  if path.is_dir():
    return path
  else:
    raise NotADirectoryError(str(path.resolve()))

def create_path(string):
  """
    Permets de creer un repertoire si inexistant

    :param string: chaine de caractere représentant un répertoire

  """
  path = Path(string)
  if path.exists() == False:
    path.mkdir()
    return path
  elif path.is_dir():
    return path
  else:
    raise NotADirectoryError(str(path.resolve()))


def url_endpoint(string):
  """
    Verifie qu'il s'agit d'une url formaté comme attendu

    :param string: chaine de cractere representant l'url d'acces a l'application dtransfert
  """
  if (string.startswith('http://') or string.startswith('https://')) and string.endswith('/') == False:
    return string
  else:
    raise ValueError("{url} is not a goot url. must begin with 'http://' or 'https://' and must not finish with /".format(url=string))


def hash_algo(string):
  """
    verifie si l'algo precisé est bien pris en charge
    
    :param string: chaine de caractere representant le label d'un algo. algo pris en charge 'MD5' ou 'SHA-256'
  """
  if string in ('MD5','SHA-256'):
    return string
  else:
    raise ValueError('{cipher} is not supported'.format(cipher=string))


def get_token(endpoint,username,password):
  """
    Permets d'obtenir un token.
    Parametres:
      :param username : nom d'utilisateur
      :param password : mot de passe associé à l'utilisateur

    Retour: 
      False ou string: le token
  """

  try:
    tokenresponse = requests.get("{endpoint}/tokens".format(endpoint=endpoint), auth=HTTPBasicAuth(username,password),headers=headersapi)
    tokenresponse.raise_for_status()
  except requests.exceptions.HTTPError as errh:
    mylogger.error("Http Error: {}".format(errh))
  except requests.exceptions.ConnectionError as errc:
    mylogger.error("Error Connecting:{}".foramt(errc))
  except requests.exceptions.Timeout as errt:
    mylogger.error("Timeout Error:{}".format(errt))
  except requests.exceptions.RequestException as err:
    mylogger.error("OOps: Something Else : {}".format(err))

  if tokenresponse.status_code == 200:
    token = json.loads(tokenresponse.text).get('token')
  elif tokenresponse.status_code == 401:
    token = False

  return token
    


def checkongeosjob(transfertuuid,endpoint,headersapiauth):
  """
    permets de relancer un transfert par son id ou uuid

    :param transfertuuid: uuid ou id du transfert a relancer
    :param jobname: nom du job auquel appartient le transfert
    :param endpoint: l'url d'acces a l'application dtransfert
    :param hashalgo: algorithme de hash à utiliser pour les calculs de checksum des fichiers source et target
  """
  try:
    jsoncopyjobdef = json.dumps({
      'status': 'checkongeos',
    })
    mylogger.info('checkongeos {uuid}'.format(uuid=transfertuuid))
    checkongeos_api = requests.put("{endpoint}/transferts/{uuid}/".format(endpoint=endpoint,uuid=transfertuuid),data=jsoncopyjobdef,headers=headersapiauth)
    checkongeos_api.raise_for_status()
  except requests.exceptions.HTTPError as errh:
    mylogger.error("Http Error: {}".format(errh))
    taskid = 'error'
  except requests.exceptions.ConnectionError as errc:
    mylogger.error("Error Connecting:{}".foramt(errc))
    taskid = 'error'
  except requests.exceptions.Timeout as errt:
    mylogger.error("Timeout Error:{}".format(errt))
    taskid = 'error'
  except requests.exceptions.RequestException as err:
    mylogger.error("OOps: Something Else : {}".format(err))
    taskid = 'error'
    
  if checkongeos_api.status_code == 200:
    checkongeos_json = json.loads(checkongeos_api.text)
    taskid = checkongeos_json.get('taskid')
    mylogger.info('transfert uuid {} checked ongeos with task {}'.format(transfertuuid,taskid))

  return taskid
  

def summarizejob(endpoint,headersapiauth,jobname=None):
  try: 
    if jobname is not None:
      url = "{endpoint}/jobs/{jobname}/summarize".format(endpoint=endpoint,jobname=jobname)
    else:
      url = "{endpoint}/jobs/summarize".format(endpoint=endpoint)
    jobstatus_api = requests.get(url,headers=headersapiauth)
    jobstatus_api.raise_for_status()
  except requests.exceptions.HTTPError as errh:
    mylogger.error("Http Error: {}".format(errh))
  except requests.exceptions.ConnectionError as errc:
    mylogger.error("Error Connecting:{}".foramt(errc))
  except requests.exceptions.Timeout as errt:
    mylogger.error("Timeout Error:{}".format(errt))
  except requests.exceptions.RequestException as err:
    mylogger.error("OOps: Something Else : {}".format(err))



if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='Distributed Transfert cli impulse tasks')

  parser.add_argument('-j','--job-name',
                      action="store",
                      dest='jobname',
                      help='Nom du job',
                    )
  parser.add_argument('-c','--checkongeos',
                      action="store_true",
                      dest='checkongeos',
                      help='Activer la vérification du check sur le site geos sync',
                      default=False,
                    )
  parser.add_argument('-e','--endpoint-url',
                    action="store",
                    dest='endpoint',
                    type=url_endpoint,
                    help='Url endpoint',
                    required=True,
                  )

                      
  parser.add_argument('-l','--list-job',
                    action="store_true",
                    dest='listjob',
                    help='Liste les job',
                    default=False
                  )
  
  parser.add_argument('-ld','--log-directory',
                      action="store",
                      dest='logdir',
                      type=create_path,
                      help='Repertoire ou est loggué la partie client',
                      default=Path('/VAR').joinpath('dtransfert','logclient'),
                     )

  parser.add_argument('-w','--worker-number',action="store",dest='workersnumber',help='Nombre de fichier à soumettre en parrallele',default=10)
  
  parser.add_argument('-u','--username',action="store",dest='username',required=True,help='Nom d\'utilisateur qui a un accès')
  parser.add_argument('-p','--password',action="store",dest='password',required=True,help='Password associé à l\'utilisateur qui a un accès')

  


  #parser.add_argument('-s','--source-dir',action="store",dest='',help='')


  args = parser.parse_args()

  mylogger = logging.getLogger()
  formatter = logging.Formatter('%(asctime)s PID %(process)5s %(levelname)s %(message)s')
  LOGLEVEL = {
      'NOTSET' : {'num':0,'object':logging.NOTSET,'name':'notset'},
      'CONSOLE' : {'num':1,'name':'CONSOLE'},
      'DEBUG' : {'num':10,'object':logging.DEBUG,'name':'debug'},
      'INFO' : {'num':20,'object':logging.INFO,'name':'info'},
      'WARNING' : {'num':30,'object':logging.WARNING,'name':'warning'},
      'ERROR' : {'num':40,'object':logging.ERROR,'name':'error'},
      'CRITICAL' : {'num':50,'object':logging.CRITICAL,'name':'critical'},
      }
 
  if not args.logdir.exists():
    args.logdir.mkdir()

 
  logging.CONSOLE = LOGLEVEL['CONSOLE']['num']
  logging.addLevelName(logging.CONSOLE, LOGLEVEL['CONSOLE']['name'])
  setattr(mylogger, 'console', lambda message, *args: mylogger._log(logging.CONSOLE, message, args))

  mylogger.setLevel(logging.CONSOLE)
  logging.getLogger("requests").setLevel(logging.WARNING)
  logfile = args.jobname if args.jobname is not None else str(date.today())
  filehandler = logging.FileHandler(os.path.join(str(args.logdir.resolve()),(logfile+'.log')),
                                                encoding="utf-8",
                                   )
  filehandler.setFormatter(formatter)
  filehandler.setLevel(level=logging.INFO)
  mylogger.addHandler(filehandler)
  
  streamhandler = logging.StreamHandler()
  streamhandler.setFormatter(formatter)
  streamhandler.setLevel(level=logging.CONSOLE)
  mylogger.addHandler(streamhandler)

  token = get_token(endpoint=args.endpoint,username=args.username,password=args.password)

  if token == False:
    mylogger.error("{endpoint} is not authorized for user {username}. verify username or password".format(endpoint=args.endpoint,username=args.username))
    raise ValueError("{endpoint} is not authorized for user {username} with password {password}".format(endpoint=args.endpoint,username=args.username,password=args.password)) 
  headersapiauth= {**headersapi, 'Authorization':'Bearer {}'.format(token)}

  if args.listjob == True: 
    if args.sourcedir is not None or args.targetdir is not None :
      mylogger.error("<-s|--source-dir> ou  <-t|--target-dir> ne sont pas autorisés quand -l|--list-job est utilisé")
      parser.error("<-s|--source-dir> ou  <-t|--target-dir> ne sont pas autorisés quand -l|--list-job est utilisé")


    try:
      if args.jobname is None :
        summarizejob(endpoint=args.endpoint,headersapiauth=headersapiauth)
        jobstatus_api = requests.get("{endpoint}/jobs/".format(endpoint=args.endpoint),headers=headersapiauth)
      else:
        summarizejob(endpoint=args.endpoint,jobname=args.jobname,headersapiauth=headersapiauth)
        jobstatus_api = requests.get("{endpoint}/jobs/{jobname}/".format(endpoint=args.endpoint,jobname=args.jobname),headers=headersapiauth)
  
      jobstatus_api.raise_for_status()
    except requests.exceptions.HTTPError as errh:
      mylogger.error("Http Error: {}".format(errh))
    except requests.exceptions.ConnectionError as errc:
      mylogger.error("Error Connecting:{}".foramt(errc))
    except requests.exceptions.Timeout as errt:
      mylogger.error("Timeout Error:{}".format(errt))
    except requests.exceptions.RequestException as err:
      mylogger.error("OOps: Something Else : {}".format(err))
 
    if jobstatus_api.status_code != 200:
      raise ValueError("{endpoint} is not available".format(endpoint=args.endpoint))
     
    jobstatus_json = json.loads(jobstatus_api.text)
    if isinstance(jobstatus_json,list):
      for job in jobstatus_json:
        mylogger.console(job)
    elif isinstance(jobstatus_json,dict):
      mylogger.console(jobstatus_json)



  elif args.checkongeos == True:
    if args.jobname is None :
      mylogger.error("<-j|--job-name>est obligatoire quand -c|--checkongeos est utilisé")
      parser.error("<-j|--job-name>est obligatoire quand -c|--checkongeos est utilisé")

   

    try:
      jobstatus_api = requests.get("{endpoint}/jobs/{jobname}/status/".format(endpoint=args.endpoint,jobname=args.jobname),headers=headersapiauth)
      
      jobstatus_api.raise_for_status()
    except requests.exceptions.HTTPError as errh:
      mylogger.error("Http Error: {}".format(errh))
    except requests.exceptions.ConnectionError as errc:
      mylogger.error("Error Connecting:{}".foramt(errc))
    except requests.exceptions.Timeout as errt:
      mylogger.error("Timeout Error:{}".format(errt))
    except requests.exceptions.RequestException as err:
      mylogger.error("OOps: Something Else : {}".format(err))

    if jobstatus_api.status_code != 200:
      raise ValueError("{endpoint} is not available".format(endpoint=args.endpoint))
    jobstatus_json = json.loads(jobstatus_api.text)
    transferts = []
    for labelstatus, valuesstatus in jobstatus_json.items():
      if labelstatus not in ['OK-geos']:
        try:
          jobperstatus_api = requests.get("{endpoint}/jobs/{jobname}/status/{labelstatus}".format(endpoint=args.endpoint,jobname=args.jobname,labelstatus=labelstatus),headers=headersapiauth)
          jobperstatus_api.raise_for_status()
        except requests.exceptions.HTTPError as errh:
          mylogger.error("Http Error: {}".format(errh))
        except requests.exceptions.ConnectionError as errc:
          mylogger.error("Error Connecting:{}".foramt(errc))
        except requests.exceptions.Timeout as errt:
          mylogger.error("Timeout Error:{}".format(errt))
        except requests.exceptions.RequestException as err:
          mylogger.error("OOps: Something Else : {}".format(err))      

 
        if jobperstatus_api.status_code != 200:
          raise ValueError("{endpoint} is not available".format(endpoint=args.endpoint))
 
        jobperstatus_json = json.loads(jobperstatus_api.text)
        transferts.extend(jobperstatus_json)

    mylogger.info("{} transfert need to be resumed for {}".format(len(transferts),args.jobname))

    """
    # un-parallelize injection 
    for transfert in transferts:
      resumecopyjob(transfertuuid=transfert.get('uuid'),jobname=args.jobname,endpoint=args.endpoint)
    """
    #parallelize injection
    with concurrent.futures.ThreadPoolExecutor(max_workers=int(args.workersnumber)) as executor:
      future_checkongeosjob= { executor.submit(checkongeosjob,
                                               transfertuuid=transfert.get('uuid'),
                                               endpoint=args.endpoint,
                                               headersapiauth=headersapiauth): (transfert.get('uuid')) for transfert in transferts }
      for future in concurrent.futures.as_completed(future_checkongeosjob):
        copyjob = future_checkongeosjob[future]
        try:
          taskid = future.result()
        except Exception as exc:
          mylogger.exception('%s: generated an exception: %s' % (args.jobname, exc))
        else:
          mylogger.info('%s: taskid is %s' % (args.jobname, taskid))

    
    summarizejob(endpoint=args.endpoint,jobname=args.jobname,headersapiauth=headersapiauth)


