from __future__ import absolute_import
from flask import Flask, current_app
from flask_restful import Api
from celery import Celery
import os
from pathlib2 import Path
from environs import Env

from flasgger import Swagger, swag_from, APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin

from models import db,JobSchema,TransfertSchema,PostTransfertSchema,HealthcheckHistorySchema,HealthcheckSchema,ParamSchema,UserSchema,JobStatusTransfertSchema
from config import app_config

from viewsauth import (UsersListView,UserView,
                       TokensView)

from viewstransfert import (TransfertView,TransfertsListView,
                            JobView,JobsListView,
                            JobSummarizeView, JobsSummarizeView, 
                            JobsDetailedView, JobsStatusView)
                  
from viewshealth import (ParamView,ParamsListView,
                         HealthCheckHistoryView,HealthCheckHistoryListView,
                         HealthCheckView,HealthCheckListView,
                         HealthCheckSearchByFullsourcenameView,HealthCheckSearchByElementView)

from viewsmimic import MimicGeos

env = Env()
env_file = str('.env')
if Path(env_file).exists():
  env.read_env(env_file)
environment = env('ENV',default='production')

def create_app(package_name='api',  settings_override=None,register_security_blueprint=True,env_name=environment):

  app = Flask(package_name, instance_relative_config=True)
  api = Api(app)
  app.config.from_object(app_config[env_name])

  db.init_app(app)

  # transfert & job
  api.add_resource(TransfertsListView,
                   '/transferts',
                   endpoint = 'transferts', methods=['GET','POST']
                  )
  api.add_resource(TransfertView,
                   '/transferts/<int:id>',
                   '/transferts/<string:uuid>',
                   endpoint = 'transfert', methods=['GET','PUT']
                  )

  api.add_resource(JobsListView,'/jobs',endpoint='jobs',methods=['GET'])
  api.add_resource(JobView,'/jobs/<int:id>','/jobs/<string:jobname>',endpoint='job',methods=['GET','PUT'])
  
  api.add_resource(JobsDetailedView,
                   '/jobs/<int:id>/transferts','/jobs/<string:jobname>/transferts',
                   endpoint = 'jobtranferts'
                   )
  api.add_resource(JobsStatusView,
                   '/jobs/<int:id>/status',
                   '/jobs/<int:id>/status/<string:labelstatus>',
                   '/jobs/<string:jobname>/status',
                   '/jobs/<string:jobname>/status/<string:labelstatus>',
                   endpoint = 'jobstatus'
                   )  

  api.add_resource(JobsSummarizeView,'/jobs/summarize',endpoint='jobsummaries',methods=['GET'])
  api.add_resource(JobSummarizeView,'/jobs/<int:id>/summarize','/jobs/<string:jobname>/summarize',endpoint='jobsummary',methods=['GET'])


  # healthchecking
  api.add_resource(ParamsListView,'/healthcheckparams',endpoint='params',methods=['GET','POST'])
  api.add_resource(ParamView,'/healthcheckparams/<string:key>',endpoint='param',methods=['GET','PUT','DELETE'])


  api.add_resource(HealthCheckHistoryListView,
                   '/healthcheckhistory',
                   endpoint='healthcheckshistory',methods=['GET']
                   )
  api.add_resource(HealthCheckHistoryView,
                   '/healthcheckhistory/<string:uuid>',
                   '/healthcheckhistory/<int:id>',
                   endpoint='healthcheckhistory',methods=['GET']
                   )

  api.add_resource(HealthCheckSearchByElementView,
                   '/healthchecksearch/element/<string:element>',
                   endpoint = 'healtchecksearchbyelement', methods=['GET']
                   )
  api.add_resource(HealthCheckSearchByFullsourcenameView,
                   '/healthchecksearch/fullsourcename/<string:fullsourcename>',
                   endpoint = 'healtchecksearchbyfullsourcename', methods=['GET']
                   )

  api.add_resource(HealthCheckListView,
                   '/healthcheck',
                   endpoint = 'healthchecks', methods=['GET','POST']
                   )
  
  api.add_resource(HealthCheckView,
                   '/healthcheck/<string:uuid>',
                   '/healthcheck/<int:id>',
                   endpoint = 'healthcheck', methods=['GET','PUT','DELETE']
                   )

  # auth 
  api.add_resource(UsersListView,
                   '/users',
                   endpoint = 'users', methods=['GET','POST']
                   )
  api.add_resource(UserView,
                   '/users/<int:id>','/users/<int:uuid>',
                   endpoint = 'user', methods=['GET','PUT','DELETE']
                   )
  api.add_resource(TokensView,
                   '/tokens', endpoint= 'token' )

  #mimic
  api.add_resource(MimicGeos,'/mimic/scalitygeos')

  api.init_app(app)
  app.url_map.strict_slashes = False
  return app


def create_celery_app(app=None,env_name=environment):
  app = app or create_app()
  app.app_context().push()
  celery = Celery(app.import_name, broker=app.config['CELERY_BROKER'],backend=app.config['CELERY_RESULT'])
  celery.conf["broker_url"] = app.config['CELERY_BROKER']
  celery.conf["result_backend"] = app.config['CELERY_RESULT']
  TaskBase = celery.Task
  class ContextTask(TaskBase):
    abstract = True

    def __call__(self, *args, **kwargs):
      with app.app_context():
        return TaskBase.__call__(self, *args, **kwargs)


  celery.Task = ContextTask
  return celery



def create_swagger_app(app=None,env_name=environment):
  app = app or create_app()
  app.app_context().push()

  spec = APISpec(
    title='Dtransfert Api Documentation',
    version='1.0.0-a',
    openapi_version='2.0',
    plugins=[
        FlaskPlugin(),
        MarshmallowPlugin(),
    ],
  )
  template = spec.to_flasgger(
    app,
    definitions=[JobSchema,TransfertSchema,PostTransfertSchema,HealthcheckHistorySchema,HealthcheckSchema,ParamSchema,UserSchema,JobStatusTransfertSchema ],
  ) 

  swag = Swagger(app, template=template, parse=True  )
  return swag

