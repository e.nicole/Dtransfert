from __future__ import absolute_import
import os
from pathlib2 import Path
from environs import Env
import shutil
import json
from datetime import datetime,timedelta
from time import sleep
from pytz import timezone
import pytz
import requests
from flask import Flask, current_app,jsonify
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,scoped_session
from sqlalchemy import func

import celery
from celery import Task,task
from celery import shared_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger
from celery.app.log import TaskFormatter

from celery.exceptions import Retry
from celery.five import monotonic

import hashlib
import base64
import codecs


from factory import create_app,create_celery_app
from models import db,TransfertModel,JobModel,UserModel,TransfertTracingModel
from utils import SendMsgBot 

logger = get_task_logger(__name__)

@after_setup_task_logger.connect
def setup_task_logger(logger, *args, **kwargs):
  for handler in logger.handlers:
    handler.setFormatter(TaskFormatter('%(asctime)s - %(task_id)s - %(task_name)s - %(name)s - %(levelname)s - %(message)s'))

env = Env()
env_file = str('.env')
if Path(env_file).exists():
  env.read_env(env_file)
environment = env('ENV',default='production')

app = create_app()
app.app_context().push()
celery_ins = create_celery_app(app)
celery_ins.conf.update(app.config)
celery_ins.conf["broker_url"] = app.config['CELERY_BROKER']
celery_ins.conf["result_backend"] = app.config['CELERY_RESULT']
celery_ins.conf["worker_send_task_events"] = app.config['CELERY_SEND_EVENTS']
celery_ins.conf["task_send_sent_event"] = app.config['CELERY_SEND_SENT_EVENT']
celery_ins.conf["worker_max_tasks_per_child"] = app.config['CELERYD_MAX_TASKS_PER_CHILD']
celery_ins.conf["worker_prefetch_multiplier"] = app.config['CELERYD_PREFETCH_MULTIPLIER']
#celery_ins.conf["database_engine_options"] = app.config['CELERY_RESULT_ENGINE_OPTIONS']



class TransfertTask(Task):
  abstract = True
  HASH_ALGO = {
    'MD5' : hashlib.md5,
    'SHA-256': hashlib.sha256,
  }
  
  _session = None
  
  @property
  def session(self):
     
    if self._session is None:
      engine = create_engine(self.app.conf['SQLALCHEMY_DATABASE_URI'])
      
      Session = scoped_session(sessionmaker(bind=engine))
      self._session = Session()
    return self._session


  def after_return(self, status, retval, task_id, args, kwargs, einfo):
    self.session.close()

  def get_transfert(self,transfertid):
    """
      renvoie l'instance du model transfert selon le transfert  id
    """
    transfert = TransfertModel.get_one(session=self.session, id = transfertid)
    return transfert

  def get_job(self,transfertid):
    """
      renvoie l'instance du model job selon le transfert id
    """
    job = JobModel.get_one(session=self.session,id=self.get_transfert(transfertid).jobid)
    return job

  
  def file_as_bytes(self,filestream):
    """
      renvoie le contenu du fichier en bytes
    """
    with filestream:
      return filestream.read()


  def hashalgohandler(self,hashlabel,absolutefilename):
    """
      constructeur pour fonction de hash
      selectione l'algo de hash adéquate et renvoie l'objet 
    """
    if hashlabel not in self.HASH_ALGO.keys():
      raise ValueError("Unsupported Hash algorithm",
                       "Supported hash algoritm are {supported_algo}".format(self.HASH_ALGO.keys())
      )

    absolutefilestream = self.file_as_bytes(Path(absolutefilename).open('rb'))
    HashClass = self.hashalgodispatcher(hashlabel)
    return HashClass(absolutefilestream)

  def hashalgodispatcher(self,hashlabel):
    """
      selection de la classe d'algo basé sur le label d'algo voulu
    """
    return self.HASH_ALGO[hashlabel]

  def checksumfile(self,absolutefilename,hashlabel):
    """
    calcule le checksum d'un fichier
    """
    checksum = self.hashalgohandler(hashlabel=hashlabel,absolutefilename=absolutefilename).digest()

    return checksum

  def broadening(self,**kwargs):
    #self.transfert.step = self.__name__
    self.transfert.status = 'processing'
    absolutefile_source = Path(kwargs.get('root_source'),).joinpath(kwargs.get('path'),kwargs.get('filename'))
    absolutefile_target =  Path(kwargs.get('root_target'),).joinpath(kwargs.get('path'),kwargs.get('filename'))
    if absolutefile_source.exists():
      self.transfert.message = 'source file exists {absolutefile_source}'.format(absolutefile_source=str(absolutefile_source.resolve()))
    else:
      self.transfert.status = 'failed'
      self.transfert.message = 'source file not exists {absolutefile_source}'.format(absolutefile_source=str(absolutefile_source.resolve()))
      self.request.callbacks = None
      self.request.chain = None

    self.transfert.updated_on = db.func.now()
    #self.job.updated_on = db.func.now()
    self.session.commit()
    self.session.refresh(self.transfert)
    return {**self.transfert.to_dict(), 'absolutefile_source':str(absolutefile_source.resolve()),'absolutefile_target':str(absolutefile_target.resolve()) }

  def checksumsource(self,**kwargs):
    """
    calcule le checksum du fichier source et l'enregistre dans la base de donnée
    """
    self.transfert.status = 'processing'
    #self.transfert.step = self.__name__
    self.transfert.checksum_source = self.checksumfile(absolutefilename=kwargs.get('absolutefile_source'),hashlabel=self.job.checkhash)
    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}

  def checksumtarget(self,**kwargs):
    """
    calcule le checksum du fichier target et l'enregistre dans la base de donnée
    """
    self.transfert.status = 'processing'
    #self.transfert.step = self.__name__
    self.transfert.checksum_target = self.checksumfile(absolutefilename=kwargs.get('absolutefile_target'),
                                                      hashlabel=self.job.checkhash)

    
    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}

  def checksumgeosyncsite(self,**kwargs):
    """
    calcule le checksum du fichier sur le site de geosync et l'enregistre dans la base de donnée
    """
    self.transfert.status = 'processing'
    #self.transfert.step = self.__name__
    self.transfert.checksum_geosyncsite = self.checksumfile(absolutefilename=kwargs.get('absolutefile_target'),
                                                      hashlabel=self.job.checkhash)

    
    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}


  def comparechecksum(self,**kwargs):
    """
    Compare le checksum entre la source et la target
    """

    #self.transfert.step = self.__name__
    self.transfert.status = 'processing'
    if self.transfert.checksum_target == self.transfert.checksum_source :
      self.transfert.status = 'OK'
      self.transfert.message = "All is OK when transfering {absolutefile_source}->{absolutefile_target}".format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'))
    else:
      self.transfert.status = 'NOK'
      self.transfert.message = "NOK. Deleting {absolutefile_target} ".format(absolutefile_target=kwargs.get('absolutefile_target'))
      try:
        Path(kwargs.get('absolutefile_target')).unlink()
      except e:
        self.transfert.message = "NOK. Can not remove {absolutefile_target} ".format(kwargs.get('absolutefile_target'))
        logger.exception('Can not remove {absolutefile_target}'.format(absolutefile_target=kwargs.get('absolutefile_target')))




    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}


  def mkdir(self,**kwargs):
    #self.transfert.step = self.__name__
    self.transfert.status = 'processing'
    path = Path(kwargs.get('absolutefile_target'))
    
    if path.parent.exists() == False:
      path.parent.mkdir(parents=True,exist_ok=True)
      self.transfert.message = 'Creating {}'.format(str(path.parent.resolve()))

    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}



  def copy(self,**kwargs):
    """

    """
    #self.transfert.step = self.__name__
    self.transfert.status = 'processing'
    self.transfert.message = "Must copy  {absolutefile_source}->{absolutefile_target}".format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'))  
    if Path(kwargs.get('absolutefile_target')).exists():
      self.transfert.message = "{absolutefile_target} already exist. Getting checksum_target".format(absolutefile_target=kwargs.get('absolutefile_target'))
      self.transfert.checksum_target = self.checksumfile(kwargs.get('absolutefile_target'),hashlabel=self.job.checkhash)
      if self.transfert.checksum_target != self.transfert.checksum_source :
        try:
          shutil.copy(kwargs.get('absolutefile_source'),kwargs.get('absolutefile_target'))
          self.transfert.status  = 'processing'
          self.transfert.message = 'copied: {absolutefile_source}->{absolutefile_target}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'))
  #      except IOError as ioe:
  #        self.transfert.status  = 'copy-failed'
  #        self.transfert.message = 'copy failed: {absolutefile_source}->{absolutefile_target}.{msgerr}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'),msgerr=ioe.message)
  #        self.request.callbacks = None
  #        self.request.chain = None
        except Exception as e:
          if hasattr(e, 'message'):
            msgerr=e.message
          else:
            msgerr=e
          self.transfert.status  = 'copy-failed'
          self.transfert.message = 'copy failed: {absolutefile_source}->{absolutefile_target}.{msgerr}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'),msgerr=msgerr)    
    else:
      try:
        shutil.copy(kwargs.get('absolutefile_source'),kwargs.get('absolutefile_target'))
        self.transfert.status  = 'processing'
        self.transfert.message = 'copied: {absolutefile_source}->{absolutefile_target}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'))
  #    except IOError as ioe:
  #      self.transfert.status  = 'copy-failed'
  #      self.transfert.message = 'copy failed: {absolutefile_source}->{absolutefile_target}.{msgerr}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'),msgerr=ioe.message)
  #      self.request.callbacks = None
  #      self.request.chain = None

      except Exception as e:
        if hasattr(e, 'message'):
          msgerr=e.message
        else:
          msgerr=e
        self.transfert.status  = 'copy-failed'
        self.transfert.message = 'copy failed: {absolutefile_source}->{absolutefile_target}.{msgerr}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'),msgerr=msgerr)
      

    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}


  def getsize(self,**kwargs):
    self.transfert.status = 'processing'
    self.transfert.message = 'record file size for {absolutefile_target}'.format(absolutefile_target=kwargs.get('absolutefile_target'))
    try:
      self.transfert.size = Path(kwargs.get('absolutefile_target')).stat().st_size

    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      self.transfert.status  = 'getsize-failed'
      self.transfert.message = 'getsize failed: {absolutefile_target}.{msgerr}'.format(absolutefile_source=kwargs.get('absolutefile_source'),absolutefile_target=kwargs.get('absolutefile_target'),msgerr=msgerr)
   
    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}

  def comparechecksumongeos(self,**kwargs):
    """
    Compare le checksum entre la source et la target
    """
    #self.transfert.step = self.__name__
    self.transfert.status = 'processing'
    if self.transfert.checksum_target == self.transfert.checksum_geosyncsite :
      self.transfert.status = 'OK-geos'
      self.transfert.message = "All is OK when geosyncing {absolutefile_target}".format(absolutefile_target=kwargs.get('absolutefile_target'))
    else:
      self.transfert.status = 'NOK'
      self.transfert.message = "NOK when deleting {absolutefile_target} ".format(absolutefile_target=kwargs.get('absolutefile_target'))


    return {**self.transfert.to_dict(),'absolutefile_source':kwargs.get('absolutefile_source'),'absolutefile_target':kwargs.get('absolutefile_target')}



@celery.task(base=TransfertTask,bind=True,name="tasks_transfert.checkongeos",resultrepr_maxsize=2048)
def checkongeos(self,jsonkwargs):
  kwargs = json.loads(jsonkwargs)
  self.transfert = self.get_transfert(transfertid=kwargs.get('id'))
  self.job = self.get_job(transfertid=kwargs.get('id'))
  absolutefile_source = Path(kwargs.get('root_source'),).joinpath(kwargs.get('path'),kwargs.get('filename'))
  absolutefile_target =  Path(kwargs.get('root_target'),).joinpath(kwargs.get('path'),kwargs.get('filename'))
  try:
    self.transfert.step = 'checksumgeosyncsite'
   
    kwargs = self.checksumgeosyncsite(**{**self.transfert.to_dict(), 'absolutefile_source':str(absolutefile_source.resolve()),'absolutefile_target':str(absolutefile_target.resolve()) })
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'checksumgeosyncsite-failed'
    self.transfert.message = 'checksumgeosyncsite-failed : {msgerr}'.format(msgerr=msgerr)

    self.request.callbacks = None
    self.request.chain = None

    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()

    return self.transfert.to_dict()
  finally:

    if self.job.begin_on is None:
      self.job.begin_on = db.func.now()
    self.session.commit()


  try:
    self.transfert.step = 'comparechecksumongeos'
    kwargs = self.comparechecksumongeos(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'comparechecksumongeos-failed'
    self.transfert.message = 'comparechecksumongeos-failed : {msgerr}'.format(msgerr=msgerr)

    self.request.callbacks = None
    self.request.chain = None
    self.job.updated_on = db.func.now()
    self.job.finished_on = db.func.now()
    return self.transfert.to_dict()
  finally:
    self.transfert.updated_on = db.func.now()
    self.session.commit()
  
  kwargs['task_name']= self.name
  return kwargs
  



@celery.task(base=TransfertTask,bind=True,name="tasks_transfert.instance",resultrepr_maxsize=2048)
def instance(self,jsonkwargs):
  kwargs = json.loads(jsonkwargs)
  self.transfert = self.get_transfert(transfertid=kwargs.get('id'))
  self.job = self.get_job(transfertid=kwargs.get('id'))

  begin = datetime.now()
  try:
    self.transfert.step = 'broadening'
    kwargs = self.broadening(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'broadening-failed'
    self.transfert.message = 'broadening-failed : {msgerr}'.format(msgerr=msgerr)
    
    self.request.callbacks = None
    self.request.chain = None
    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()
    return self.transfert.to_dict()
  finally:
    if self.job.begin_on is None:
      self.job.begin_on = db.func.now()
    self.session.commit()

    

  
  try:
    self.transfert.step = 'checksumsource'
    kwargs = self.checksumsource(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'checksumsource-failed'
    self.transfert.message = 'checksumsource-failed : {msgerr}'.format(msgerr=msgerr)
    
    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()
    return self.transfert.to_dict()
  finally:
    self.session.commit()
  
   
  try: 
    self.transfert.step = 'mkdir'
    kwargs = self.mkdir(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'mkdir-failed'
    self.transfert.message = 'mkdir-failed : {msgerr}'.format(msgerr=msgerr)
    self.request.callbacks = None
    self.request.chain = None
    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()
    return self.transfert.to_dict()
  finally:
    self.session.commit()


  try:  
    self.transfert.step = 'copy'
    kwargs = self.copy(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'copy-failed'
    self.transfert.message = 'copy-failed : {msgerr}'.format(msgerr=msgerr)
    self.request.callbacks = None
    self.request.chain = None
    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()
    return self.transfert.to_dict()
  finally:

    self.session.commit()


  try: 
    self.transfert.step = 'checksumtarget'
    kwargs = self.checksumtarget(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'checksumtarget-failed'
    self.transfert.message = 'checksumtarget-failed : {msgerr}'.format(msgerr=msgerr)
    self.request.callbacks = None
    self.request.chain = None
    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()
    return self.transfert.to_dict()
  finally:

    self.session.commit() 


  try:
    self.transfert.step = 'getsize'
    kwargs = self.getsize(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'getsize-failed'
    self.transfert.message = 'getsize-failed : {msgerr}'.format(msgerr=msgerr)
    self.request.callbacks = None
    self.request.chain = None
    self.transfert.updated_on = db.func.now()
    self.job.updated_on = db.func.now()
    return self.transfert.to_dict()
  finally:

    self.session.commit()



  try:
    self.transfert.step = 'comparechecksum'
    kwargs = self.comparechecksum(**kwargs)
  except Exception as e:
    if hasattr(e, 'message'):
      msgerr=e.message
    else:
      msgerr=e
    self.transfert.status  = 'comparechecksum-failed'
    self.transfert.message = 'comparechecksum-failed : {msgerr}'.format(msgerr=msgerr)
    self.request.callbacks = None
    self.request.chain = None
    return self.transfert.to_dict()
  finally:
    self.transfert.updated_on = db.func.now()
    #self.job.updated_on = db.func.now()
    #self.job.finished_on = db.func.now()
    self.session.commit()


  kwargs['task_name']= self.name
  return kwargs




@celery.task(base=TransfertTask,bind=True,name="tasks_transfert.summarize_job",resultrepr_maxsize=2048)
def summarize_job(self,jobid=None):
  jobs = []
  if jobid is None:
    jobs = JobModel.get(session=self.session,enable_summary = True)
  else:
    jobs.append(JobModel.get_one(session=self.session,id=jobid))
  
  summaries = []
  for job in jobs:
    transferts_status = self.session.query(TransfertModel.status,func.count(TransfertModel.id)).join(JobModel).filter(JobModel.jobname == job.jobname).group_by(TransfertModel.status).all()
    summary = {}
    total = 0
    for row in transferts_status:
      total += row[1]
      summary[row[0]]=row[1]

    summary['total'] = total
    summary['jobsize'] = self.session.query(func.sum(TransfertModel.size)).join(JobModel).filter(JobModel.jobname == job.jobname).all()[0][0]
    summary['runtime'] = self.session.query(func.sum(TransfertTracingModel.runtime)).filter(TransfertTracingModel.jobname == job.jobname).all()[0][0]


    job.updated_summary = db.func.now()    
    job.summary = summary
    self.session.commit()
    self.session.refresh(job)
    summaries.append(job.to_dict())
    jobsummarylog = { job.jobname : summary }
    logger.info("{jobsummarylog}".format(jobsummarylog=jobsummarylog))

  return summaries


@celery.task(base=TransfertTask,bind=True,name="tasks_transfert.alert_for_dead_job",resultrepr_maxsize=2048)
def alert_for_dead_job(self,deltasec=1800):
  """
    this task list all job that finish_on < now - 30 min and enable_summary = True
    and send a xmpp message to user where pageme is true & room 
  """
  logger.info(deltasec)
  ue_paris = pytz.timezone(app.config.get('TIMEZONE'))
  logger.info(ue_paris)
  delta = datetime.now(ue_paris) - timedelta(seconds=deltasec)
  logger.info(delta)
  alivejobs = self.session.query(JobModel).filter(JobModel.enable_summary == True).all()
  
  deadjobs = self.session.query(JobModel).filter(JobModel.enable_summary == True).filter(ue_paris.localize(JobModel.finished_on) <= delta).all()
  logger.info('deadjobs : {}'.format(deadjobs))


  if app.config.get('XMPP_SERVER') is not None or app.config.get('XMPP_USER') is not None or app.config.get('XMPP_PASSWORD') is not None or app.config.get('XMPP_ROOM') is not None or app.config.get('XMPP_PORT') is not None:

    pagers = [user.pager for user in UserModel.get(session=self.session, pageme=True ) if user.pager is not None]
    logger.info('xmpp message send to {XMPP_ROOM} and {pagers}'.format(XMPP_ROOM=app.config.get('XMPP_ROOM'),pagers=pagers))
    for alivejob in alivejobs:
      logger.info('alivejob : {}/{}/{}.delta {} '.format(alivejob.jobname,alivejob.finished_on,ue_paris.localize(alivejob.finished_on),delta))
      logger.info('time alivejob : {}/{}'.format(alivejob.jobname,ue_paris.localize(alivejob.finished_on) <= delta))
      if ue_paris.localize(alivejob.finished_on) <= delta :
        message = "job {jobname} semble non actif. Le résultat de ce job est : {summary}. Si vous ne souhaitez plus recevoir ce message pour ce job, Il faut passser enable_summary a false ".format(jobname=deadjob.jobname,summary=deadjob.summary)
        logger.info(message)

        #send message to room 
        xmpp = SendMsgBot(nick=app.config['XMPP_USER'].split('@')[0],jid=app.config['XMPP_USER'],password=app.config['XMPP_PASSWORD'],room=app.config['XMPP_ROOM'],msg=message,invitations=pagers)
        xmpp.register_plugin('xep_0030')
        xmpp.register_plugin('xep_0199')
        xmpp.register_plugin('xep_0045')
        xmpp.connect((app.config['XMPP_SERVER'],app.config['XMPP_PORT']))
        xmpp.process(timeout=10)

        #send message to jid
        xmpp = SendMsgBot(nick=app.config['XMPP_USER'].split('@')[0],jid=app.config['XMPP_USER'],password=app.config['XMPP_PASSWORD'],msg=message,recipients=pagers)
        xmpp.register_plugin('xep_0030')
        xmpp.register_plugin('xep_0199')
        xmpp.register_plugin('xep_0045')
        xmpp.connect((app.config['XMPP_SERVER'],app.config['XMPP_PORT']))
        xmpp.process(timeout=10)

      message = "Vous pouvez également désactiver toutes les notifications dans votre profil utilisateur."
      xmpp = SendMsgBot(nick=app.config['XMPP_USER'].split('@')[0],jid=app.config['XMPP_USER'],password=app.config['XMPP_PASSWORD'],msg=message,recipients=pagers)
      xmpp.register_plugin('xep_0030')
      xmpp.register_plugin('xep_0199')
      xmpp.register_plugin('xep_0045')
      xmpp.connect((app.config['XMPP_SERVER'],app.config['XMPP_PORT']))
      xmpp.process(timeout=10)
  

