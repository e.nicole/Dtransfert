from __future__ import absolute_import
from marshmallow import fields, Schema
from itertools import chain
from  datetime import datetime
from uuid import uuid4,uuid1
import json
from passlib.apps import custom_app_context as pwd_context
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from sqlalchemy.ext import mutable
from sqlalchemy.engine import reflection
from sqlalchemy import func
from sqlalchemy.inspection import inspect
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import  MultipleResultsFound, NoResultFound
from sqlalchemy.ext.hybrid import hybrid_property



db = SQLAlchemy()


class JsonEncodedDict(db.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = db.Text

    def process_bind_param(self, value, dialect):
        if value is None:
            return '{}'
        else:
            return json.dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return {}
        else:
            return json.loads(value)


mutable.MutableDict.associate_with(JsonEncodedDict)


class MyModel(db.Model):
  """ Extend SQLAlchemy's Model class with a few handy methods. """
  __abstract__ = True
  created_on = db.Column(db.DateTime, server_default=db.func.now(),default=db.func.now())
  updated_on = db.Column(db.DateTime, server_default=db.func.now(),default=db.func.now(), server_onupdate=db.func.now(),onupdate=db.func.now())
  @classmethod
  def get_one(self, session, **kwargs):
    return session.query(self).filter_by(**kwargs).first()

  @classmethod
  def get(self, session, **kwargs):
    return session.query(self).filter_by(**kwargs).all()
  
  @classmethod
  def get_all(self, session):
    return session.query(self).all()

  @classmethod
  def get_or_create(self, session, **kwargs):
    instance = session.query(self).filter_by(**kwargs).first()
    created = False
    if  instance is None:
        created = True
        instance = self(**kwargs)
        session.add(instance)
        session.commit()
        session.refresh(instance)       
    return created,instance

class MySchema(Schema):
  __abstract__ = True
  created_on = fields.DateTime()
  updated_on = fields.DateTime()

 
class JobModel(MyModel):
  __tablename__ = 'jobs'
  id = db.Column(db.Integer, primary_key=True)
  jobname = db.Column(db.Text, index=True)
  updated_summary = db.Column(db.DateTime,index=True)
  summary = db.Column(JsonEncodedDict)
  enable_summary = db.Column(db.Boolean,index=True,default=True)
  checkhash = db.Column(db.Enum("MD5","SHA-256",name='algo_choices'))

  @hybrid_property
  def begin_on(self):
    return db.session.query(func.min(TransfertModel.updated_on)).filter(TransfertModel.jobid == self.id).all()[0][0]

  @hybrid_property
  def finished_on(self):
    return db.session.query(func.max(TransfertModel.updated_on)).filter(TransfertModel.jobid == self.id).all()[0][0]

  transferts = relationship("TransfertModel",backref='jobmodel',lazy='dynamic')
  def to_dict(self):
   return dict(id=self.id,
               jobname=self.jobname ,
               created_on=str(self.created_on) ,
               updated_on=str(self.updated_on),
               updated_summary=self.updated_summary ,
               summary =self.summary ,
               enable_summary=self.enable_summary ,
              )






class JobSchema(MySchema):
  id = fields.Int(dump_only=True)
  jobname = fields.Str(required=True,unique=True)
  updated_summary = fields.DateTime()
  created_on = fields.DateTime()
  updated_on = fields.DateTime()
  summary = fields.Str()
  enable_summary = fields.Bool()
  checkhash = fields.Str()


class JobStatusTransfertSchema(Schema):
  id=fields.Int()
  status=fields.Str()
  step=fields.Str()
  updated_on= fields.DateTime(dt_format='rfc822')
  root_source=fields.Str()
  root_target=fields.Str()
  path=fields.Str()
  filename=fields.Str()
  size=fields.Int()


class JobsStatusSchema(Schema):
  id = fields.Int(dump_only=True)
  jobname = fields.Str(required=True,unique=True)
  updated_summary = fields.DateTime(dt_format='rfc822')
  created_on = fields.DateTime()
  updated_on = fields.DateTime()
  checkhash = fields.Str()
  transferts = fields.Nested(JobStatusTransfertSchema,many=True) 


class TransfertModel(MyModel):
  __tablename__ = 'transferts'
  id = db.Column(db.Integer, primary_key=True, index=True)
  uuid = db.Column(db.Text,  default=lambda: uuid4().__str__(), unique = True)
  root_source = db.Column(db.Text)
  root_target = db.Column(db.Text)
  path = db.Column(db.Text)
  filename = db.Column(db.Text)
  size = db.Column(db.Integer)
  step = db.Column(db.Text,default='initiate')
  status = db.Column(db.Text)
  checksum_source = db.Column(db.String)
  checksum_target = db.Column(db.String)
  checksum_geosyncsite = db.Column(db.String)
  message = db.Column(db.Text)
  jobid=db.Column(db.Integer, db.ForeignKey('jobs.id'))
  
  @hybrid_property
  def jobname(self):
    return JobModel.get_one(session=db.session,id=self.jobid).jobname

  def __repr__(self):
    return '<id {}>'.format(self.id)

  def to_dict(self):
   return dict(id=self.id,
               uuid=self.uuid ,
               root_source=self.root_source ,
               root_target=self.root_target ,
               path=self.path ,
               step = self.step,
               filename=self.filename ,
               status =self.status ,
               size = self.size,
               jobname = self.jobname,
               checksum_source=self.checksum_source ,
               checksum_target=self.checksum_target ,
               checksum_geosyncsite=self.checksum_geosyncsite,
               message=self.message ,
               created_on=str(self.created_on) ,
               updated_on=str(self.updated_on) ,
              )

class PostTransfertSchema(MySchema):
  class Meta:
    model = TransfertModel
    include_fk = True
  
  root_source = fields.Str(required=True)
  root_target = fields.Str(required=True)
  path = fields.Str(required=True)
  filename = fields.Str(required=True)

class TransfertSchema(MySchema):
  class Meta:
    model = TransfertModel
    include_fk = True
    
  id = fields.Int(dump_only=True)
  uuid = fields.Str()
  root_source = fields.Str(required=True)
  root_target = fields.Str(required=True)
  path = fields.Str(required=True)
  filename = fields.Str(required=True)
  size =  fields.Int()
  step = fields.Str()
  status = fields.Str()
  checksum_source = fields.Str()
  checksum_target = fields.Str()
  checksum_geosyncsite = fields.Str()
  message = fields.Str()

class TransfertTracingModel(MyModel):
  __tablename__ = 'tf_tracing'
  id =  db.Column(db.Integer, primary_key=True)
  transfert_uuid = db.Column(db.Text,index=True)
  task_uuid = db.Column(db.Text)
  runtime = db.Column(db.Float)
  jobname = db.Column(db.Text,index=True)
  size = db.Column(db.Integer)
  task_name = db.Column(db.Text)
  


class ParamModel(MyModel):
  __tablename__ = 'params'
  key = db.Column(db.Text,primary_key=True)
  value = db.Column(db.Text)
  description = db.Column(db.Text)

class ParamSchema(MySchema):
  class Meta:
    model = ParamModel
  id = fields.Int(dump_only=True)
  key = fields.Str(required=True)
  value = fields.Str(required=True)
  description = fields.Str(required=True)


class HealthCheckHistoryModel(MyModel):
  __tablename__ = 'hist_healthcheck'
  id = db.Column(db.Integer, primary_key=True)
  uuid = db.Column(db.Text,index=True)
  source = db.Column(db.Text,index=True)
  name = db.Column(db.Text,index=True)
  value = db.Column(db.Text)
  status = db.Column(db.Text)
  reason = db.Column(db.Text)
  element = db.Column(db.Text)
  created_on = db.Column(db.DateTime)
  updated_on = db.Column(db.DateTime)

class HealthcheckHistorySchema(MySchema):
  class Meta:
    model = HealthCheckHistoryModel

  id = fields.Int(dump_only=True)
  uuid = db.Column(db.Text)
  source = fields.Str(required=True)
  name = fields.Str(required=True)
  value = fields.Str()
  status = fields.Str()
  reason = fields.Str()
  element = fields.Str()

 
class HealthcheckModel(MyModel):
  __tablename__ = 'healthcheck'
  __table_args__ = (db.UniqueConstraint('source','name','element',name='unicity_source_name_element'), )
  id = db.Column(db.Integer, primary_key=True)
  uuid = db.Column(db.Text,  default=lambda: uuid4().__str__(), unique = True)
  source = db.Column(db.Text,index=True)
  name = db.Column(db.Text,index=True)
  value = db.Column(db.Text)
  status = db.Column(db.Text)
  reason = db.Column(db.Text)
  element = db.Column(db.Text,default='*',index=True)

  def to_dict(self):
   return dict(id=self.id,
               uuid = self.uuid,
               source = self.source,
               name = self.name,
               value = self.value,
               status = self.status,
               reason = self.reason,
               element = self.element,
              )

  def historize(self,session):
    history = HealthCheckHistoryModel(
                            uuid = self.uuid,
                            source = self.source,
                            name = self.name,
                            value = self.value,
                            status = self.status,
                            reason = self.reason,
                            element = self.element,
                            created_on = self.created_on,
                            updated_on = self.updated_on, 
    )
    session.add(history)
    session.commit()
    return history
   

class HealthcheckSchema(MySchema):
  class Meta:
    model = HealthcheckModel

  id = fields.Int(dump_only=True)
  uuid = fields.Str()
  source = fields.Str(required=True)
  name = fields.Str(required=True)
  value = fields.Str()
  status = fields.Str()
  reason = fields.Str()
  element = fields.Str()



class UserModel(MyModel):
  __tablename__ = 'users'
  id = db.Column(db.Integer, primary_key = True)
  uuid = db.Column(db.Text,  default=lambda: uuid4().__str__(), unique = True )
  username = db.Column(db.String(32), index = True)
  email = db.Column(db.String(128))
  password_hash = db.Column(db.String(128))
  pager = db.Column(db.String(128))
  pageme = db.Column(db.Boolean, default=False)


  def hash_password(self, password):
      self.password_hash = pwd_context.encrypt(password)

  def verify_password(self, password):
      return pwd_context.verify(password, self.password_hash)

  def generate_auth_token(self, expiration = 600):
    s = Serializer(current_app.config['TOKEN_SECRET_KEY'], expires_in = expiration)
    return s.dumps({ 'id': self.id })

  @staticmethod
  def verify_auth_token(token):
    s = Serializer(current_app.config['TOKEN_SECRET_KEY'])
    try:
      data = s.loads(token)
    except SignatureExpired:
      return None # valid token, but expired
    except BadSignature:
      return None # invalid token
    user = UserModel.query.get(data['id'])
    return user

class UserSchema(MySchema):
  class Meta:
    model = UserModel
  id = fields.Int(dump_only=True)
  uuid = fields.Str()
  username = fields.Str(required=True)
  email = fields.Str(required=True)
  pager = fields.Str()
  pageme = fields.Bool()

