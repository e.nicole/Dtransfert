from environs import Env
import argparse
from pathlib2 import Path
from factory import create_app,create_celery_app
import logging

env = Env()
env_file = str('.env')
if Path(env_file).exists():
  env.read_env(env_file)
environment = env('ENV',default='production')
app = create_app(environment)
app.app_context().push()

from models import db,UserModel

parser = argparse.ArgumentParser(description='Distributed Transfert : user creation/modification')

parser.add_argument('-u','--username',action="store",dest='username',help='Name of the user',required=True)
parser.add_argument('-p','--password',action="store",dest='password',help='Password of the user',default=None)
parser.add_argument('-e','--email',action="store",dest='email',help='email of the user',default=None)
parser.add_argument('-pg','--pager',action="store",dest='pager',help='pager of the user',default=None)
parser.add_argument('-pm','--pageme',action="store_true",dest='pageme',help='page-spam the user',default=None)
parser.add_argument('-ll','--loglevel',action="store",dest='level',help='Log Level',default='info')
args = parser.parse_args()



LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

logging.basicConfig(level=LEVELS.get(args.level),format=' %(levelname)s -> %(message)s')
logging.debug(__name__)  
logging.debug(args)

created,user = UserModel.get_or_create(session=db.session,username=args.username)
user.email =  args.email if args.email is not None else user.email
user.pager = args.pager if args.pager is not None else user.pager
user.pageme = args.pageme if args.pageme is not None else user.pageme
if args.password is not None:
  user.hash_password(args.password)

db.session.commit()
db.session.refresh(user) 
if created:
  logging.info('{userid}:{username} have been created with {email}/{pager}'.format(userid=user.id,username=user.username,email=user.email,pager=user.pager))
else:
  logging.info('{userid}:{username} have been modified with {email}/{pager}'.format(userid=user.id,username=user.username,email=user.email,pager=user.pager))

if args.password is not None:
  logging.debug('and password |{password}|'.format(password=args.password))

