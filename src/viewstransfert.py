from __future__ import absolute_import
import os
from pathlib2 import Path
from datetime import datetime
from flask import Flask, request, abort,redirect, url_for,jsonify,make_response,current_app, g
from flask_restful import Resource, reqparse, fields, marshal_with,inputs
from flasgger import swag_from
from time import sleep
import uuid
import json
from celery import chain,Signature


from models import (db,
                    TransfertModel,JobModel,
                    TransfertSchema,JobSchema,
                    JobsStatusSchema,JobStatusTransfertSchema)

from viewsauth import multi_auth

class JobsStatusView(Resource):
  job_fields = {
    'id':fields.Integer,
    'jobname':fields.String,
    'checkhash':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
    'begin_on':fields.DateTime(dt_format='rfc822'),
    'finished_on': fields.DateTime(dt_format='rfc822'),
    'transferts' : fields.List(fields.Nested({'id':fields.Integer,'status':fields.String,'step':fields.String,'updated_on':fields.DateTime(dt_format='rfc822'),'root_source':fields.String,'root_target':fields.String,'path':fields.String,'filename':fields.String,'size':fields.Integer})),
  }

  @multi_auth.login_required
  @swag_from({
      'tags': ['jobs'],
      'summary': 'get information about a job in a choosen status',
      'definitions': {
        'JobStatusTransfers': {
          'type': 'array',          
          'items': {
            '$ref' : '#/definitions/JobStatusTransfert',
          }
        },
      },
      'parameters':[
          { 'in': 'path','name': 'id' ,'type': 'integer', 'description':'id of a job' },
          { 'in': 'path','name': 'jobname' ,'type': 'string','description':'name of a job'},
          { 'in': 'path','name': 'labelstatus' ,'type': 'string','description':'Status'},
      ],
      'responses': {
        200: {
          'description': "Retrieve counter and link information by status for a job (by id or jobname)",
          'examples': {
            'samples' : '{"processing": {\r "count": 3,\r"links": "/jobs/testnfsclient20/status/processing/"\r},\r"getsize-failed": {\r "count": 19,\r"links": "/jobs/testnfsclient20/status/getsize-failed/"\r} }'
          }
        },
        201: {
          'description': "Retrieve transferts in a choosen status for a job (by id or jobname)",
           'schema' : {
              '$ref' : '#/definitions/JobStatusTransfers',
           }
        },
        404: {
          'description': 'A job with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,jobname=None,labelstatus=None):

    if id is None and jobname is None:
      abort(404,"A job name or a job id must be pass '/jobs/<id>/status' or '/jobs/<jobname>/status' or '/jobs/<id>/status/<labelstatus>/' or '/jobs/<jobname>/status/<labelstatus>/' ")
    if id is not None and jobname is not None:  
      abort(404,"A job name or a job id must be pass '/jobs/<id>/status' or '/jobs/<jobname>/status' or '/jobs/<id>/status/<labelstatus>/' or '/jobs/<jobname>/status/<labelstatus>/' ")

    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
    if labelstatus is None :
      summary = {}
      for labelstatus, count in job.summary.items():
        if labelstatus not in  ['total','jobsize']:
          summary[labelstatus] = {'count' : count, 'links' : '/jobs/{job}/status/{labelstatus}/'.format(job=job.jobname,labelstatus=labelstatus)}
      return summary, 200
    else:
      transferts = [ transfert.to_dict() for transfert in TransfertModel.get(session=db.session, jobid=job.id, status = labelstatus) ]
      return transferts, 201

class JobsListView(Resource):
  job_fields = {
    'id':fields.Integer,
    'jobname':fields.String,
    'checkhash':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'begin_on':fields.DateTime(dt_format='rfc822'),
    'finished_on': fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
    'summary':fields.String,
    'updated_summary':fields.DateTime(dt_format='rfc822'),
    'enable_summary': fields.String,
  }

  
  @multi_auth.login_required
  @marshal_with(job_fields)
  @swag_from({
      'tags': ['jobs'],
      'summary': 'get all jobs list',
      'definitions': {
        'Jobs': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/Job',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. A list of Jobs",
           'schema' : {
             '$ref' : '#/definitions/Jobs',
           }
        },
        404: {
          'description': 'A job with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,jobname=None):
    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
      if not job:
        abort(404,"That job id {} does not exists".format(id))
      return job
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
      if not job:
        abort(404,"That job name {} does not exists".format(jobname))
      return job      
    else:
      jobs = JobModel.get_all(session=db.session)
      if len(jobs) == 0:
        jobs = []
      return jobs

class JobView(Resource):
  job_fields = {
    'id':fields.Integer,
    'jobname':fields.String,
    'checkhash':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'begin_on':fields.DateTime(dt_format='rfc822'),
    'finished_on': fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
    'summary':fields.String,
    'updated_summary':fields.DateTime(dt_format='rfc822'),
    'enable_summary': fields.String,
  }


  def __init__(self):
    self.put_parser = reqparse.RequestParser()
    self.put_parser.add_argument('enable_summary',
                             type=inputs.boolean,
                             required=True,
                             help='activation du la fonctionnalité de summarization',
                             location='json'
    )

  @multi_auth.login_required
  @marshal_with(job_fields)
#  @swag_from({
#    'tags': ['job'],
#    'summary': 'modify enable_summary of a job by id or jobname',
#    'parameters' : [
#      { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a job' },
#      { 'in': 'path','name': 'jobname' ,'type': 'string','description':'name of a job' },
#      { 'in': 'body','name':'status', 'type': 'boolean',"enum": ["True", "False"], 'examples' : {"enable_summary":False}}
#    ],
#    'responses': {
#      200: {
#        'description': "Modify is OK. Give modified job data",
#        #  'schema' : {
#        #    '$ref' : '#/definitions/Job',
#        #  }
#      },
#      404: {
#        'description': 'A transfert with this id or uuid does not exist',
#      },
#      401: {
#        'description': 'Not authorized. Please provide credentials',
#      },
#      409: {
#        'description': "Only only \"enable_summary\" is writeable with 'True' or 'False' value.",
#      }
#    }
#  })
  def put(self,id=None,jobname=None):
    args = self.put_parser.parse_args()
    if len(args.keys()) != 1 or 'enable_summary' not in args.keys():
      abort(409, "Only only \"enable_summary\" is writeable. {keys}".format( keys=args.keys()))
    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
      if not job:
        abort(404,"That job id {} does not exists".format(id))
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
      if not job:
        abort(404,"That job name {} does not exists".format(jobname))
    else:
      abort(404,"That job id {} or name {} does not exists".format(id,name))
    
    job.enable_summary = args['enable_summary']
    db.session.commit()
    db.session.refresh(job)
    return job

       
  
  @multi_auth.login_required
  @marshal_with(job_fields)
  @swag_from({
      'tags': ['job'],
      'summary': 'get a job by id or jobname',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a job' },
        { 'in': 'path','name': 'jobname' ,'type': 'string','description':'name of a job' }
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. Job data",
           'schema' : {
             '$ref' : '#/definitions/Job',
           }
        },
        404: {
          'description': 'A job with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,jobname=None):
    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
      if not job:
        abort(404,"That job id {} does not exists".format(id))
      return job
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
      if not job:
        abort(404,"That job name {} does not exists".format(jobname))
      return job      
    else:
      jobs = JobModel.get_all(session=db.session)
      if len(jobs) == 0:
        jobs = []
      return jobs


class JobsSummarizeView(Resource):

  @swag_from({
      'tags': ['jobs','jobsummaries'],
      'summary': 'Launch summarize task of all jobs which enable_summary is True',
      'responses': {
        200: {
          'description': "Summarize jobs is enqueued. Return jobs data",
           'schema' : {
             '$ref' : '#/definitions/Jobs',
           }
        },
        404: {
          'description': 'A job with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,jobname=None):
    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
      if not job:
        abort(404,"That job id {} does not exists".format(id))
      summarize = Signature('tasks_transfert.summarize_job',args=(job.id,))
      result = summarize.apply_async(queue='periodic')
      sleep(5)
      return redirect('/jobs/{id}'.format(id=id))
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
      if not job:
        abort(404,"That job name {} does not exists".format(jobname))
      summarize = Signature('tasks_transfert.summarize_job',args=(job.id,))
      result = summarize.apply_async(queue='periodic')
      sleep(5)
      return redirect('/jobs/{jobname}'.format(jobname=jobname))
    else:
      summarize = Signature('tasks_transfert.summarize_job')
      result = summarize.apply_async(queue='periodic')
      sleep(15)
      return redirect('/jobs/')

class JobSummarizeView(Resource):
  @swag_from({
      'tags': ['job','jobsummary'],
      'summary': 'Launch summarize task of a job by id or jobname even if  enable_summary is True ',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a job' },
        { 'in': 'path','name': 'jobname' ,'type': 'string','description':'name of a job' }
      ],
      'responses': {
        200: {
          'description': "Summarize job is enqueued. Job data",
           'schema' : {
             '$ref' : '#/definitions/Job',
           }
        },
        404: {
          'description': 'A job with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,jobname=None):
    """ Launch summarize task of a job item by id or jobname or all"""
    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
      if not job:
        abort(404,"That job id {} does not exists".format(id))
      summarize = Signature('tasks_transfert.summarize_job',args=(job.id,))
      result = summarize.apply_async(queue='periodic')
      sleep(5)
      return redirect('/jobs/{id}'.format(id=id))
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
      if not job:
        abort(404,"That job name {} does not exists".format(jobname))
      summarize = Signature('tasks_transfert.summarize_job',args=(job.id,))
      result = summarize.apply_async(queue='periodic')
      sleep(5)
      return redirect('/jobs/{jobname}'.format(jobname=jobname))
    else:
      summarize = Signature('tasks_transfert.summarize_job')
      result = summarize.apply_async(queue='periodic')
      sleep(15)
      return redirect('/jobs/')



class JobsDetailedView(Resource):
  job_fields = {
    'id':fields.Integer,
    'jobname':fields.String,
    'checkhash':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
    'begin_on':fields.DateTime(dt_format='rfc822'),
    'finished_on': fields.DateTime(dt_format='rfc822'),
    'transferts' : fields.List(fields.Nested({'id':fields.Integer,'status':fields.String,'updated_on':fields.DateTime(dt_format='rfc822'),'root_source':fields.String,'root_target':fields.String,'path':fields.String,'filename':fields.String,'size':fields.Integer})),
  }


  def __init__(self):
    self.parser = reqparse.RequestParser()
    self.parser.add_argument('jobname',
                             type=str,
                             required=True,
                             help='job name',
                             location='json'
    )
  
  @multi_auth.login_required
  @marshal_with(job_fields)
  def get(self,id=None,jobname=None):
    """ Retrieve a job item by id or jobname or all with transferts"""
    if id is not None:
      job = JobModel.get_one(session = db.session , id = id)
      if not job:
        abort(404,"That job id {} does not exists".format(id))
      return job
    elif jobname is not None:
      job = JobModel.get_one(session = db.session , jobname = jobname)
      if not job:
        abort(404,"That job name {} does not exists".format(jobname))
      return job  
    else:
      jobs = JobModel.get_all(session=db.session)
      if len(jobs) == 0:
        jobs = []

      return jobs



class TransfertsListView(Resource):
  """
  This view manipulate transfert
  
  """

  resource_fields = {
    'id':fields.Integer,
    'root_source':fields.String,
    'root_target':fields.String,
    'path':fields.String,
    'filename':fields.String,
    'size':fields.Integer,
    'status':fields.String,
    'step':fields.String,
    'checksum_source':fields.String,
    'checksum_target':fields.String,
    'checksum_geosyncsite':fields.String,
    'message':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
    'jobname':fields.String,
  }


  def __init__(self):
    self.post_parser = reqparse.RequestParser()
    self.post_parser.add_argument('root_source',
                                type=str,
                                required=True,
                                help='Root dir of the source',
                                location='json'                
    )
    self.post_parser.add_argument('root_target',
                                type=str,
                                required=True,
                                help='Root dir of the target',
                                location='json'                
    )
    self.post_parser.add_argument('path',
                                type=str,
                                required=True,
                                help='path of the file relative to the root_source dir',
                                location='json'                
    )
    self.post_parser.add_argument('filename',
                                type=str,
                                required=True,
                                help='name of the file without path',
                                location='json'                
    )

    self.post_parser.add_argument('jobname',
                                type=str,
                                required=False,
                                help='name of the job. jobid or jobname is required ',
                                location='json'                
    )
    

    self.post_parser.add_argument('checkhash',
                                type=str,
                                required=False,
                                help='hash used for checksum verification. default SHA-256. only usable when jobname is given for a non previouly created job',
                                default='SHA-256',
                                location='json'
    )

    self.post_parser.add_argument('jobid',
                                type=str,
                                required=False,
                                help='id of a previouly created job. jobid or jobname is required ',
                                location='json'
    )
 
  @multi_auth.login_required
  @marshal_with(resource_fields)
  @swag_from({
      'tags': ['transferts'],
      'summary': 'get all transferts',
      'definitions': {
        'Transferts': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/Transfert',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. A list of Transferts",
           'schema' : {
             '$ref' : '#/definitions/Transferts',
           }
        },
        404: {
          'description': 'A transfert with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,):
    transferts = TransfertModel.get_all(session=db.session)
    if len(transferts) == 0:
      transferts = []
    return transferts

  @multi_auth.login_required
  #@marshal_with(transfert_postput_fields)
  @swag_from({
      'tags': ['transferts'],
      'summary': 'create a new  transfert instance',
      'parameters' : [
        { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/PostTransfert'}},
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. Give tranfert data",
           'schema' : {
             '$ref' : '#/definitions/Transfert',
           }
        },
        404: {
          'description': 'A transfert with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def post(self):
    """ create a new  transfert"""
    args = self.post_parser.parse_args()
    if 'jobname' in args.keys():
      created,job = JobModel.get_or_create(db.session,jobname = args.get('jobname') )
      if created :
        job.checkhash = args['checkhash']
        db.session.commit()
    
    if 'jobid' in args.keys() and args.get('jobid') is not None :
      job = JobModel.get_one(db.session, id= args.get('jobid') )

    path = Path(args['root_target']).joinpath(args['path'],args['filename']).resolve()
    uuidgenerator = str(uuid.uuid5(uuid.NAMESPACE_DNS, str(path)))
    transfert = TransfertModel(
                               uuid=uuidgenerator,
                               root_source=args['root_source'],
                               root_target=args['root_target'],
                               path=args['path'],
                               filename=args['filename'],
                               status='new',
                               jobmodel=job,
                )
    db.session.add(transfert)
    db.session.commit()
    db.session.flush()
    db.session.refresh(transfert)
    jsoncopyjobdef = json.dumps(transfert.to_dict())
    chained_tasks = chain([
                            Signature('tasks_transfert.instance',args=(jsoncopyjobdef,),immutable=False) ,
                            #Signature('tasks_transfert.duration',immutable=False),
                          ])
    result = chained_tasks.apply_async(countdown=current_app.config['TASK_DELAY'])
    return jsonify({**transfert.to_dict(),'taskid':str(result)})


def put_status_resume(string):
  """ type pour un  argument du parser self.put_parser qui n'autorise qu'une valeur de status a resume checkongeos"""
  if string in ['resume','checkongeos']:
    return string
  else:
    raise AttributeError(string)

class TransfertView(Resource):
  """
  This view manipulate transfert
  
  """

  resource_fields = {
    'id':fields.Integer,
    'root_source':fields.String,
    'root_target':fields.String,
    'path':fields.String,
    'filename':fields.String,
    'size':fields.Integer,
    'status':fields.String,
    'step':fields.String,
    'checksum_source':fields.String,
    'checksum_target':fields.String,
    'checksum_geosyncsite':fields.String,
    'message':fields.String,
    'created_on':fields.DateTime(dt_format='rfc822'),
    'updated_on':fields.DateTime(dt_format='rfc822'),
    'jobname':fields.String,
  }


  def __init__(self):

    self.put_parser = reqparse.RequestParser()
    self.put_parser.add_argument('status',
                                type=put_status_resume,
                                required=True,
                                help='Status of the tranfert. only resume is allowed',
                                location='json'                
    )
 
  @multi_auth.login_required
  @marshal_with(resource_fields)
  @swag_from({
      'tags': ['transfert'],
      'summary': 'get a transfert by id or uuid',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a transfert' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a transfert' }
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. Give tranfert data",
           'schema' : {
             '$ref' : '#/definitions/Transfert',
           }
        },
        404: {
          'description': 'A transfert with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,uuid=None):
    if id is not None:
      transfert = TransfertModel.get_one(session=db.session,id = id)
      if not transfert:
        abort(404,"That transfert id {} does not exists".format(id))
      return transfert
    elif uuid is not None:
      transfert = TransfertModel.get_one(session=db.session,uuid = uuid)
      if not transfert:
        abort(404,"That transfert uuid {} does not exists".format(uuid))
      return transfert

    else:
      transferts = TransfertModel.get_all(session=db.session)
      if len(transferts) == 0:
        transferts = []
      return transferts

#  @swag_from({
#    'tags': ['transfert'],
#    'summary': 'modify status of a transfert by id or uuid',
#    'parameters' : [
#      { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a transfert' },
#      { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a transfert' },
#      { 'in': 'body','name':'status', 'type': 'string',"enum": ["resume", "checkongeos"], 'examples' : '{"status":"resume"}'}
#    ],
#    'responses': {
#      200: {
#        'description': "Modify is OK. Give tranfert data",
#          #'schema' : {
#          #  '$ref' : '#/definitions/Transfert',
#          #}
#      },
#      404: {
#        'description': 'A transfert with this id or uuid does not exist',
#      },
#      401: {
#        'description': 'Not authorized. Please provide credentials',
#      },
#      409: {
#        'description': "Only only \"status\" is writeable with 'resume' or 'checkongeos' value.",
#      }
#    }
#  })
  @multi_auth.login_required
  def put(self, id=None,uuid=None):
    args = self.put_parser.parse_args()
    if len(args.keys()) != 1 or 'status' not in args.keys():
      abort(409, "Only only \"status\" is writeable. {keys}".format( keys=args.keys()))
    if 'status' in args.keys() and args['status'] not in  ['resume','checkongeos']:
      abort(409, "Only { \"status\" : \"resume\" } or { \"status\" : \"checkongeos\" } is allowed.  {status}".format( status=args['status']))

    if id is not None:
      transfert = TransfertModel.get_one(session=db.session,id = id)
    elif uuid is not None:
      transfert = TransfertModel.get_one(session=db.session,uuid = uuid)
    
    if not transfert:
      abort(404, "That id {transfert_id} or uuid {uuid} does not exist in the database".format(transfert_id=id,uuid=uuid))

    transfert.status = args['status']
    db.session.commit()
    jsoncopyjobdef = json.dumps(transfert.to_dict())
    if args['status'] == 'resume':
      chained_tasks = chain([
                              Signature('tasks_transfert.instance',args=(jsoncopyjobdef,),immutable=False),
                              #Signature('tasks_transfert.duration',immutable=False),
                            ])
      result = chained_tasks.apply_async(countdown=current_app.config['TASK_DELAY'])
    elif args['status'] == 'checkongeos':
      chained_tasks = chain([
                              Signature('tasks_transfert.checkongeos',args=(jsoncopyjobdef,),immutable=False),
                              #Signature('tasks_transfert.duration',immutable=False),
                            ])
      result = chained_tasks.apply_async(countdown=current_app.config['TASK_DELAY'])
    
    db.session.refresh(transfert)
    return jsonify({**transfert.to_dict(),'taskid':str(result)})
