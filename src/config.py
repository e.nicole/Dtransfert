from __future__ import absolute_import
import os
from environs import Env
from celery.schedules import crontab

class Config(object):
  env = Env()
  ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__)))
  env_file = os.path.join(ROOT_DIR, '.env')
  if os.path.exists(env_file):
    env.read_env(env_file)
  DEBUG = env.bool('DEBUG', default=False)
  SQLALCHEMY_ECHO = env('SQLALCHEMY_ECHO',default=False)
  SQLALCHEMY_TRACK_MODIFICATIONS = env('SQLALCHEMY_TRACK_MODIFICATIONS',default=True)
  SQLALCHEMY_DATABASE_URI = env('SQLALCHEMY_DATABASE_URI',default="postgresql://username:password@localhost/database_name")
  SQLALCHEMY_POOL_SIZE = env.int('SQLALCHEMY_POOL_SIZE',default=10)
  SQLALCHEMY_MAX_OVERFLOW = env.int('SQLALCHEMY_MAX_OVERFLOW',default=10)

  TASK_DELAY=env.int('TASK_DELAY',default=10)

  TOKEN_SECRET_KEY=env('TOKEN_SECRET_KEY')
  TOKEN_EXPIRATION=env.int('TOKEN_EXPIRATION',default=600)

  CELERY_BROKER = env('CELERY_BROKER',default="amqp://username:password@0.0.0.0/default_vhost")
  CELERY_RESULT = env('CELERY_RESULT',default="db+postgresql://username:password@localhost/database_name")
  CELERY_SEND_EVENTS = env.bool('CELERY_SEND_EVENTS',default=True)
  CELERY_SEND_SENT_EVENT = env.bool('CELERY_SEND_SENT_EVENT',default=True)
  CELERYD_MAX_TASKS_PER_CHILD = env.int('CELERYD_MAX_TASKS_PER_CHILD',default=500)
  CELERYD_PREFETCH_MULTIPLIER = env.int('CELERYD_PREFETCH_MULTIPLIER',default=1)
  database_engine_options = {'echo': True}

  XMPP_SERVER=env('XMPP_SERVER',default=None)
  XMPP_USER=env('XMPP_USER',default=None)
  XMPP_PASSWORD=env('XMPP_PASSWORD',default=None)
  XMPP_ROOM=env('XMPP_ROOM',default=None)
  XMPP_PORT=env('XMPP_PORT',default=5222)
  
  TIMEZONE=env('TIMEZONE',default='Europe/Paris')

  HEALTCHECK_CRONMIN = env('HEALTCHECK_CRONMIN',default='*/5')

class DevelopmentConfig(Config):
  DEBUG = True




class ProductionConfig(Config):
 pass

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
}
