
from celery.schedules import crontab

from factory import create_app,create_celery_app,create_swagger_app

from tasks_transfert import summarize_job, alert_for_dead_job
from tasks_healthcheck import geosmetrics,geosindicators,snmpsourcehealthcheck,snmptargethealthcheck,snmpworkershealthcheck,manageworkers

app = create_app()
app.app_context().push()
celery_ins = create_celery_app(app) 
celery_ins.conf.update(app.config)

celery_ins.conf["broker_url"] = app.config['CELERY_BROKER']
celery_ins.conf["result_backend"] = app.config['CELERY_RESULT']
celery_ins.conf["worker_send_task_events"] = app.config['CELERY_SEND_EVENTS']
celery_ins.conf["task_send_sent_event"] = app.config['CELERY_SEND_SENT_EVENT']
celery_ins.conf["worker_max_tasks_per_child"] = app.config['CELERYD_MAX_TASKS_PER_CHILD']
celery_ins.conf["worker_prefetch_multiplier"] = app.config['CELERYD_PREFETCH_MULTIPLIER']
#celery_ins.conf["database_engine_options"] = app.config['CELERY_RESULT_ENGINE_OPTIONS']



swagger = create_swagger_app(app) 

celery_ins.conf.beat_schedule = {
  "summarize_beat": {
    "task": "tasks_transfert.summarize_job",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute='*/10', hour='*'),
  },
  # décommenter le bloc ci-dessous pour activer les alertes sur les jobs qui n'ont pas été mis à jour depuis longtemps (par défaut 1800 sec)
  """
  "alert_for_dead_job_beat": {
    "task": "tasks_transfert.alert_for_dead_job",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute='0,30', hour='*'),
  },
  """
  # décommenter le bloc ci-dessous pour activer les vérification des  métriques geos (SCALITY RING V7)
  """
  "geosmetrics_beat": {
    "task": "tasks_healthcheck.geosmetrics",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute=app.config['HEALTCHECK_CRONMIN'], hour='*'),
  },
  """
  # décommenter le bloc ci-dessous pour activer les vérification de la santé de geos (SCALITY RING V7)
  """
  "geosindicators_beat": {
    "task": "tasks_healthcheck.geosindicators",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute=app.config['HEALTCHECK_CRONMIN'], hour='*'),
      #"kwargs" : { },
  },
  """
  # décommenter le bloc ci-dessous pour vérifier la santé de la plateforme source dans ce cas là il faut activer snmp sur la source
  """
  "snmpsource_beat" :  {
    "task": "tasks_healthcheck.snmpsourcehealthcheck",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute=app.config['HEALTCHECK_CRONMIN'], hour='*'),
  },
  """

  # décommenter le bloc ci-dessous pour vérifier la santé de la plateforme destination dans ce cas là il faut activer snmp sur la destination
  """
  "snmptarget_beat" :  {
    "task": "tasks_healthcheck.snmptargethealthcheck",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute=app.config['HEALTCHECK_CRONMIN'], hour='*'),
  },
  """
  
  "snmpworker_beat" :  {
    "task": "tasks_healthcheck.snmpworkershealthcheck",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute=app.config['HEALTCHECK_CRONMIN'], hour='*'),
  },

  "worker_beat" :  {
    "task": "tasks_healthcheck.manageworkers",
      "options" :{'queue': 'periodic'},
      "schedule": crontab(minute=app.config['HEALTCHECK_CRONMIN'], hour='*'),
  },
  

}

if __name__ == '__main__' :
  app.run(host='127.0.0.1')
