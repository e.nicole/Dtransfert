import ast, json
from celery.schedules import crontab
from factory import create_app,create_celery_app,create_swagger_app
from models import db,TransfertTracingModel
from tasks_transfert import summarize_job, alert_for_dead_job
from tasks_healthcheck import geosmetrics,geosindicators,snmpsourcehealthcheck,snmptargethealthcheck,snmpworkershealthcheck,manageworkers

app = create_app()
app.app_context().push()
celery_ins = create_celery_app(app) 
celery_ins.conf.update(app.config)

celery_ins.conf["broker_url"] = app.config['CELERY_BROKER']
celery_ins.conf["result_backend"] = app.config['CELERY_RESULT']
celery_ins.conf["worker_send_task_events"] = app.config['CELERY_SEND_EVENTS']
celery_ins.conf["task_send_sent_event"] = app.config['CELERY_SEND_SENT_EVENT']
celery_ins.conf["worker_max_tasks_per_child"] = app.config['CELERYD_MAX_TASKS_PER_CHILD']
celery_ins.conf["worker_prefetch_multiplier"] = app.config['CELERYD_PREFETCH_MULTIPLIER']
#celery_ins.conf["database_engine_options"] = app.config['CELERY_RESULT_ENGINE_OPTIONS']


def monitor(app):
  state = app.events.State()
  def succeeded_task(event):
    state.event(event)
    task = state.tasks.get(event['uuid'])
    print("task succeeded {task_name}:{task_uuid} on {host}".format(task_name=task.name,task_uuid=task.uuid,host=task.hostname))
    if task.name is None or task.name in [ 'tasks_transfert.instance', 'tasks_transfert.checkongeos'] :
      task_result = ast.literal_eval(task.result)
      if 'task_name' in task_result:
        print("task {task_name}:{task_result}".format(task_name=task_result.get('task_name'),task_result=task_result))
        if task_result.get('task_name') in [ 'tasks_transfert.instance', 'tasks_transfert.checkongeos'] :
          created, tf_trace = TransfertTracingModel.get_or_create(db.session,
                                                                  transfert_uuid = task_result.get('uuid'),
                                                                  task_uuid = task.uuid,
                                                                  task_name = task_result.get('task_name'),
                                                                  runtime = task.runtime,
                                                                  size =  task_result.get('size'),
                                                                  jobname = task_result.get('jobname'),)

          
  with app.connection() as connection:
    recv = app.events.Receiver(connection,
                               handlers={'task-succeeded': succeeded_task,'*': state.event}
                              )
    recv.capture(limit=None,timeout=None, wakeup=True)


monitor(celery_ins)

