from __future__ import absolute_import
import os
from pathlib2 import Path
from datetime import datetime
from flask import Flask, request, abort,redirect, url_for,jsonify,make_response,current_app, g
from flask_restful import Resource, reqparse, fields, marshal_with,inputs
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth, MultiAuth
from flasgger import swag_from
from time import sleep
import uuid
import json



from models import (db,
                    UserModel,
                    UserSchema)

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth('Bearer')
multi_auth = MultiAuth(basic_auth, token_auth)

@basic_auth.verify_password
def verify_password(username, password):
  g.user = None
  user = UserModel.query.filter_by(username = username).first()
  if not user or not user.verify_password(password):
    return False
  g.user = user
  return True


@token_auth.verify_token
def verify_token(token):
  g.user=None
  user = UserModel.verify_auth_token(token)
  if not user :
    return False
  g.user=user
  return True


class TokensView(Resource):
  token_fields = {
    'token': fields.String,
  }
  @basic_auth.login_required
  @swag_from({
      'tags': ['auth','token'],
      'summary': 'get a token for the user specified with basic http authentication. Basic authentication is required to get a token.(<a href="https://tools.ietf.org/html/rfc7617">See RFC7617</a>',
      'definitions': {
        'Token': {
          'type': 'object',
          'properties' : {
            'token': {
              'type': 'string'
            }
          } 
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. Username and password are correct ",
          'schema': {
            'id': 'Token',
          }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self):
    token = g.user.generate_auth_token(expiration=current_app.config['TOKEN_EXPIRATION'])
    return jsonify({ 'token': token.decode('ascii') })


class UsersListView(Resource):
  users_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'username':fields.String,
    'email':fields.String,
    'pager':fields.String,
    'pageme': fields.Boolean,
  }


  def __init__(self):
    self.post_parser = reqparse.RequestParser()
    self.post_parser.add_argument('username',
                                  type=str,
                                  required=True,
                                  help='Username',
                                  location='json'
    )
    self.post_parser.add_argument('email',
                                  type=str,
                                  required=True,
                                  help='email',
                                  location='json'
    )
    self.post_parser.add_argument('pager',
                                  type=str,
                                  required=False,
                                  help='pager',
                                  location='json'
    )
    self.post_parser.add_argument('password',
                                  type=str,
                                  required=True,
                                  help='password',
                                  location='json'
    )
    self.post_parser.add_argument('pageme',
                                  type=inputs.boolean,
                                  help='pageme',
                                  location='json'
    )


  @multi_auth.login_required
  @marshal_with(users_fields)
  @swag_from({
      'tags': ['auth','users'],
      'summary': 'get all users',
      'definitions': {
        'Users': {
          'type': 'array',
          'items': {
            '$ref' : '#/definitions/User',
          }
        }
      },
      'responses': {
        200: {
          'description': "Retrieve is OK. A list of Users",
           'schema' : {
             '$ref' : '#/definitions/Users',
           }
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self):
    users = UserModel.get_all(session=db.session)
    if len(users) == 0:
      users = []
    return users

  @multi_auth.login_required
  @marshal_with(users_fields)
  @swag_from({
      'tags': ['auth','users'],
      'summary': 'create a new  user ',
      'parameters' : [
        { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/User'}},
      ],
      'responses': {
        200: {
          'description': "Creation is OK. Give newly user data",
           'schema' : {
             '$ref' : '#/definitions/User',
           }
        },
        403: {
          'description': 'A user already exists with this username',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def post(self):
    args = self.post_parser.parse_args()


    if UserModel.query.filter_by(username = args['username']).first() is not None:
      abort(403,"this username {} already exist".format(args['username']))
    user = UserModel(
      username=args['username'],
      email=args['email'],
      pager=args['pager'],
      pageme=args['pageme'],
    )
    user.hash_password(args['password'])
    db.session.add(user)
    db.session.commit()
    return user

class UserView(Resource):
  users_fields = {
    'id':fields.Integer,
    'uuid':fields.String,
    'username':fields.String,
    'email':fields.String,
    'pager':fields.String,
    'pageme': fields.Boolean,
  }


  def __init__(self):
    self.put_parser = reqparse.RequestParser()

    self.put_parser.add_argument('email',
                                  type=str,
                                  required=False,
                                  help='email',
                                  location='json'
    )
    self.put_parser.add_argument('pager',
                                  type=str,
                                  required=False,
                                  help='pager',
                                  location='json'
    )
    self.put_parser.add_argument('password',
                                  type=str,
                                  required=False,
                                  help='password',
                                  location='json'
    )
    self.put_parser.add_argument('pageme',
                                  type=inputs.boolean,
                                  required=False,
                                  help='pageme',
                                  location='json'
    )

  @multi_auth.login_required
  @marshal_with(users_fields)
  @swag_from({
      'tags': ['auth','user'],
      'summary': 'get a user by id or uuid',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a user' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a user' }
      ],
      'responses': {
        200: {
          'description': "Retrieve is OK. Give user data",
           'schema' : {
             '$ref' : '#/definitions/User',
           }
        },
        404: {
          'description': 'A user with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def get(self,id=None,uuid=None):
    if id is not None:
      user = UserModel.get_one(session=db.session,id = id)
      if not user:
        abort(404,"That user id {} does not exists".format(id))
      return user
    elif uuid is not None:
      user = UserModel.get_one(session=db.session,uuid = uuid)
      if not user:
        abort(404,"That user uuid {} does not exists".format(uuid))
      return user
    else:
      abort(404,'id or uuid is not known : id {id}. uuid {uuid}.'.format(id=id, uuid=uuid))

      

  @multi_auth.login_required
  @marshal_with(users_fields)
  @swag_from({
      'tags': ['auth','user'],
      'summary': 'modify a user by id or uuid',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a user' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a user' },
        { 'in': 'body','name': 'body' ,'schema': {'$ref':'#/definitions/User'}},
      ],
      'responses': {
        200: {
          'description': "Modify is OK. Give user data",
           'schema' : {
             '$ref' : '#/definitions/User',
           }
        },
        404: {
          'description': 'A user with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  def put(self,id=None,uuid=None):
    """ modify  a user item by id or all"""
    args = self.put_parser.parse_args()

    if id is not None:
      user = UserModel.get_one(session=db.session,id = id)
      if not user:
        abort(404,"That user id {} does not exists".format(id))
    elif uuid is not None:
      user = UserModel.get_one(session=db.session,uuid = uuid)
      if not user:
        abort(404,"That user uuid {} does not exists".format(uuid))
    else:
      abort(404,"That user id {} or uuid {} does not exists".format(id,uuid))

    user.email = args['email'] if args['email'] is not None else user.email
    user.pager = args['pager'] if args['pager'] is not None else user.pager
    user.pageme = args['pageme'] if args['pageme'] is not None else user.pageme
    if args['password'] is not None:
      user.hash_password(args['password'])
    db.session.commit()
    db.session.refresh(user)
    return user

  @swag_from({
      'tags': ['auth','user'],
      'summary': 'delete a user by id or uuid',
      'parameters' : [
        { 'in': 'path','name': 'id' ,'type': 'integer','description':'id of a user' },
        { 'in': 'path','name': 'uuid' ,'type': 'string','description':'uuid or a user' }
      ],
      'responses': {
        200: {
          'description': "delete is OK. Give deleted user data",
           'schema' : {
             '$ref' : '#/definitions/User',
           }
        },
        404: {
          'description': 'A user with this id or uuid does not exist',
        },
        401: {
          'description': 'Not authorized. Please provide credentials',
        }
      }
  })
  @multi_auth.login_required
  @marshal_with(users_fields)
  def delete(self,id=None,uuid=None):
    if id is not None:
      user = UserModel.get_one(session=db.session,id = id)
      if not user:
        abort(404,"That user id {} does not exists".format(id))
    elif uuid is not None:
      user = UserModel.get_one(session=db.session,uuid = uuid)
      if not user:
        abort(404,"That user uuid {} does not exists".format(uuid))
    else:
      abort(404,"That user id {} or uuid {} does not exists".format(id,uuid))

    db.session.delete(user)
    db.session.commit()
    return user


