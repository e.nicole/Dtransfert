from __future__ import absolute_import

from flask import request
from flask_restful import Resource
import json

class MimicGeos(Resource):
  mimicgeosfile = 'geos.json'
  def get(self):
    with open('geos.json') as jsonstream:
      data = json.load(jsonstream)

    return data

  def post(self):
    json_data = request.get_json(force=True)
    with open('geos.json','w') as jsonstream:
      json.dump(json_data,jsonstream)
    
    return json_data
