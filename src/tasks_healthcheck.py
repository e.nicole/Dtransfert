from __future__ import absolute_import
import os
from pathlib2 import Path
from environs import Env
import shutil
import json
from datetime import datetime
from time import sleep
import requests
from flask import Flask, current_app
import sqlalchemy
from sqlalchemy.orm import sessionmaker,scoped_session
from sqlalchemy import func
from sqlalchemy import or_
from sqlalchemy import create_engine
import celery
from celery import Task,task
from celery import shared_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger
from celery.app.log import TaskFormatter
from celery.exceptions import Retry
from celery.five import monotonic
from celery.app.control import Inspect,Control
import hashlib
import socket
import delegator
from pysnmp.hlapi import *
from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto.rfc1905 import NoSuchObject
import re

#from flask import jsonify
from factory import create_celery_app,create_app
from models import db,ParamModel,HealthcheckModel,HealthCheckHistoryModel,UserModel
from utils import SendMsgBot



app = create_app()
app.app_context().push()
celery_ins = create_celery_app(app)
celery_ins.conf.update(app.config)
celery_ins.conf["broker_url"] = app.config['CELERY_BROKER']
celery_ins.conf["result_backend"] = app.config['CELERY_RESULT']
celery_ins.conf["worker_send_task_events"] = app.config['CELERY_SEND_EVENTS']
celery_ins.conf["task_send_sent_event"] = app.config['CELERY_SEND_SENT_EVENT']
celery_ins.conf["worker_max_tasks_per_child"] = app.config['CELERYD_MAX_TASKS_PER_CHILD']
celery_ins.conf["worker_prefetch_multiplier"] = app.config['CELERYD_PREFETCH_MULTIPLIER']
#celery_ins.conf["database_engine_options"] = app.config['CELERY_RESULT_ENGINE_OPTIONS']

logger = get_task_logger(__name__)

@after_setup_task_logger.connect
def setup_task_logger(logger, *args, **kwargs):
    for handler in logger.handlers:
        handler.setFormatter(TaskFormatter('%(asctime)s - %(task_id)s - %(task_name)s - %(name)s - %(levelname)s - %(message)s'))


class Healthcheck(Task):
  _session = None
  @property
  def session(self):
     
    if self._session is None:
      engine = create_engine(self.app.conf['SQLALCHEMY_DATABASE_URI'])
      
      Session = scoped_session(sessionmaker(bind=engine))
      self._session = Session()
    return self._session


  def get_paramaslist(self,key):
    paramvalue = self.get_param(key)
    if paramvalue is not None:
      return paramvalue.split(',')
    else:
      return paramvalue

  def get_param(self,key):
    with app.app_context():
      param =  ParamModel.get_one(session=self.session,key='{key}'.format(key=key))
      if param is not None:
        return param.value
      else:
        return None

  def historize(self,healthcheck):
    history = HealthCheckHistoryModel(
                            uuid = healthcheck.uuid,
                            source = healthcheck.source,
                            name = healthcheck.name,
                            value = healthcheck.value,
                            status = healthcheck.status,
                            reason = healthcheck.reason,
                            element = healthcheck.element,
                            created_on = healthcheck.created_on,
                            updated_on = healthcheck.updated_on,
    )
    self.session.add(history)
    self.session.commit()
    return history

    


class GeosHealthCheck(Healthcheck):
  source = 'GEOS'
  def geosapi(self):
    # initilisation de l'url de geos
    geosurl = self.get_param(key='{source}_URL'.format(source=self.source))
    if geosurl is not None:
      # TODO: si je ne récupere pas d'info de l'api geos, je dois arreter
      try:
        # recuperation des infos de geos
        geos_api = requests.get(geosurl)
        geos_api.raise_for_status()
      except requests.exceptions.HTTPError as errh:
        print ("Http Error:",errh)
      except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:",errc)
      except requests.exceptions.Timeout as errt:
        print ("Timeout Error:",errt)
      except requests.exceptions.RequestException as err:
        print ("OOps: Something Else",err)      

      # si la récuperation est OK. je transforme la réponse de l'api geos en objet json
      if geos_api.status_code == 200: 
        self.geos_json = json.loads(geos_api.text)
        return True
      else:
        self.geos_json = None
        return False
      

@celery.task(base=GeosHealthCheck,bind=True,resultrepr_maxsize=2048)
def geosindicators(self):
  with app.app_context():
    #pour chaque indicateur souhaité
    if self.geosapi():
      healthchecklist=[]
      for geoslabel in self.get_paramaslist(key='{source}_indicators'.format(source=self.source)):
        logger.info('Evaluating {}'.format(geoslabel))
        geosvalue = self.geos_json[geoslabel]
        created, healthcheck = HealthcheckModel.get_or_create(session=self.session, 
                                                          name=geoslabel, 
                                                          source=self.source)
        # pour la emtric teste, je vais cherche la valeur attendu
        geosexpectedvalue = self.get_param(key='{source}_{geoslabel}'.format(
                                                                                          source=self.source,
                                                                                          geoslabel=geoslabel,
                                                                                        )
                                      )
        logger.info('Expected value for {geoslabel} is {geosexpectedvalue}'.format(geoslabel=geoslabel,geosexpectedvalue=geosexpectedvalue))
        if geosexpectedvalue is None:
          raise ValueError("Param {source}_{geoslabel} is not know".format(
                                                                            source=self.source,
                                                                            geoslabel=geoslabel,
                                                                          ),
                          "Create an instance of param for {source}_{geoslabel}".format(
                                                                                          source=self.source,
                                                                                          geoslabel=geoslabel,
                                                                                        )
                          )
        
        # si c'est un création 
        if created :
          # le statut est a new
          status='new'
          reason=''
        elif healthcheck.status == 'new':
          # si le dernier statut est a new, je le passe par défaut a start
          status='start'
          reason=''
        else:
          # sinon je conserve le dernier statut
          status=healthcheck.status
          reason=healthcheck.reason
    
        if geosvalue is not None:
          if str(geosvalue) != str(geosexpectedvalue):
            status='stop'
            reason='{geoslabel}:{geosvalue} is different than {source}_{geoslabel}:{geosexpectedvalue}'.format(
                                                                                                                source=self.source,
                                                                                                                geoslabel=geoslabel,
                                                                                                                geosvalue=geosvalue,
                                                                                                                geosexpectedvalue=geosexpectedvalue,
                                                                                                              )
          else:
            status='start'
            reason='{geoslabel}:{geosvalue} is equal than {source}_{geoslabel}:{geosexpectedvalue}'.format(
                                                                                                            source=self.source,
                                                                                                            geoslabel=geoslabel,
                                                                                                            geosvalue=geosvalue,
                                                                                                            geosexpectedvalue=geosexpectedvalue,
                                                                                                          )
        # sauvegarde  en bdd
        healthcheck.status = status
        healthcheck.reason = reason
        healthcheck.value = str(geosvalue)
        #healthcheck.updated_on = db.func.now()
        self.session.flush()
        self.session.commit()
        self.session.refresh(healthcheck)
        history = self.historize(healthcheck)
        logger.info('{uuid}:{status}:{reason}'.format(uuid=healthcheck.uuid,status=healthcheck.status,reason=healthcheck.reason))
        healthchecklist.append(healthcheck.to_dict())
      return healthchecklist
  

@celery.task(base=GeosHealthCheck,bind=True,resultrepr_maxsize=2048)
def geosmetrics(self): 
  with app.app_context():
    # pour chaque indicateur souhaité  
    if self.geosapi():
      healthchecklist=[] 
      for geoslabel in self.get_paramaslist(key='{source}_metrics'.format(source=self.source)):
        logger.info('Evaluating {}:{}'.format(geoslabel,self.geos_json['metrics'][geoslabel]['value']))
        # je recupere la valeur de metrics 
        # l'api geos renvoi  None au lieu de 0 je force a 0 lorsque je detecte None pour que  la métrique soit bien evaluée
        geosvalue = self.geos_json['metrics'][geoslabel]['value'] if self.geos_json['metrics'][geoslabel]['value'] is not None else 0
        logger.info('Evaluating {}:{}'.format(geoslabel,geosvalue))
        created, healthcheck = HealthcheckModel.get_or_create(session=self.session, 
                                                          name=geoslabel, 
                                                          source=self.source)
    
        # pour la emtric teste, je vais cherche la valeur max
        geosmetricmax = self.get_param(key='{source}_{geoslabel}_MAX'.format(
                                                                                              source=self.source,
                                                                                              geoslabel=geoslabel,
                                                                                            )
                                      )
        logger.info('Max value for {geoslabel} is {geosmetricmax}'.format(geoslabel=geoslabel,geosmetricmax=geosmetricmax))
        if geosmetricmax is None:
          raise ValueError("Param {source}_{geoslabel}_MAX is not know".format(
                                                                                  source=self.source,
                                                                                  geoslabel=geoslabel,
                                                                                ),
                            "Create an instance of param for {source}_{geoslabel}_MIN".format(
                                                                                              source=self.source,
                                                                                              geoslabel=geoslabel,
                                                                                            )
                          )
        # et la valeur min  
        geosmetricmin = self.get_param(key='{source}_{geoslabel}_MIN'.format(
                                                                                              source=self.source,
                                                                                              geoslabel=geoslabel,
                                                                                            )
                                          )
        logger.info('Min value for {geoslabel} is {geosmetricmin}'.format(geoslabel=geoslabel,geosmetricmin=geosmetricmin))
        if geosmetricmin is None:
          raise ValueError("Param {source}_{geoslabel}_MIN is not know".format(
                                                                                source=self.source,
                                                                                geoslabel=geoslabel,
                                                                              ),
                            "Create an instance of param for {source}_{geoslabel}_MIN".format(
                                                                                              source=self.source,
                                                                                              geoslabel=geoslabel,
                                                                                            )
                          )
        
        # si c'est un création 
        if created :
          # le statut est a new
          status='new'
          reason=''
        elif healthcheck.status == 'new':
          # si le dernier statut est a new, je le passe par défaut a start
          status='start'
          reason=''
        else:
          # sinon je conserve le dernier statut
          status=healthcheck.status
          reason=healthcheck.reason  
        

        if int(geosvalue) > int(geosmetricmax):
          # geos indique un depassement par rapport au seuil fixe, le workload doit s'arreter
          status='stop'
          reason='{geoslabel}:{geosvalue} is greater than {source}_{geoslabel}_MAX:{geosmetricmax}'.format(
                                                                                                                source=self.source,
                                                                                                                geoslabel=geoslabel,
                                                                                                                geosvalue=geosvalue,
                                                                                                                geosmetricmax=geosmetricmax,
                                                                                                              )
        else:
          # la valeur de la metrics est en deca de la valeur acceptable je mets tout de meme a jour la raison
          reason='{geoslabel}:{geosvalue} is less than {source}_{geoslabel}_MAX:{geosmetricmax}'.format(
                                                                                                                source=self.source,
                                                                                                                geoslabel=geoslabel,
                                                                                                                geosvalue=geosvalue,
                                                                                                                geosmetricmax=geosmetricmax,
                                                                                                              )          
        if healthcheck.status == 'stop':
          # lorsque le dernier statut pour cette metric est stop
          if int(geosvalue) < int(geosmetricmin):
            # je teste un retour à la normale de l'indicateur
            status='start'
            reason='{geoslabel}:{geosvalue} is less than {source}_{geoslabel}_MIN:{geosmetricmin}'.format(
                                                                                                                  source=self.source,
                                                                                                                  geoslabel=geoslabel,
                                                                                                                  geosvalue=geosvalue,
                                                                                                                  geosmetricmin=geosmetricmin,
                                                                                                                )
          else:
            # si l'indicateur n'est pas revenu a la normale je mets tout de meme à jour le champ reason
            reason='{geoslabel}:{geosvalue} is greater than {source}_{geoslabel}_MIN:{geosmetricmin}'.format(
                                                                                                                  source=self.source,
                                                                                                                  geoslabel=geoslabel,
                                                                                                                  geosvalue=geosvalue,
                                                                                                                  geosmetricmin=geosmetricmin,
                                                                                                                )
        # sauvegarde  en bdd
        healthcheck.status = status
        healthcheck.reason = reason
        healthcheck.value = str(geosvalue)
        self.session.flush()
        self.session.commit()
        self.session.refresh(healthcheck)
        self.historize(healthcheck)
        logger.info('{uuid}:{status}:{reason}'.format(uuid=healthcheck.uuid,status=healthcheck.status,reason=healthcheck.reason))
        healthchecklist.append(healthcheck.to_dict())
    
      return healthchecklist

class SnmpHealthCheck(Healthcheck):
  source = 'SNMP'

  def snmpget(self,oid,SNMP_HOST,SNMP_PORT,SNMP_COMMUNITY):
    
  
  
    cmdGen = cmdgen.CommandGenerator()
  
    errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
      cmdgen.CommunityData(SNMP_COMMUNITY),
      cmdgen.UdpTransportTarget((SNMP_HOST, SNMP_PORT)),
      oid
    )
  
    # Check for errors and print out results
    if errorIndication:
      print(errorIndication)
    else:
      if errorStatus:
        print('%s at %s' % (
          errorStatus.prettyPrint(),
          errorIndex and varBinds[int(errorIndex)-1] or '?'
        )
      )
      else:
        for name, val in varBinds:
          #print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
          return val


  
  def get_snmpdict(self):
    oids_dict={}
    for snmplabel in self.get_paramaslist(key='{source}_labelsoids'.format(source=self.source)):
  
      oid = self.get_param(key='{source}_{snmplabel}'.format(
                                                                        source = self.source,
                                                                        snmplabel = snmplabel,
                                                                        )
                          )

      if oid is None:
        raise ValueError("Param {source}_{snmplabel} is not know".format(
                                                                          source=self.source,
                                                                          snmplabel=snmplabel,
                                                                        ),
                  "Create an instance of param for {source}_{snmplabel}".format(
                                                                            source=self.source,
                                                                            snmplabel=snmplabel,
                                                                            )
                )
      else:
        oids_dict[snmplabel]=oid 
    
    return oids_dict

  def getsimple(self,snmp_ipserver,element):
    with app.app_context(): 
      snmp_community = self.get_param(key="{source}_community".format(source=self.source))
      snmp_port = self.get_param(key="{source}_port".format(source=self.source))
    
      if snmp_ipserver is not None or snmp_community is not None or snmp_port is not None:
        healthchecklist = []
        for snmplabel,snmpoid in self.get_snmpdict().items():
          created, healthcheck = HealthcheckModel.get_or_create(self.session,
                                                          name=snmplabel,
                                                          source=self.source,
                                                          element=element)
    
          value = self.snmpget(snmpoid,snmp_ipserver,snmp_port,snmp_community)
          if isinstance(value,NoSuchObject):
            value = None
          healthcheck.value = str(value) if value is not None else 0
          healthcheck.status='start'
          self.session.flush()
          self.session.commit()
          self.session.refresh(healthcheck)
          self.historize(healthcheck)
          logger.info('{snmp_ipserver}:{uuid}:{status}:{name}:{value}'.format(snmp_ipserver=snmp_ipserver,uuid=healthcheck.uuid,status=healthcheck.status,name=healthcheck.name,value=healthcheck.value))
          healthchecklist.append(healthcheck.to_dict())
        return healthchecklist
        

  def getcpunumber(self,snmp_ipserver):
    with app.app_context():
      snmp_community = self.get_param(key="{source}_community".format(source=self.source))
      snmp_port = self.get_param(key="{source}_port".format(source=self.source))
  
      if snmp_ipserver is not None or snmp_community is not None or snmp_port is not None:
        #print(self.snmpwalk('.1.3.6.1.2.1.25.3.3.1.1',snmp_ipserver,snmp_port,snmp_community))
        item = 0
        for errorIndication, \
          errorStatus, \
          errorIndex, \
          varBinds in nextCmd(SnmpEngine(),
                              CommunityData(snmp_community, mpModel=0),
                              UdpTransportTarget((snmp_ipserver, snmp_port)),
                              ContextData(),
                              ObjectType(ObjectIdentity('HOST-RESOURCES-MIB', 'hrProcessorFrwID')),
                              lexicographicMode=False):
          item += 1
        return str(item)




@celery.task(base=SnmpHealthCheck,bind=True,resultrepr_maxsize=2048)
def snmpsourcehealthcheck(self):
  with app.app_context():
    snmp_server = self.get_param(key="{source}_serversource".format(source=self.source))
   
    if snmp_server is not None:
      healthchecklist = self.getsimple(snmp_ipserver=snmp_server,element='source')
      created, healthcheck = HealthcheckModel.get_or_create(session=self.session,
                                                            name='cpunumber',
                                                            source=self.source,
                                                            element='source')
  
       
      healthcheck.status='start'
      healthcheck.value=self.getcpunumber(snmp_ipserver=snmp_server)
      self.session.flush()
      self.session.commit()
      self.session.refresh(healthcheck)
      history = self.historize(healthcheck)
      healthchecklist.append(healthcheck.to_dict())
  
    return healthchecklist    

      

@celery.task(base=SnmpHealthCheck,bind=True,resultrepr_maxsize=2048)
def snmptargethealthcheck(self):
  with app.app_context():
    snmp_server = self.get_param(key="{source}_servertarget".format(source=self.source))
  
    if snmp_server is not None:
      healthchecklist = self.getsimple(snmp_ipserver=snmp_server,element='target')
      created, healthcheck = HealthcheckModel.get_or_create(session=self.session,
                                                            name='cpunumber',
                                                            source=self.source,
                                                            element='target')
      healthcheck.status='start'
      healthcheck.value=self.getcpunumber(snmp_ipserver=snmp_server)
      self.session.flush()
      self.session.commit()
      self.session.refresh(healthcheck)
      self.historize(healthcheck)
      healthchecklist.append(healthcheck.to_dict())
  
    return healthchecklist
  
@celery.task(base=SnmpHealthCheck,bind=True,resultrepr_maxsize=2048)
def snmpworkershealthcheck(self):
  with app.app_context():
    workers = [workerfullname.split('@')[1] for workerfullname in celery_ins.control.inspect().ping().keys() ]
    healthchecklist = []
    for snmp_server in workers:
      healthchecklist.append(self.getsimple(snmp_ipserver=snmp_server,element=snmp_server))
      created, healthcheck = HealthcheckModel.get_or_create(session=self.session,
                                                            name='cpunumber',
                                                            source=self.source,
                                                            element=snmp_server)
      healthcheck.status='start'
      healthcheck.value=self.getcpunumber(snmp_ipserver=snmp_server)
      self.session.flush()
      self.session.commit()
      self.session.refresh(healthcheck)
      history = self.historize(healthcheck)
      healthchecklist.append(healthcheck.to_dict())
  
  return healthchecklist
    
    

class WorkerHealthCheck(Healthcheck):
  source = 'WORKER'
  def evaluate_element_load(self,element,processnumber,weight=1):
    """
    les opération en minus ont un poids doublés
    """
    try:
      modified=False
      decision=None
      paces = int(self.get_param(key='{source}_pacesnumprocess'.format(source=self.source))) 
      load5healthcheck = HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='cpu5mn')
      ratioload5min = float(load5healthcheck.value) / float(HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='cpunumber').value)
      logger.info('in evaluate_element_load for {element} ratioload5min {ratioload5min}'.format(element=element,ratioload5min=ratioload5min))
      if ratioload5min > float(self.get_param(key='{source}_load5ratiomax'.format(source=self.source))) :
        ratioload1min = float(HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='cpu1mn').value) / float(HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='cpunumber').value)
        logger.info('in evaluate_element_load for {element} ratioload1min {ratioload1min}'.format(element=element,ratioload1min=ratioload1min))
        if ratioload1min >= ratioload5min :
          processnumber = processnumber -  ( paces * ( weight * 2 )  )
          decision='minus'
          modified = True
      elif ratioload5min < ( float(self.get_param(key='{source}_load5ratiomax'.format(source=self.source))) - float(self.get_param(key='{source}_loadratiomargin'.format(source=self.source)))) :
        processnumber = processnumber + ( paces * weight )
        decision='majus'
        modified = True
      logger.info('in evaluate_element_load for {element} modified:{modified}/processnumber:{processnumber}/uuid:{uuid}'.format(element=element,modified=modified,processnumber=processnumber,uuid=load5healthcheck.uuid))
      return modified,processnumber,load5healthcheck.uuid,decision
    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{}'.format(element,msgerr))
  
  def evaluate_element_mem(self,element,processnumber,weight=1):
    """
    les opération en minus ont un poids doublés
    """
    try:
      modified=False
      decision=None
      paces = int(self.get_param(key='{source}_pacesnumprocess'.format(source=self.source))) 
      freememhealthcheck = HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='phyramfree')
      cachememhealthcheck = HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='phyramcache')
      totalmemhealthcheck = HealthcheckModel.get_one(session=self.session,source='SNMP',element=element,name='phyramtotal')
      freemem = ( int(freememhealthcheck.value) + int(cachememhealthcheck.value)) / int(totalmemhealthcheck.value)
      logger.info('in evaluate_element_mem for {element} freemem % {freemem} = {freememhealthcheck} + {cachememhealthcheck} / {totalmemhealthcheck}'.format(element=element,
                                                                                                                                                            freemem=freemem,
                                                                                                                                                            freememhealthcheck=freememhealthcheck.value,
                                                                                                                                                            cachememhealthcheck=cachememhealthcheck.value,
                                                                                                                                                            totalmemhealthcheck=totalmemhealthcheck.value,
                                                                                                                                                            ))
      if freemem < float(self.get_param(key='{source}_freemem'.format(source=self.source))):
        processnumber = processnumber - ( paces * ( weight * 2 )  )
        decision='minus'
        modified = True


      logger.info('in evaluate_element_mem for {element} modified:{modified}/processnumber:{processnumber}/uuid:{uuid}'.format(element=element,modified=modified,processnumber=processnumber,uuid=freememhealthcheck.uuid))
      return modified,processnumber,freememhealthcheck.uuid,decision
    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{}'.format(element,msgerr))

  def autoworker(self,worker,processnumber):
    """
      abandon de l'arbre de decision s'il y a un minus
    """
    modified=[]
    uuidlist=[]
    if len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == worker).filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'phyramfree' ).all() ) != 0 \
        and len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == worker).filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'phyramtotal' ).all() ) != 0 :
      memworkermodified , processnumber, uuid, decision = self.evaluate_element_mem(element=worker,processnumber=processnumber,weight=2 )
      modified.append(memworkermodified)
      if memworkermodified:
        uuidlist.append(uuid)
        logger.info('worker memory evaluation changed worker to {}'.format(processnumber))
    
    if decision == 'majus' or decision is None :
      if len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'source').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'phyramfree' ).all() ) != 0 \
          and len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'source').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'phyramtotal' ).all() ) != 0 :
        memsourcemodified , processnumber, uuid, decision = self.evaluate_element_mem(element='source',processnumber=processnumber,weight=1 )
        modified.append(memsourcemodified)
        if memsourcemodified:
          uuidlist.append(uuid)
          logger.info('source memory evaluation changed worker to {}'.format(processnumber))

    if decision == 'majus' or decision is None :
      if len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'target').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'phyramfree' ).all() ) != 0 \
          and len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'target').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'phyramtotal' ).all() ) != 0 :
        memtargetmodified ,processnumber,uuid,decision = self.evaluate_element_mem(element='target',processnumber=processnumber,weight=1  ) 
        modified.append(memtargetmodified)
        if memtargetmodified:
          uuidlist.append(uuid)
          logger.info('target memory evaluation changed worker to {}'.format(processnumber))
              
    
    if decision == 'majus' or decision is None  :
      # pour ce faire il faut connaitre la charge du worker en attente des healthcheck snmp relatif au cpu
      if len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == worker).filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'cpunumber' ).all() ) != 0 \
          and len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == worker).filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'cpu5mn' ).all() ) != 0 :
        cpuworkermodified , processnumber, uuid, decision = self.evaluate_element_load(element=worker,processnumber=processnumber,weight=2 )
        modified.append(cpuworkermodified)
        if cpuworkermodified:
          uuidlist.append(uuid)
          logger.info('worker cpu-load  evaluation changed worker to {}'.format(processnumber))

    if decision == 'majus' or decision is None  :
      if len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'source').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'cpunumber' ).all() ) != 0 \
          and len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'source').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'cpu5mn' ).all() ) != 0 :
        cpusourcemodified , processnumber, uuid, decision = self.evaluate_element_load(element='source',processnumber=processnumber,weight=1)
        modified.append(cpusourcemodified)
        if cpusourcemodified:
          uuidlist.append(uuid)
          logger.info('source cpu-load  evaluation changed worker to {}'.format(processnumber))

    if decision == 'majus' or decision is None  :
      if len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'target').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'cpunumber' ).all() ) != 0 \
          and len( self.session.query(HealthcheckModel).filter(HealthcheckModel.element == 'target').filter(HealthcheckModel.source == 'SNMP' ).filter(HealthcheckModel.name == 'cpu5mn' ).all() ) != 0 :
        cputargetmodified ,processnumber, uuid, decision = self.evaluate_element_load(element='target',processnumber=processnumber,weight=1) 
        modified.append(cputargetmodified)
        if cputargetmodified:
          uuidlist.append(uuid)
          logger.info('target cpu-load  evaluation changed worker to {}'.format(processnumber))

    return processnumber,uuidlist


 
  def getargswatcher(self,worker):
    try:
      getargscommand='circusctl --endpoint tcp://{worker}:5555 --json get worker_transfert args'.format(worker=worker)
      getargswatcher = delegator.run(getargscommand)
      getargswatcherjson = json.loads(getargswatcher.out)
      logger.info("{}:{} -> {}".format(worker,getargscommand,getargswatcher.return_code))
      return getargswatcherjson['options']['args']
    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{} for worker {}'.format('concurency',msgerr,worker))
      
  def getconcurrency(self,worker):
    try:
      current_concurency = int(re.search('^[a-zA-Z-._ =]+([0-9]+)$',self.getargswatcher(worker)).group(1))
      logger.info('current_concurency from circusctl {}'.format(current_concurency))
      return int(current_concurency)
    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{} for worker {}'.format('concurency',msgerr,worker))
      
  def getstatuswatcher(self,worker): 
    try:
      getstatuscommand='circusctl --endpoint tcp://{worker}:5555 --json status worker_transfert '.format(worker=worker)
      getstatuswatcher = delegator.run(getstatuscommand)
      getstatuswatcherjson = json.loads(getstatuswatcher.out)
      logger.info("{}:{} -> {}".format(worker,getstatuscommand,getstatuswatcher.return_code))
      return getstatuswatcherjson
    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{} for worker {}'.format('concurency',msgerr,worker))


  def setconcurrency(self,worker,poolmanager,nextconcurrency,currentargs):
    try:
      argswatcherexpected = 'worker -A api.celery_ins -l info -P {poolmanager} -E --concurrency={concurrency}'.format(poolmanager=poolmanager,concurrency=nextconcurrency)
      setargswatcher=None
      reconfigured = False

      if currentargs  != argswatcherexpected:
        reconfigured = True
        logger.info('watcher need reconfiguration on {worker} for concurency {concurency}'.format(worker=worker,concurency=nextconcurrency))
        setargscommand='circusctl --endpoint tcp://{worker}:5555 --json set worker_transfert \'args\' \'{argswatcherexpected}\''.format(worker=worker,argswatcherexpected=argswatcherexpected)
        setargswatcher = delegator.run(setargscommand)
        logger.info("{}:{} -> {}".format(worker,setargscommand,setargswatcher.return_code))
        reloadcommand='circusctl --endpoint tcp://{worker}:5555 --json restart worker_transfert'.format(worker=worker)
        reloadwatcher=delegator.run(reloadcommand)
        logger.info("{}:{} -> {}".format(worker,reloadcommand,reloadwatcher.return_code))
        logger.info(reloadwatcher.out)
        return reconfigured
    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{} for worker {}'.format('concurency',msgerr,worker)) 

  def incrnumprocesswatcher(self,worker):
    incrnumprocesscommand = 'circusctl --endpoint tcp://{worker}:5555 --json incr worker_transfert '.format(worker=worker)
    incrnumprocesswatcher = delegator.run(incrnumprocesscommand)
    incrnumprocesswatcherjson = json.loads(incrnumprocesswatcher.out)
    logger.info("{}:{} -> {} : {}".format(worker,incrnumprocesscommand,incrnumprocesswatcher.return_code,incrnumprocesswatcherjson))
    sleep(2)
    return incrnumprocesswatcherjson

  def decrnumprocesswatcher(self,worker):
    decrnumprocesscommand = 'circusctl --endpoint tcp://{worker}:5555 --json decr worker_transfert '.format(worker=worker)
    decrnumprocesswatcher = delegator.run(decrnumprocesscommand)
    decrnumprocesswatcherjson = json.loads(decrnumprocesswatcher.out)
    logger.info("{}:{} -> {} : {}".format(worker,decrnumprocesscommand,decrnumprocesswatcher.return_code,decrnumprocesswatcherjson))
    sleep(2)
    return decrnumprocesswatcherjson

  def getnumprocesswatcher(self,worker):
    getnumprocesscommand = 'circusctl --endpoint tcp://{worker}:5555 --json numprocesses worker_transfert '.format(worker=worker)
    getnumprocesswatcher = delegator.run(getnumprocesscommand)
    getnumprocesswatcherjson = json.loads(getnumprocesswatcher.out)
    logger.info("{}:{} -> {} : {}".format(worker,getnumprocesscommand,getnumprocesswatcher.return_code,getnumprocesswatcherjson))
    
    return getnumprocesswatcherjson

  def getnumprocess(self,worker):
    self.current_numprocess = self.getnumprocesswatcher(worker).get('numprocesses')
    return int(self.current_numprocess)

  def setnumprocess(self,worker,numprocessexpected):
    reconfigured = False
    try:
      logger.info('{}:current numprocess:|{}|. desired numprocess:|{}|.'.format(worker,self.getnumprocess(worker=worker),numprocessexpected))
      if self.getnumprocess(worker=worker) > numprocessexpected :
        reconfigured = True
        while self.getnumprocess(worker=worker) > numprocessexpected:
          logger.info('{}:current numprocess:|{}|. desired numprocess:|{}|. need less'.format(worker,self.getnumprocess(worker=worker),numprocessexpected))
          try:
            result = self.decrnumprocesswatcher(worker=worker)

          except Exception as e:
            if hasattr(e, 'message'):
              msgerr=e.message
            else:
              msgerr=e
            logger.exception('when treating {}{} for worker {}'.format('numprocess decr',msgerr,worker))             


      if self.getnumprocess(worker=worker) < numprocessexpected :
        reconfigured = True
        while self.getnumprocess(worker=worker) < numprocessexpected:
          logger.info('{}:current numprocess:|{}|. desired numprocess:|{}|. need more'.format(worker,self.getnumprocess(worker=worker),numprocessexpected))
          try:
            result = self.incrnumprocesswatcher(worker=worker)
          except Exception as e:
            if hasattr(e, 'message'):
              msgerr=e.message
            else:
              msgerr=e
            logger.exception('when treating {}{} for worker {}'.format('numprocess incr',msgerr,worker))
     

    except Exception as e:
      if hasattr(e, 'message'):
        msgerr=e.message
      else:
        msgerr=e
      logger.exception('when treating {}{} for worker {}'.format('numprocess',msgerr,worker))

    return reconfigured 
   
@celery.task(base=WorkerHealthCheck,bind=True,resultrepr_maxsize=2048)
def manageworkers(self):
  with app.app_context():
    WORKER_pacesnumprocess = int(self.get_param(key='{source}_pacesnumprocess'.format(source=self.source)))

    WORKER_poolmanager = self.get_param(key='{source}_poolmanager'.format(source=self.source))

    controller = Control(celery_ins)
    healthchecklist = []
    workersstats = controller.inspect().stats()
    for workerfullname,workerstat in workersstats.items():
      worker = workerfullname.split('@')[1]

      if worker != socket.gethostname():
        logger.info('---------------------------')
        logger.info('{worker} is a active runner'.format(worker=worker))
        currentprocessnumber = self.getnumprocess(worker)

        currentconcurrencynumber = self.getconcurrency(worker)

        stoppers = self.session.query(HealthcheckModel) \
                   .filter(HealthcheckModel.element.in_(['*','source','target',worker])) \
                   .filter(HealthcheckModel.name != 'numprocess') \
                   .filter(HealthcheckModel.status == 'stop').all()
        uuidlist = [stopper.uuid for stopper in stoppers ]
        
        # quel est le comportement paramétré pour le nombre de process
        workerbehaviour = self.get_param(key='{source}_numprocess'.format(source=self.source))

        # quel est le comportement paramétre pour la valeur de concurence
        WORKER_numconcurrency = self.get_param(key='{source}_numconcurrency'.format(source=self.source))
          
        # quel est le nombre de process max quer je peux lancer
        WORKER_numprocessmax = int(self.get_param(key='{source}_numprocessmax'.format(source=self.source)))

        logger.info('workerbehaviour : {workerbehaviour}. WORKER_numprocessmax : {WORKER_numprocessmax}. WORKER_numconcurency : {WORKER_numconcurrency}'.format(
                                          workerbehaviour=workerbehaviour,
                                          WORKER_numprocessmax=WORKER_numprocessmax,
                                          WORKER_numconcurrency=WORKER_numconcurrency,
                                        ))
        if len(stoppers) != 0 :
          ctrl = { 'status':'stop',
                   'processnumber':self.get_param(key='{source}_numprocessonstart'.format(source=self.source)),
                   'concurrencynumber':WORKER_numconcurrency }
          logger.info('Stopper condition : {uuidlist}'.format(uuidlist=uuidlist))
        else:
          # lancement du worker
          ctrl = { 'status':'start','processnumber':currentprocessnumber,'concurrencynumber':WORKER_numconcurrency }
          created, healthcheck = HealthcheckModel.get_or_create(session=self.session,
                                                                name='numprocess',
                                                                source=self.source,
                                                                element=worker)
          logger.info('No stopper condition. Initial config is {}'.format(ctrl)) 
          if workerbehaviour == 'auto' :
            # le comportement attendu est intelligent. cela renseigne  l'instance de healthcheck ayant pour source = 'WORKER', name = 'numprocess', element='le_nom_complet_du_worker'
            # pour ce faire il faut connaitre la charge du worker en attente des healthcheck snmp relatif a la memoire

            ctrl['processnumber'], autouuidlist = self.autoworker(worker=worker,processnumber=currentprocessnumber)
            
            uuidlist.extend(autouuidlist)
            
          elif workerbehaviour == 'off' :
            # le comportement attendu est inactif. cela se basera sur l'instance de healthcheck ayant pour source = 'WORKER', name = 'numprocess', element='le_nom_complet_du_worker'
            ctrl['processnumber'] = healthcheck.value
          elif int(workerbehaviour) <= WORKER_numprocessmax :
            # le comportement attendu est naif. tous les worker auront un max-concurency a cette valeur. cela renseigne  l'instance de healthcheck ayant pour source = 'WORKER', name = 'numprocess', element='le_nom_complet_du_worker'
            ctrl['processnumber'] = int(workerbehaviour)
          else:
            raise ValueError("Param {source}_numprocess is not know".format(
                                                                    source=self.source,
                                                                  ),
                  "Create an instance of param for {source}_numprocess".format(
                                                                                  source=self.source,
                                                                                )
                  ) 
        
        if int(ctrl['processnumber'] ) > WORKER_numprocessmax :
          logger.info('{desired} is bigger than {numprocessmax}(params WORKER_numprocessmax). taking {numprocessmax}'.format(desired=ctrl.get('processnumber'),numprocessmax=WORKER_numprocessmax))
          ctrl['processnumber'] = WORKER_numprocessmax

        if int(ctrl.get('processnumber')) <= 0 :
          logger.info('{desired} is less than 0. taking 0 and stop'.format(desired=ctrl.get('processnumber')))
          ctrl['processnumber']=0
          ctrl['status']='stop'
          
        diff = currentprocessnumber - int(ctrl['processnumber'] )
        if diff > 0 :
          ctrl['action'] = 'shrink'
          ctrl['paces'] = diff
        elif diff < 0:
          ctrl['action'] = 'grow'
          ctrl['paces'] = abs(diff)
        else:
          ctrl['action'] = 'keep'
          ctrl['paces'] = 0

        created, healthcheck = HealthcheckModel.get_or_create(session=self.session,
                                                                name='numprocess',
                                                                source=self.source,
                                            element=worker)



        healthcheck.reason = "status:{status}:poolmanager:{poolmanager}:workerbehaviour:{workerbehaviour}:currentnumberofprocess:{currentnumberofprocess}:nextnumberofprocess:{nextnumberofprocess}:currentconcurency:{currentconcurrency}:nextconcurency:{nextconcurrency}:action:{action}:paces:{pacesnumprocess} because of {uuidlist}".format(
                                                                                                                                                         status=ctrl.get('status'),
                                                                                                                                                         poolmanager=WORKER_poolmanager,
                                                                                                                                                         workerbehaviour=workerbehaviour,
                                                                                                                                                         currentnumberofprocess=currentprocessnumber,
                                                                                                                                                         nextnumberofprocess=ctrl['processnumber'],
                                                                                                                                                         currentconcurrency=currentconcurrencynumber,
                                                                                                                                                         nextconcurrency=ctrl['concurrencynumber'],
                                                                                                                                                         action=ctrl['action'],
                                                                                                                                                         pacesnumprocess=WORKER_pacesnumprocess,
                                                                                                                                                         uuidlist=uuidlist,
                                                                                                                                                         )

        healthcheck.value = ctrl.get('processnumber')
        healthcheck.status = ctrl.get('status')
        self.session.flush()
        self.session.commit()
        self.session.refresh(healthcheck)
        self.historize(healthcheck)
        healthchecklist.append(healthcheck.to_dict())
        
        logger.info('numprocess healtcheck is {} '.format(healthcheck.uuid))

        reconfigured = False
 
        try:
          logger.info('{}:current status:|{}|. desired status:|{}|'.format(worker,self.getstatuswatcher(worker).get('status'),ctrl.get('status')))
          setstatuscommand=None
          setstatuswatcher=None
          currentstatus = self.getstatuswatcher(worker).get('status')
          if currentstatus == 'stopped' and ctrl.get('status') == 'start':
            logger.info('watcher need to be started on {worker}'.format(worker=worker))
            setstatuscommand='circusctl --endpoint tcp://{worker}:5555 --json start worker_transfert '.format(worker=worker) 
            reconfigured = True
  
          if currentstatus== str('active') and ctrl.get('status') == 'stop':
            logger.info('watcher need to be stopped on {worker}'.format(worker=worker))
            setstatuscommand='circusctl --endpoint tcp://{worker}:5555 --json stop worker_transfert '.format(worker=worker)
            reconfigured = True
  
          if setstatuscommand is not None:
            setstatuswatcher = delegator.run(setstatuscommand)
            logger.info("{}:{} -> {}".format(worker,setstatuscommand,setstatuswatcher.return_code))
        except Exception as e:
          if hasattr(e, 'message'):
            msgerr=e.message
          else:
            msgerr=e
          logger.exception('when treating {}{} for worker {}'.format('status',msgerr,worker))
          
        if ctrl.get('status') != 'stop' and reconfigured == False:
          reconfigurednumprocess = self.setnumprocess(worker=worker,numprocessexpected=int(ctrl.get('processnumber')))
          logger.info("number of process have been reconfigured {}".format(reconfigurednumprocess))

          reconfiguredconcurrency = self.setconcurrency(worker=worker,poolmanager=WORKER_poolmanager,nextconcurrency=int(ctrl.get('concurrencynumber')),currentargs=self.getargswatcher(worker))
          logger.info("concurency have been reconfigured {}".format(reconfiguredconcurrency))

          if reconfigurednumprocess == True or reconfiguredconcurrency == True:
            reconfigured = True

        if reconfigured:
          if app.config.get('XMPP_SERVER') is not None or app.config.get('XMPP_USER') is not None or app.config.get('XMPP_PASSWORD') is not None or app.config.get('XMPP_ROOM') is not None or app.config.get('XMPP_PORT') is not None:
            pagers = [user.pager for user in UserModel.get(session=self.session, pageme=True ) if user.pager is not None]
            logger.info('xmpp message send to {XMPP_ROOM}'.format(XMPP_ROOM=app.config.get('XMPP_ROOM')))
            message = '{workerfullname} avec le {poolmanager} mode est en état {status} avec une concurence de {concurrencynumber}. Le nombre de process {action} est a {numberofprocess} grace à {uuidlist}. \r\nL etat précédent était : \r\n\t- {previousstatus}\r\n\t- nombre de process: {previousprocessnumber}\r\n\t- concurence :  {previousconcurrencynumber}'.format(workerfullname=workerfullname,
                                                              action=ctrl['action'],
                                                              status=ctrl.get('status'),
                                                              poolmanager=WORKER_poolmanager,
                                                              concurrencynumber=ctrl['concurrencynumber'],
                                                              numberofprocess=ctrl['processnumber'],
                                                              uuidlist=uuidlist,
                                                              previousstatus=currentstatus,
                                                              previousprocessnumber=currentprocessnumber,
                                                              previousconcurrencynumber=currentconcurrencynumber,
                                                              )
            xmpp = SendMsgBot(nick=app.config['XMPP_USER'].split('@')[0],jid=app.config['XMPP_USER'],password=app.config['XMPP_PASSWORD'],room=app.config['XMPP_ROOM'],invitations=pagers,msg=message)
            xmpp.register_plugin('xep_0030')
            xmpp.register_plugin('xep_0199')
            xmpp.register_plugin('xep_0045')
            xmpp.connect((app.config['XMPP_SERVER'],app.config['XMPP_PORT']))
            xmpp.process(timeout=2)

        # ceci ne marche je pense a cause de circus en front
        #if ctrl.get('action') == 'shrink' :
        #  controller.pool_shrink(n=diff,destination=[workerfullname,]) 
        #elif ctrl.get('action') == 'grow' :
        #  controller.pool_grow(n=abs(diff),destination=[workerfullname,])
 
        # ceci marche pas terriblement trop lent a repondre 
        #if ctrl.get('status') == 'stop':
        #  controller.cancel_consumer(queue='celery',destination=[workerfullname,])
        #elif ctrl.get('status') == 'start':
        #  controller.add_consumer(queue='celery',destination=[workerfullname,])

  return  healthchecklist
   







