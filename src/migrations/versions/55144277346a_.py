"""empty message

Revision ID: 55144277346a
Revises: 260512843792
Create Date: 2020-07-24 11:36:01.513057

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '55144277346a'
down_revision = '260512843792'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('transferts', sa.Column('duration', sa.Interval(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('transferts', 'duration')
    # ### end Alembic commands ###
