"""empty message

Revision ID: 197e3e0571e2
Revises: 
Create Date: 2019-10-19 21:31:43.192503

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '197e3e0571e2'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('transferts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('uuid', sa.Text(), nullable=True),
    sa.Column('root_source', sa.Text(), nullable=True),
    sa.Column('root_target', sa.Text(), nullable=True),
    sa.Column('path', sa.Text(), nullable=True),
    sa.Column('filename', sa.Text(), nullable=True),
    sa.Column('status', sa.Text(), nullable=True),
    sa.Column('checksum_source', sa.Text(), nullable=True),
    sa.Column('checksum_target', sa.Text(), nullable=True),
    sa.Column('message', sa.Text(), nullable=True),
    sa.Column('created_on', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
    sa.Column('updated_on', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('uuid')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('transferts')
    # ### end Alembic commands ###
