"""empty message

Revision ID: 3b0207ef7a99
Revises: 197e3e0571e2
Create Date: 2019-10-24 22:04:46.234851

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3b0207ef7a99'
down_revision = '197e3e0571e2'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('transferts', sa.Column('jobname', sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('transferts', 'jobname')
    # ### end Alembic commands ###
