#!/bin/bash
# Description : ce script est à utiliser pour installer les differents tiers de l'application Dtransfert aka Distributed Transfer

SCRIPTNAME=$(basename $0)

oops(){
  /bin/cat << EOF
Oooops, an error occured. Read above message if any.
Process Aborted !

EOF
  exit 1
}

usage(){
  /bin/cat << EOF
Usage : ${SCRIPTNAME} 
  - <-r|--roles>: role
Description : 
EOF
}


ARGS=`getopt -o "r:h:" -l "roles:" -l "help" -- "$@"`

if [ $? -ne 0 ]; then # script have to follow corect syntax
  usage
  exit 1
fi


chmod u+x ./*/*.sh

while true; do
  case "$1" in
    -r|--roles) roles+=("${2}"); shift 2;;
    -h|--help) usage; exit 0;;
    *) break;;
  esac
done


for role in "${roles[@]}";do
  filerole=$(find ./ -name "${role}.sh")
  echo "Executing ${filerole}  >> ${filerole%.*}.log 2>&1"

  $($filerole) #>> ${filerole%.*}.log 2>&1

done

