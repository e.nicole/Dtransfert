SCRIPTNAME=$(basename $0)
DIRNAME=$(dirname $(which ${0}))

chmod u+x ${DIRNAME}/../export_env.sh
source ${DIRNAME}/../export_env.sh

rpm --import ${DIRNAME}/rpms/*.gpg

yum install -y ${DIRNAME}/rpms/*.rpm


systemctl enable rabbitmq-server 
systemctl start rabbitmq-server 


rabbitmqctl add_user dtransfert ${RMQPWD}
rabbitmqctl set_permissions  dtransfert ".*" ".*" ".*"



