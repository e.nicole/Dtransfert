#!/bin/bash
echo $(which ${0})
SCRIPTNAME=$(basename $0)
DIRNAME=$(dirname $(which ${0}))

source ${DIRNAME}/../export_env.sh

yum install -y python36 python36-devel --nogpgcheck
yum install -y gcc --nogpgcheck 

yum install -y git --nogpgcheck 

useradd dtransfert --home /USR/dtransfert --create-home --shell /bin/bash
loginctl enable-linger dtransfert

mkdir -p /USR/dtransfert/circusd/
mkdir -p /OPT/dtransfert
mkdir -p /VAR/dtransfert

python3 -m venv /OPT/dtransfert/

source /OPT/dtransfert/bin/activate
/OPT/dtransfert/bin/pip install --upgrade pip  --no-index --find-links file:///${DIRNAME}/pylib/
/OPT/dtransfert/bin/pip install -r ${DIRNAME}/requirments.txt --no-index --find-links file:///${DIRNAME}/pylib/


git clone ${DIRNAME}/../../ /USR/dtransfert/app/
git --git-dir=/USR/dtransfert/app/.git  fetch --all --tags
git --git-dir=/USR/dtransfert/app/.git  checkout tags/${GIT_TAG} -b release-${GIT_TAG}

if [ -e ${DIRNAME}/env ];then
  cp ${DIRNAME}/env /USR/dtransfert/app/src/.env
fi


if [ -e ${DIRNAME}/circusd.service ] && [ ! -e /etc/systemd/system/circusd.service ];then
  cp ${DIRNAME}/circusd.service /etc/systemd/system/circusd.service
fi
systemctl daemon-reload
systemctl enable circusd.service



yum install -y net-snmp net-snmp-utils --nogpgcheck
if [ -e ${DIRNAME}/snmpd.conf ];then
  #cp ${DIRNAME}/snmpd.conf /etc/snmp/snmpd.conf
  cat ${DIRNAME}/snmpd.conf  |  sed -e "s@.*#{CONTROLLERIP}.*@  allow $CONTROLLERIP;@" > /etc/snmp/snmpd.conf

fi
systemctl enable snmpd
systemctl restart snmpd


yum install -y ${DIRNAME}/rpms/nginx-1.16.1-1.el7.ngx.x86_64.rpm --nogpgcheck

if [ -e /etc/nginx/conf.d/default.conf ];then
  mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf-sav
fi

systemctl enable nginx

