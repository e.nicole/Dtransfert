#!/bin/bash
echo $(which ${0})
SCRIPTNAME=$(basename $0)
DIRNAME=$(dirname $(which ${0}))

${DIRNAME}/common.sh

chmod u+x ${DIRNAME}/../export_env.sh
source ${DIRNAME}/../export_env.sh

/bin/cat << EOF > ${DIRNAME}/../../src/env
TOKEN_SECRET_KEY='${TOKEN_SECRET_KEY}'
SQLALCHEMY_DATABASE_URI='${SQLALCHEMY_DATABASE_URI}'
CELERY_BROKER='${CELERY_BROKER}'
CELERY_RESULT='${CELERY_RESULT}'
EOF

echo "Content of ${DIRNAME}/../../src/env "
cat ${DIRNAME}/../../src/env

cp ${DIRNAME}/../../src/env /USR/dtransfert/app/src/.env

cp ${DIRNAME}/runner/circus.ini /USR/dtransfert/circusd/

systemctl restart circusd.service

cat ${DIRNAME}/runner/nginx.conf  | sed -s "s/#{hostname}/$(hostname -f)/" | sed -e "s@.*#{CONTROLLERIP}.*@  allow $CONTROLLERIP;@" > /etc/nginx/nginx.conf

systemctl restart nginx

${DIRNAME}/rights.sh
