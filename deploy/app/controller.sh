#!/bin/bash
echo $(which ${0})
SCRIPTNAME=$(basename $0)
DIRNAME=$(dirname $(which ${0}))

${DIRNAME}/common.sh

chmod u+x ${DIRNAME}/../export_env.sh
source ${DIRNAME}/../export_env.sh

/bin/cat << EOF > ${DIRNAME}/../../src/env
TOKEN_SECRET_KEY='${TOKEN_SECRET_KEY}'
SQLALCHEMY_DATABASE_URI='${SQLALCHEMY_DATABASE_URI}'
CELERY_BROKER='${CELERY_BROKER}'
CELERY_RESULT='${CELERY_RESULT}'
XMPP_SERVER='${XMPP_SERVER}'
XMPP_USER='${XMPP_USER}'
XMPP_PASSWORD='${XMPP_PASSWORD}'
XMPP_ROOM='${XMPP_ROOM}'
EOF

echo "Content of ${DIRNAME}/../../src/env "
cat ${DIRNAME}/../../src/env

cp ${DIRNAME}/../../src/env /USR/dtransfert/app/src/.env

cd /USR/dtransfert/app/src 
/OPT/dtransfert/bin/python manage.py db upgrade
/OPT/dtransfert/bin/python manage.py db migrate


cp ${DIRNAME}/controller/circus.ini /USR/dtransfert/circusd/

systemctl restart circusd.service




#cp ${DIRNAME}/controller/nginx.conf /etc/nginx/conf.d/controller.conf 

cat ${DIRNAME}/controller/nginx.conf | sed -e "s@.*#{CHORDNETWORK}.*@  allow $CHORDNETWORK;@"  | sed -e "s@.*#{ADMINNET1}.*@  allow $ADMINNET1;@" | sed -e "s@.*#{ADMINNET2}.*@  allow $ADMINNET2;@" > /etc/nginx/conf.d/controller.conf

systemctl restart nginx

${DIRNAME}/rights.sh

