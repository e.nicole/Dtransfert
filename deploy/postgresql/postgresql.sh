SCRIPTNAME=$(basename $0)
DIRNAME=$(dirname $(which ${0}))

chmod u+x ${DIRNAME}/../export_env.sh
source ${DIRNAME}/../export_env.sh

RPMREPOS="${DIRNAME}/rpms/pgdg-redhat-repo-latest.noarch.rpm"
echo "install ${RPMREPOS}"
yum install -y $RPMREPOS


RPMKEYS='/etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG'
rpm --import $RPMKEYS

yum install -y ${DIRNAME}/rpms/*.rpm 

if [ ! -e /etc/systemd/system/postgresql-12.service ];then
  /bin/cat << EOF >> /etc/systemd/system/postgresql-12.service
.include /usr/lib/systemd/system/postgresql-12.service
[Service]
Environment=PGDATA=${PGDATA}
EOF
fi

systemctl daemon-reload
systemctl enable postgresql-12.service 

if [ ! -e ${PGDATA}pg_hba.conf ];then
  /usr/pgsql-12/bin/postgresql-12-setup initdb
  #cp pg_hba.conf ${PGDATA}
  sed -s "s/#{CHORDNETWORK}/${CHORDNETWORK}/" ${DIRNAME}/pg_hba.conf >> ${PGDATA}/pg_hba.conf
  /bin/cp -f postgresql.conf ${PGDATA}
fi


systemctl start postgresql-12.service

su postgres <<EOSU
#createuser dtransfert -P dtransfert -g dtransfert
psql -c "CREATE ROLE dtransfert PASSWORD '${PGQPWD}' NOSUPERUSER CREATEDB CREATEROLE INHERIT LOGIN ;"
psql -c "CREATE DATABASE dtransfert OWNER dtransfert;"
psql -c "CREATE DATABASE dtransfert_result OWNER dtransfert;"
EOSU

